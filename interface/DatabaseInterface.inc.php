<?php
/**
 * Aetolos - Database interface
 *
 * An interface which defines a database connection.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage database
 */

/**
 * Database interface
 *
 * @package aetolos
 * @subpackage database
 */
interface DatabaseInterface {

	/**
	 * Open new database connection
	 * @api
	 * @return bool
	 */
	public function open();

	/**
	 * Close existing database connection
	 * @api
	 * @return bool
	 */
	public function close();

	/**
	 * Execute an SQL query
	 * @api
	 * @param string $sql SQL query
	 * @return bool
	 */
	public function query($sql);

	/**
	 * Execute multiple SQL queries
	 * @api
	 * @param string $sql One or more SQL queries
	 * @return bool
	 */
	public function multiQuery($sql);

	/**
	 * Get inserted row ID from last SQL query
	 * @api
	 * @param string $name Optional name of the sequence object from which the ID should be returned
	 * @return mixed
	 */
	public function insertId($name = null);

	/**
	 * Get affected rows from last SQL query
	 * @api
	 * @return int|false
	 */
	public function affectedRows();

	/**
	 * Get next row from the results of the last SQL query
	 * @api
	 * @param int $style PDO fetch style
	 * @return bool
	 */
	public function nextRow($style = PDO::FETCH_NUM);

	/**
	 * Get an association array of the results of the last SQL query
	 * @api
	 * @return array<string, mixed>
	 */
	public function getAssocArray();

	/**
	 * Protect SQL queries with escape string
	 * @api
	 * @param string $value SQL query
	 * @return string
	 */
	public function sqlProtect($value);

}

