<?php
/**
 * Aetolos - Setup interface
 *
 * An interface which defines an abstraction usage for Aetolos installations.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage setup
 */

/**
 * Setup interface
 *
 * @package aetolos
 * @subpackage setup
 */
interface SetupInterface {

	/**
	 * Setup system repositories
	 * @api
	 * @return bool
	 */
	public function repository();

	/**
	 * Setup system RPM packages
	 * @api
	 * @return bool
	 */
	public function package();

	/**
	 * Setup SELinux
	 * @return bool
	 */
	public function seLinux();

	/**
	 * Run installation and setup
	 * @api
	 * @return bool
	 */
	public function run();

}

