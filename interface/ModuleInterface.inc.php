<?php
/**
 * Aetolos - Module interface
 *
 * An interface which defines an abstraction module for Aetolos installations.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage module
 */

/**
 * Module interface
 *
 * @package aetolos
 * @subpackage module
 */
interface ModuleInterface {

	/**
	 * Supported module features
	 * @api
	 * @return array<string>
	 */
	public function features();

	/**
	 * Supported module parameters
	 * @api
	 * @return array<string, int|string>
	 */
	public function parameters();

	/**
	 * Required RPM packages
	 * @api
	 * @return array<string>
	 */
	public function packages();

	/**
	 * Module dependency
	 * @api
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies();

	/**
	 * Module command-line parameters
	 * @api
	 * @return array<string>
	 */
	public function cmdParameters();

	/**
	 * Help
	 * @api
	 * @return array<string|array<string, string>>
	 */
	public function help();

	/**
	 * Module controller
	 * @api
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters);

	/**
	 * Setup SELinux requirements
	 * @api
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts);

	/**
	 * Save configuration
	 * @api
	 * @return bool
	 */
	public function saveConfiguration();

	/**
	 * Export virtual host
	 * @api
	 * @param VirtualHostManager $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array<string, string> $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array());

	/**
	 * Enable module
	 * @api
	 * @return bool
	 */
	public function enable();

	/**
	 * Disable module
	 * @api
	 * @return bool
	 */
	public function disable();

	/**
	 * Start service
	 * @api
	 * @return bool
	 */
	public function start();

	/**
	 * Stop service
	 * @api
	 * @return bool
	 */
	public function stop();

	/**
	 * Reload service
	 * @api
	 * @return bool
	 */
	public function reload();

	/**
	 * Service status
	 * @api
	 * @return string
	 */
	public function status();

}

