<?php
/**
 * Aetolos - Import
 *
 * Import cPanel backup file
 *
 * Backup analysis:
 *
 * DIRECTORY		| Description			| Support	| Conversion procedure
 * -------------------------------------------------------------------------------------------
 * bandwidth		= bandwidth data in XML format	| ignored	|
 * counters		= unknown			| ignored	|
 * cp			= account parameters		| supported	| use some parameters
 * cron			= cron jobs			| supported	| copied to /var/spool/cron/
 * dehydrated		= dehydrated module		| supported	|
 * dnszones		= DNS zone files		| ignored	|
 * domainkeys		= DKIM keys			| ignored	|
 * fp			= unknown			| ignored	|
 * homedir		= home directory		| supported	| used instead of homedir.tar
 * httpfiles		= unknown			| ignored	|
 * interchange		= unknown			| ignored	|
 * locale		= unknown			| ignored	|
 * logaholic		= unknown			| ignored	|
 * logs			= log files			| ignored	|
 * meta			= account meta			| ignored	|
 * mm			= unknown			| ignored	|
 * mma			= unknown			| ignored	|
 * mms			= unknown			| ignored	|
 * mysql		= mariadb module		| supported	|
 * mysql-timestamps	= unknown			| ignored	|
 * psql			= unknown			| ignored	|
 * resellerconfig	= unknown			| ignored	|
 * resellerfeatures	= unknown			| ignored	|
 * resellerpackages	= unknown			| ignored	|
 * sslcerts		= SSL certificates		| supported	| copy to /etc/pki/tls/certs/
 * sslkeys		= SSL private keys		| supported	| copy to /etc/pki/tls/private/
 * suspended		= unknown			| ignored	|
 * suspendinfo		= unknown			| ignored	|
 * userconfig		= unknown			| ignored	|
 * userdata		= domain information		| ignored	| use of custom templates
 * va			= email forwarders		| supported	| converted to postfix hash
 * vad			= unknown			| ignored	|
 * vf			= spamassassin filters/rules	| ignored	|
 *
 * FILE			| Description			| Support	| Conversion procedure
 * -------------------------------------------------------------------------------------------
 * addons		= unknown			| ignored	|
 * homedir.tar		= home directory		| supported	|
 * homedir_paths	= Path to user home directory	| supported	| home directory location
 * mysql.sql		= SQL user priviledges		| supported	| imported to MariaDB
 * nobodyfiles		= unknown			| ignored	|
 * pds			= unknown			| ignored	|
 * proftpdpasswd	= ftp password			| ignored	| ftp is not supported
 * quota		= account quota in MB		| ignored	|
 * sds			= unknown			| ignored	|
 * sds2			= unknown			| ignored	|
 * shadow		= user shell password		| ignored	| shell access is not supported
 * shell		= user login shell		| ignored	| replaced by /sbin/nologin
 * ssldomain		= virtual host domain name	| supported	| used as the virtual host name
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage import
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['import'])
)
	die("No Access");

Log::debug("Import virtual host from: " . $cmdParameters['import']);

// Create virtual host object
$vhost = new VirtualHostManager();

// Set parameters - home directory and unix name
$vhost->home = trim(ImportManager::tarReadFile($cmdParameters['import'], "homedir_paths", true));
$pos = mb_strrpos($vhost->home, "/");
if($pos === false) {

	Log::error("Could not parse home directory from the file: homedir_paths");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}
$vhost->unixName = mb_substr($vhost->home, $pos + 1);
$vhost->home = mb_substr($vhost->home, 0, $pos);

// Test for home directory
if(
	!is_dir($vhost->home) &&
	!is_dir("/home")
) {

	Log::error("Backup home directory (" . $vhost->home .
		") does not exist and the default /home is not usable or not found");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

} elseif(!is_dir($vhost->home)) {

	Log::warning("Backup home directory (" . $vhost->home . ") does not exist, using default /home instead");
	$vhost->home = "/home";

}

// Load generic config
$cpConfig = trim(ImportManager::tarReadFile($cmdParameters['import'], "cp/" . $vhost->unixName));

// Set parameters - domain name
$rc = preg_match('/DNS=(.*)/', $cpConfig, $matches);
if(
	$rc === 1 &&
	isset($matches[1]) &&
	!empty($matches[1])
)
	$vhost->domainName = $matches[1];
else
	$vhost->domainName = trim(ImportManager::tarReadFile($cmdParameters['import'], "ssldomain"));
if(empty($vhost->domainName)) {

	Log::error("Could not detect the domain name");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

Log::debug("Domain name: " . $vhost->domainName);

// Parse parked domains (currently limited to 100 parked domains)
$parkedDomains = array();
$rc = preg_match_all('/^DNS[0-9]{1,100}=(.*)/m', $cpConfig, $matches);
if(
	$rc !== false &&
	isset($matches[1])
)
	$parkedDomains = $matches[1];

Log::debug("Parked domains: " . sizeof($parkedDomains));

// Set parameters - account quota
$vhost->quota = (int)trim(ImportManager::tarReadFile($cmdParameters['import'], "quota"));

Log::debug("Quota: " . $vhost->quota);

// Set parameters - MariaDB privileges
$vhost->sqlPrivileges = trim(ImportManager::tarReadFile($cmdParameters['import'], "mysql.sql"));
$sqlPrivCount = substr_count($vhost->sqlPrivileges, "\n");

Log::debug("Database privileges: " . $sqlPrivCount);

// Set parameters - Dehydrated module
$vhost->moduleDehydrated = ImportManager::tarReadDirectory(
	$cmdParameters['import'],
	"dehydrated/certs/" . $vhost->domainName
);

Log::debug("Dehydrated module files: " . sizeof($vhost->moduleDehydrated));

// Validate unix name
$rc = preg_match('/[a-z_][a-z0-9_-]*[$]?/', $vhost->unixName);
if($rc !== 1) {

	Log::error("Unix name failed validation: " . $vhost->unixName);

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

Log::debug("Home directory: " . $vhost->home . "/" . $vhost->unixName);

// Set parameters - admin email
$rc = preg_match('/CONTACTEMAIL=(.*)/', $cpConfig, $matches);
if(
	$rc === 1 &&
	isset($matches[1]) &&
	!empty($matches[1])
)
	$rc = $vhost->adminEmail = $matches[1];
else
	$rc = "(empty)";

Log::debug("Admin/owner email: " . $rc);

// Check unix name
$rc = preg_match('/USER=(.*)/', $cpConfig, $matches);
if(
	$rc === 1 &&
	isset($matches[1])
) {

	if($vhost->unixName !== $matches[1]) {

		Log::error("User name mismatch, the home directory name differs from the actual owner");

		if(
			Config::read("daemon") === true &&
			isset($this)
		) {

			$this->sendHttpError(500);
			return true;

		} else {

			exit(9);

		}

	}

}

// Start with no forwarders
$forwarders = array();

// Load forwarders
$forwarders[$vhost->domainName] = ImportManager::convertForwarders(
	trim(
		ImportManager::tarReadFile($cmdParameters['import'], "va/" . $vhost->domainName)
	)
);

foreach($parkedDomains as $p) {

	$forwarders[$p] = ImportManager::convertForwarders(
		trim(
			ImportManager::tarReadFile($cmdParameters['import'], "va/" . $p)
		)
	);

}

Log::debug("Forwarders loaded: " . sizeof($forwarders));

// Load cron
$cron = trim(ImportManager::tarReadFile($cmdParameters['import'], "cron/" . $vhost->unixName));

Log::debug("Cron size: " . mb_strlen($cron));

// Load SSL/TLS key
$sslKey = trim(ImportManager::tarReadFile($cmdParameters['import'], "sslkeys/" . $vhost->domainName . ".key"));

if(!empty($sslKey))
	$rc = "yes";
else
	$rc = "no";
Log::debug("SSL/TLS key: " . $rc);

// Load SSL/TLS certificate
$sslCert = trim(ImportManager::tarReadFile($cmdParameters['import'], "sslcerts/" . $vhost->domainName . ".crt"));

if(!empty($sslCert))
	$rc = "yes";
else
	$rc = "no";
Log::debug("SSL/TLS certificate: " . $rc);

// Load SSL/TLS CA bundle
$sslBundle = trim(ImportManager::tarReadFile($cmdParameters['import'], "sslcerts/" . $vhost->domainName . ".cabundle"));

if(!empty($sslBundle))
	$rc = "yes";
else
	$rc = "no";
Log::debug("SSL/TLS CA bundle: " . $rc);

// Create virtual host and insert to database
$rc = $vhost->insert();
if($rc === false) {

	Log::error("Error while adding virtual host");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Test for homedir.tar (old cPanel backup files)
$rc = ImportManager::tarFileExists($cmdParameters['import'], "homedir.tar");
if($rc === true) {

	Log::debug("Extracting homedir.tar...");

	// Extract homedir.tar under the user home directory
	exec("/usr/bin/tar --extract --strip-components=1 --one-file-system -f " .
		escapeshellarg($cmdParameters['import']) . " -C " . escapeshellarg($vhost->home . "/" .
		$vhost->unixName) . " " . escapeshellarg("*/homedir.tar") . " 2>/dev/null");

	// Set permissions
	$homeDirTar = $vhost->home . "/" . $vhost->unixName . "/homedir.tar";
	chown($homeDirTar, $vhost->unixName);
	chgrp($homeDirTar, $vhost->unixName);

	Log::debug("Untaring homedir.tar...");

	// Untar homedir.tar
	exec("/usr/bin/sudo -u " .
		escapeshellarg($vhost->unixName) . " /usr/bin/tar --extract --one-file-system --exclude='*/.autoresp" .
		"ond' --exclude='*/.cpaddons' --exclude='*/.cpanel' --exclude='*/perl5' --exclude='*/.autorespond-lo" .
		"opprotect' --exclude='*/.contactemail' --exclude='*/cpbackup-exclude.conf' --exclude='*/.dns' --exc" .
		"lude='*/.filter' --exclude='*/.lastlogin' --exclude='*/.spamassassinenable' --exclude='*/access-log" .
		"s' --exclude='*/www' -f " . escapeshellarg($homeDirTar) . " -C " . escapeshellarg($vhost->home . "/" .
		$vhost->unixName));

	// Delete homedir.tar file
	unlink($homeDirTar);

} else {

	Log::debug("Untaring homedir...");

	// Untar homedir
	exec("/usr/bin/tar --extract --strip-components=2 --one-file-system --exclude='*/.autorespond' --exclude='*/" .
		".cpaddons' --exclude='*/.cpanel' --exclude='*/perl5' --exclude='*/.autorespond-loopprotect' --exclu" .
		"de='*/.contactemail' --exclude='*/cpbackup-exclude.conf' --exclude='*/.dns' --exclude='*/.filter' -" .
		"-exclude='*/.lastlogin' --exclude='*/.spamassassinenable' --exclude='*/access-logs' --exclude='*/ww" .
		"w' -f " . escapeshellarg($cmdParameters['import']) . " -C " . escapeshellarg($vhost->home . "/" .
		$vhost->unixName) . " " . escapeshellarg("*/homedir/") . " 2>/dev/null");

	// Update ownership
	exec("/usr/bin/chown -R " . escapeshellarg($vhost->unixName) . ": " . escapeshellarg($vhost->home . "/" .
		$vhost->unixName));
	if(Config::read("dovecot") === "enabled")
		exec("/usr/bin/chown -R " . escapeshellarg($vhost->unixName . ":dovecot") . " " .
			escapeshellarg($vhost->home . "/" . $vhost->unixName . "/etc"));

}

// Find the real UID
$uid = exec("/usr/bin/id -u " . escapeshellarg($vhost->unixName));
if(!is_numeric($uid)) {

	Log::error("Error while detecting UID");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Find the real GID
$gid = exec("/usr/bin/id -g " . escapeshellarg($vhost->unixName));
if(!is_numeric($gid)) {

	Log::error("Error while detecting GID");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Convert passwd to use the new uid/gid
$rc = ImportManager::convertPasswd(
	$vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd",
	$vhost->home . "/" . $vhost->unixName . "/mail/" . $vhost->domainName,
	$uid,
	$gid
);
if($rc === false) {

	Log::error("Error while converting passwd format");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Set ownership for passwd and shadow
touch($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd");
chmod($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd", 0640);
chown($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd", $vhost->unixName);

if(Config::read("dovecot") === "enabled")
	chgrp($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd", "dovecot");
else
	chgrp($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/passwd", $vhost->unixName);

touch($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/shadow");
chmod($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/shadow", 0640);
chown($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/shadow", $vhost->unixName);

if(Config::read("dovecot") === "enabled")
	chgrp($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/shadow", "dovecot");
else
	chgrp($vhost->home . "/" . $vhost->unixName . "/etc/" . $vhost->domainName . "/shadow", $vhost->unixName);

// Create parked domains and insert to database
foreach($parkedDomains as $p) {

	$rc = $vhost->addParkedDomain($p);
	if($rc === false) {

		Log::error("Error while adding new parked domain");

		if(
			Config::read("daemon") === true &&
			isset($this)
		) {

			$this->sendHttpError(500);
			return true;

		} else {

			exit(9);

		}

	}

}

// Install forwarders
foreach($forwarders as $d => $f)
	ImportManager::installForwarder($vhost, strval($d), $f);

// Run restorecon to make sure all SELinux contexts are correct
exec("/usr/sbin/restorecon -r " . escapeshellarg($vhost->home) . " 2>/dev/null");

// Access rights - needs to be reset because it is overwritten during the untar procedure
chmod($vhost->home . "/" . $vhost->unixName . "/etc", 0751);

// Install cron
if(!empty($cron)) {

	Log::debug("Installing cron");
	ImportManager::installFile(
		$vhost,
		$cron,
		"/var/spool/cron/" . $vhost->unixName,
		0600
	);

}

// Install SSL/TLS key
if(!empty($sslKey)) {

	Log::debug("Installing SSL/TLS key");
	ImportManager::installFile(
		$vhost,
		$sslKey,
		Config::read("pkitls|directoryPrivate") . "/" . $vhost->domainName . ".key",
		0600
	);

}

// Install SSL/TLS certificate
if(!empty($sslCert)) {

	Log::debug("Installing SSL/TLS certificate");
	ImportManager::installFile(
		$vhost,
		$sslCert,
		Config::read("pkitls|directoryCerts") . "/" . $vhost->domainName . ".crt",
		0600
	);

}

// Install SSL/TLS CA bundle
if(!empty($sslBundle)) {

	Log::debug("Installing SSL/TLS certificate");
	ImportManager::installFile(
		$vhost,
		$sslBundle,
		Config::read("pkitls|directoryCerts") . "/" . $vhost->domainName . ".cabundle",
		0600
	);

}

// Check if Dehydrated is enabled
if(
	Config::read("dehydrated") === "enabled" &&
	sizeof($vhost->moduleDehydrated) > 0
) {

	// Archive directory
	$adir = Config::read("dehydrated|basedir") . "/archive/" . $vhost->domainName;

	// Certificate directory
	$cdir = Config::read("dehydrated|basedir") . "/certs/" . $vhost->domainName;

	// Ensure the directory tree exists
	if(!is_dir(Config::read("dehydrated|basedir")))
		mkdir(Config::read("dehydrated|basedir"), 0755);
	if(!is_dir(Config::read("dehydrated|basedir") . "/archive/"))
		mkdir(Config::read("dehydrated|basedir") . "/archive/", 0700);
	if(!is_dir(Config::read("dehydrated|basedir") . "/certs/"))
		mkdir(Config::read("dehydrated|basedir") . "/certs/", 0700);

	// Already exists
	if(
		is_dir($adir) ||
		is_dir($cdir)
	) {

		Log::warning("Dehydrated directories already exist under " . Config::read("dehydrated|basedir") .
			" - ignoring certificates");

	} else {

		// Create domain directories
		mkdir($adir, 0700);
		mkdir($cdir, 0700);

		Log::debug("Importing archive directory: " . $adir);

		// Untar archive directory
		exec("/usr/bin/tar --extract --strip-components=2 --one-file-system -f " .
			escapeshellarg($cmdParameters['import']) . " -C " .
			escapeshellarg(Config::read("dehydrated|basedir")) . " " .
			escapeshellarg("*/dehydrated/archive/") . " 2>/dev/null");

		Log::debug("Importing certificate directory: " . $cdir);

		// Untar certificate directory
		exec("/usr/bin/tar --extract --strip-components=2 --one-file-system -f " .
			escapeshellarg($cmdParameters['import']) . " -C " .
			escapeshellarg(Config::read("dehydrated|basedir")) . " " .
			escapeshellarg("*/dehydrated/certs/") . " 2>/dev/null");

		// Override /etc/pki/tls files with symbolic links to dehydrated
		$fileToLink = "/etc/pki/tls/certs/" . $vhost->domainName . ".chain";
		if(
			is_file($fileToLink) &&
			!is_link($fileToLink)
		) {

			unlink($fileToLink);
			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/chain.pem", $fileToLink);

		} elseif(!is_file($fileToLink)) {

			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/chain.pem", $fileToLink);

		}

		$fileToLink = "/etc/pki/tls/certs/" . $vhost->domainName . ".crt";
		if(
			is_file($fileToLink) &&
			!is_link($fileToLink)
		) {

			unlink($fileToLink);
			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/cert.pem", $fileToLink);

		} elseif(!is_file($fileToLink)) {

			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/cert.pem", $fileToLink);

		}

		$fileToLink = "/etc/pki/tls/certs/" . $vhost->domainName . ".fullchain";
		if(
			is_file($fileToLink) &&
			!is_link($fileToLink)
		) {

			unlink($fileToLink);
			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/fullchain.pem", $fileToLink);

		} elseif(!is_file($fileToLink)) {

			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/fullchain.pem", $fileToLink);

		}

		$fileToLink = "/etc/pki/tls/private/" . $vhost->domainName . ".key";
		if(
			is_file($fileToLink) &&
			!is_link($fileToLink)
		) {

			unlink($fileToLink);
			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/privkey.pem", $fileToLink);

		} elseif(!is_file($fileToLink)) {

			symlink("/etc/pki/letsencrypt/certs/" . $vhost->domainName . "/privkey.pem", $fileToLink);

		}

	}

} elseif(sizeof($vhost->moduleDehydrated) > 0) {

	Log::warning("Dehydrated is not enabled - ignoring certificates");

}

// Check if MariaDB is enabled
if(Config::read("mariadb") === "enabled") {

	// Create database object
	$mdb = new DatabaseMariadb();

	// Open database connection
	$rc = $mdb->open();
	if(
		$rc === false ||
		$mdb->connected !== true
	) {

		Log::error("Error while opening connection to database");

		if(
			Config::read("daemon") === true &&
			isset($this)
		) {

			$this->sendHttpError(500);
			return true;

		} else {

			exit(9);

		}

	}

	if(!empty($vhost->sqlPrivileges)) {

		Log::debug("Executing MariaDB privileges");

		// Remove comments
		$rc = preg_replace(
			array('/^-- .*$/m', '/^\/\*.*$/m', '/\/\*(.*)\*\//u'),
			"",
			$vhost->sqlPrivileges
		);
		if($rc === null) {

			Log::error("Error while parsing SQL privileges for comments");

			if(
				Config::read("daemon") === true &&
				isset($this)
			) {

				$this->sendHttpError(500);
				return true;

			} else {

				exit(9);

			}

		}

		// Remove newlines
		$rc = preg_replace(
			array('/,\n/', '/\n\)/m', '/\)\n/', '/\(\n/'),
			array(', ', ') ', ') ', '( '),
			$rc
		);
		if($rc === null) {

			Log::error("Error while parsing SQL privileges for newlines");

			if(
				Config::read("daemon") === true &&
				isset($this)
			) {

				$this->sendHttpError(500);
				return true;

			} else {

				exit(9);

			}

		}

		// Convert to a single line of multiple statements
		$vhost->sqlPrivileges = strval(str_replace(
			"\n",
			" ",
			$rc
		));

		// Execute
		$mdb->conn->exec($vhost->sqlPrivileges);

	}

	Log::debug("Looking for MariaDB tables");

	// Find tables
	$tables = ImportManager::tarVerboseDirectory($cmdParameters['import'], "mysql");

	// Loop tables
	foreach($tables as $t) {

		// Select user tables and skip system tables (horde, roundcube, etc)
		if(mb_strpos($t[5], ".create") === false)
			continue;

		// Table name
		$pos1 = mb_strrpos($t[5], "/");
		if($pos1 === false)
			continue;
		$pos1++;

		$pos2 = mb_strrpos($t[5], ".create");
		if($pos2 === false)
			continue;
		$name = mb_substr($t[5], $pos1, $pos2 - $pos1);

		// Data size
		$size = 0;
		foreach($tables as $t2) {

			if(mb_strpos($t2[5], $name . ".sql") !== false) {

				$size = $t2[2];
				break;

			}

		}

		Log::debug("Importing table: " . $name . " with size: " . $size . " bytes");

		if($size > 1073741824)
			Log::warning("The table " . $name .
				" is larger than 1GB, the import process may take up to several days");
		elseif($size > 524288000)
			Log::warning("The table " . $name .
				" is larger than 500MB, the import process may take up to several hours");

		// Load table schema
		exec("/usr/bin/tar --extract -f " .
			escapeshellarg($cmdParameters['import']) . " --to-stdout " .
			escapeshellarg("*/mysql/" . $name . ".create") . " | /usr/bin/mysql 2>/dev/null");

		// Load table data
		exec("/usr/bin/tar --extract -f " .
			escapeshellarg($cmdParameters['import']) . " --to-stdout " .
			escapeshellarg("*/mysql/" . $name . ".sql") . " | /usr/bin/mysql -D " .
			escapeshellarg($name) . " 2>/dev/null");

	}

} elseif($sqlPrivCount > 0) {

	Log::warning("MariaDB is not enabled - ignoring database privileges and schemas");

}

