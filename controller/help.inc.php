<?php
/**
 * Aetolos - Help
 *
 * Display help for command-line parameters and arguments
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage help
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Display logo
echo chr(27) . "[34;1mAetolos" . chr(27) . "[0m v" . AET_VER . " " . AET_COPYRIGHT . ", " . AET_LICENSE . "\n";

// Premature exit for version only
if(
	isset($cmdParameters['V']) ||
	isset($cmdParameters['version'])
)
	exit();

echo "\nUsage: aetolos [GENERAL OPTION] [--module=[MODULE] [OPTION]...]\n\n";

// Array of help parameters
$help = array(
	"GENERAL:",
	array('short' => "V",	'long' => "version",			'desc' => "Display version information only"),
	array('short' => "h",	'long' => "help",			'desc' => "Display help about parameters"),
	array('short' => "v",	'long' => "verbose",			'desc' => "Enable verbose output to stdout"),
	array('short' => "j",	'long' => "json",			'desc' => "JSON format for all output"),
	array('short' => "q",	'long' => "quiet",			'desc' => "Disable all output"),
	array('short' => "s",	'long' => "setup",			'desc' => "Perform system setup for all modules"),
	array('short' => "",	'long' => "status",			'desc' => "System and module status"),
	"",
	"IMPORT/EXPORT:",
	array('short' => "i",	'long' => "import=[FILE]",		'desc' => "Import Aetolos or cPanel backup file"),
	array('short' => "e",	'long' => "export=[FILE]",		'desc' => "Export to an Aetolos backup file"),
	array('short' => "",	'long2' => "virtualhost=[DOMAIN]",	'desc' => "Select the virtual host to export/backup"),
	array('short' => "",	'long2' => "extradb=[DB[,DB]]",		'desc' => "Provide additional databases, comma separated"),
	"",
	"MODULES:",
	array('short' => "",	'long' => "enable=[MODULE]",		'desc' => "Enable a module"),
	array('short' => "",	'long' => "disable=[MODULE]",		'desc' => "Disable a module"),
	array('short' => "",	'long' => "start=[MODULE]",		'desc' => "Start a module via systemctl"),
	array('short' => "",	'long' => "stop=[MODULE]",		'desc' => "Stop a module via systemctl"),
	array('short' => "r",	'long' => "reconfigure=[MODULE]",	'desc' => "Reconfigure/regenerate module config files"),
	"  MODULE = " . implode(", ", array_keys(Config::$modules)),
	""
);

// Merge help from loadable modules
foreach(Config::$modules as $moduleName => &$module) {

	// Skip disabled modules
	if(Config::read($moduleName, true) !== "enabled")
		continue;

	// Append to help parameter array
	$help = array_merge($help, $module->help());

}

// Display help
foreach($help as $h) {

	if(is_string($h))
		echo $h . "\n";
	elseif(!empty($h['short']))
		echo "  -" . $h['short'] . ",  --" . str_pad($h['long'], 40, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";
	elseif(isset($h['long2']))
		echo "           --" . str_pad($h['long2'], 36, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";
	elseif(isset($h['long3']))
		echo "               --" . str_pad($h['long3'], 32, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";
	else
		echo "       --" . str_pad($h['long'], 40, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";

}

