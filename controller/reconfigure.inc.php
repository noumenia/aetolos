<?php
/**
 * Aetolos - Reconfigure
 *
 * Reconfigure module
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage reconfigure
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['reconfigure'])
)
	die("No Access");

// Reconfigure module
Config::$modules[$cmdParameters['reconfigure']]->saveConfiguration();

