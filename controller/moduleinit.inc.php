<?php
/**
 * Aetolos - Module Initialization
 *
 * Initialize loadable modules
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage moduleinit
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Detect deprecated module directories
if(is_dir(dirname(__DIR__) . "/modules/centos7/")) {

	Log::error("A deprecated module directory was found (centos7), please migrate the templates.d files" .
		" to the el7 module directory and delete the centos7 directory and its contents.");
	exit(9);

} elseif(is_dir(dirname(__DIR__) . "/modules/centos8/")) {

	Log::error("A deprecated module directory was found (centos8), please migrate the templates.d files" .
		" to the el8 module directory and delete the centos8 directory and its contents.");
	exit(9);

} elseif(is_dir(dirname(__DIR__) . "/modules/el7/roundcube/")) {

	Log::error("A deprecated module directory was found (el7/roundcube), please delete this directory as" .
		" roundcube for CentOS 7 has been deprecated.");
	exit(9);

}

// Module directory based on distro/version
$modulesDir = dirname(__DIR__) . "/modules/" . Config::read("distro") . "/";
if(!is_dir($modulesDir)) {

	Log::error("Module directory not found: " . $modulesDir);
	exit(9);

}

// Scan the modules directory
$scanDir = scandir($modulesDir);
if($scanDir === false) {

	Log::error("Could not scan the module directory: " . $modulesDir);
	exit(9);

}

// Load module names/directories
$moduleNames = array_diff($scanDir, array("..", "."));

// Load objects
foreach($moduleNames as &$m) {

	// Instantiate Module
	$module = new $m();

	if(!$module instanceof Module) {

		Log::error("Error loading module: " . $m);
		exit(9);

	}

	Config::$modules[$m] = $module;

}

