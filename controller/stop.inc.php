<?php
/**
 * Aetolos - Stop
 *
 * Stop module
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage stop
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['stop'])
)
	die("No Access");

// Stop module
Config::$modules[$cmdParameters['stop']]->stop();

