<?php
/**
 * Aetolos - Start
 *
 * Start module
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage start
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['start'])
)
	die("No Access");

// Start module
Config::$modules[$cmdParameters['start']]->start();

