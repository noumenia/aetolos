<?php
/**
 * Aetolos - Database Initialization
 *
 * Initialize configuration database
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage databaseinit
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Open a database connection
$rc = DbFactory::open();
if($rc === false) {

	Log::error("Encountered an error while opening Aetolos SQLite3 configuration");
	exit(9);

}

// Connection check
if(DbFactory::$db->connected !== true) {

	Log::error("Invalid database connection");
	return false;

}

// Ensure the database has been properly initialized
$rc = Config::initDatabase();
if(
	$rc === false ||
	AET_DB_VER !== Config::read("aetolos|dbversion")
) {

	// Perform database setup
	$rc = Config::setupDatabase();
	if($rc === false) {

		Log::error("Encountered an error while configuring Aetolos SQLite3 configuration");
		exit(9);

	}

	// Re-try
	$rc = Config::initDatabase();
	if($rc === false) {

		Log::error("Encountered an error while loading the Aetolos SQLite3 configuration");
		exit(9);

	}

}

