<?php
/**
 * Aetolos - MariaDB helper functions
 *
 * Generate a dynamic configuration based on system resources
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage mariadbhelper
 */

/**
 * MariaDB helper class
 *
 * @package aetolos
 * @subpackage mariadbhelper
 */
class MariadbHelper {

	/**
	 * Dynamic server buffers
	 * @var array<string, int>
	 */
	public static $serverBuffers = array();

	/**
	 * Generate dynamic config parameters based on total system memory
	 * @return array<int, array<string, int>>
	 */
	public static function generateDynamicConfig()
	{

		// Default MariaDB memory structure (MariaDB default values)
		$perThreadMemory = array(
			'read_buffer_size'	=> 131072,
			'read_rnd_buffer_size'	=> 262144,
			'sort_buffer_size'	=> 2097152,
			'thread_stack'		=> 299008,
			'max_allowed_packet'	=> 16777216,
			'join_buffer_size'	=> 262144
		);

		// If already run, return results
		if(sizeof(self::$serverBuffers) > 0)
			return array($perThreadMemory, self::$serverBuffers);

		Log::info("System memory: " . intval(Config::read("memtotal") / 1024 / 1024) . "MB");

		// Memory usage
		$memoryUsage = intval(Config::read("mariadb|memoryUsage"));

		// Allocate system memory to MariaDB
		$maxMemoryUsage = intval(Config::read("memtotal") * ($memoryUsage / 100));

		Log::info("Allocate " . $memoryUsage . "% of system memory to MariaDB: " . intval($maxMemoryUsage / 1024 / 1024) . "MB");

		// Warn about low memory situations
		if($maxMemoryUsage < 384000000)
			Log::warning("The available memory to MariaDB is less than 384MB which can lead to memory problems. Since MariaDB" .
				" v10.3.6 the maximum number of connnections has a minimum of 10 connections, which can lead to memory overuse.");

		// 70% of the above usage goes to total thread memory
		$maxThreadUsage = intval($maxMemoryUsage * 0.70);

		Log::info("70% of the above usage goes to total thread memory: " . intval($maxThreadUsage / 1024 / 1024) . "MB");

		// The remaining 30% goes to server buffers
		$maxBufferUsage = $maxMemoryUsage - $maxThreadUsage;

		Log::info("The rest 30% goes to server buffers: " . intval($maxBufferUsage / 1024 / 1024) . "MB");

		// Maximum number of concurrent connections based on thread usage
		$maxConnections = intval(floor($maxThreadUsage / array_sum($perThreadMemory)));

		// Set a minimum limit for connections
		if($maxConnections < 10) {

			// Minimum value for max connections (as per MariaDB v10.3.6 specifications)
			// @see https://mariadb.com/docs/reference/mdb/system-variables/max_connections/
			$maxConnections = 10;

		}

		Log::info("MariaDB memory per connection: " . intval(array_sum($perThreadMemory) / 1024 / 1024) . "MB");
		Log::info("MariaDB max connections: " . $maxConnections);

		// Table size, typical usage is 1% of the buffer memory
		$tmpTableSize = intval($maxBufferUsage * 0.01);

		// With a minimum value of 16MB
		if($tmpTableSize < 16777216)
			$tmpTableSize = 16777216;

		Log::info("MariaDB temporary table size: " . intval($tmpTableSize / 1024 / 1024) . "MB");

		// Get the allocated MariaDB memory in GB
		$gb = intval($maxMemoryUsage / 1024 / 1024 / 1024, 2);
		if($gb < 1)
			$gb = 1;

		// Query cache, 16MB per GB of memory
		$queryCache = intval((16 * $gb) * 1024 * 1024);

		// Set a 50MB maximum query cache
		if($queryCache > 52428800)
			$queryCache = 52428800;

		Log::info("MariaDB query cache: " . intval($queryCache / 1024 / 1024) . "MB");

		// Calculate the MyISAM index buffer, use a small value because the default storage engine is InnoDB
		$myisamBuffer = intval($maxBufferUsage / 10);

		// Limit the maximum MyISAM index buffer to 134MB (default value)
		if($myisamBuffer > 134217728)
			$myisamBuffer = 134217728;

		// Default MariaDB memory structure
		self::$serverBuffers = array(
			'max_connections'			=> $maxConnections,
			'key_buffer_size'			=> $myisamBuffer,		// MyISAM tables only
			'tmp_table_size'			=> $tmpTableSize,		// 1% of usable buffer memory
			'max_heap_table_size'			=> $tmpTableSize,		// Same as tmp_table_size
			'innodb_buffer_pool_size'		=> $maxBufferUsage,		// InnoDB tables only
			'innodb_log_buffer_size'		=> 16777216,			// MariaDB default value
			'query_cache_size'			=> $queryCache,			// 16MB per GB of memory
			'query_cache_limit'			=> intval($queryCache / 2),	// Query size limit
			'aria_pagecache_buffer_size'		=> intval($maxBufferUsage / 2)	// Aria, system, and temporary tables
		);

		return array($perThreadMemory, self::$serverBuffers);

	}

}

