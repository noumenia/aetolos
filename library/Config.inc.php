<?php
/**
 * Aetolos - Config
 *
 * System configuration parameters.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage config
 */

/**
 * Config database class
 *
 * Adds the database-related functions to manage the global configuration.
 *
 * @package aetolos
 * @subpackage config
 */
class Config {

	/**
	 * Global configuration table
	 * @var string
	 */
	private static $schemaConfig = "CREATE TABLE IF NOT EXISTS `config` (
						Id INTEGER PRIMARY KEY AUTOINCREMENT,
						Config VARCHAR(50) NOT NULL UNIQUE,
						Value BLOB NOT NULL
					)";

	/**
	 * Global virtual host table
	 * @var string
	 */
	private static $schemaVirtualHost = "CREATE TABLE IF NOT EXISTS `virtualHost` (
						Id INTEGER PRIMARY KEY AUTOINCREMENT,
						Created DATETIME DEFAULT CURRENT_TIMESTAMP,
						DomainName VARCHAR(253) NOT NULL UNIQUE,
						UnixName VARCHAR(32) NOT NULL,
						DbPrefix VARCHAR(8) NOT NULL,
						IpAddress VARCHAR(39) NOT NULL,
						Home VARCHAR(255) NOT NULL,
						DomainZoneVersion INTEGER DEFAULT 1,
						AdminEmail VARCHAR(255),
						Quota INTEGER DEFAULT 0,
						BandwidthLimit INTEGER,
						MaxEmails INTEGER,
						MaxDatabases INTEGER,
						MaxSubDomains INTEGER,
						MaxParkedDomains INTEGER,
						ParkedUnder INTEGER,
						PrefixAlias VARCHAR(32) DEFAULT 'www',
						FOREIGN KEY(ParkedUnder) REFERENCES virtualHost(Id)
					)";

	/**
	 * Global DNS table
	 * @var string
	 */
	private static $schemaVirtualHostNs = "CREATE TABLE IF NOT EXISTS `virtualHostNs` (
						VirtualHost_ID INTEGER NOT NULL,
						DomainName VARCHAR(253) NOT NULL
					)";

	/**
	 * Global MX table
	 * @var string
	 */
	private static $schemaVirtualHostMx = "CREATE TABLE IF NOT EXISTS `virtualHostMx` (
						VirtualHost_ID INTEGER NOT NULL,
						DomainName VARCHAR(253) NOT NULL
					)";

	/**
	 * Global HAproxy table
	 * @var string
	 */
	private static $schemaHaproxyServer = "CREATE TABLE IF NOT EXISTS `haproxyServer` (
						Server VARCHAR(253) PRIMARY KEY NOT NULL,
						Parameters VARCHAR(253) DEFAULT ''
					)";

	/**
	 * Global modules array
	 * @var array<string, Module>
	 */
	public static $modules = array();

	/**
	 * Global configuration array
	 * @var array<string, int|bool|string>
	 */
	public static $configuration = array();

	/**
	 * Global log array
	 * @var array<string>
	 */
	public static $log = array();

	/**
	 * Set global configuration array with default values
	 * @return void
	 */
	public static function setDefaults()
	{

		Log::debug("Set configuration default values");

		// Set global configuration array with default values
		self::$configuration = array(
			'version'	=> AET_VER,
			'dbversion'	=> AET_DB_VER,
			'verbose'	=> false,
			'json'		=> false,
			'database'	=> "sqlite:" . dirname(__DIR__) . "/database/database.sqlite3",
			'memtotal'	=> 0,
			'cpucores'	=> 1,
			'systemd'	=> false,
			'proxy'		=> "",
			'daemon'	=> false,
			'pmanager'	=> "",
			'timezone'	=> ""
		);

	}

	/**
	 * Detect running system
	 * @return void
	 */
	public static function detectSystem()
	{

		Log::debug("Check running system");

		// Check file existence
		if(!is_file("/etc/os-release")) {

			Log::error("The system file /etc/os-release was not found");
			exit(9);

		}

		// Load contents into an array
		$osRelease = file("/etc/os-release", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		if($osRelease === false) {

			Log::error("The system file /etc/os-release could not be read");
			exit(9);

		}

		$osRelease = array_filter(array_map(function ($line) {

			// Skip invalid lines
			if(strpos($line, "=") === false)
				return false;

			// Remove quotes
			$line = str_replace(array("\"", "'"), "", $line);

			// Split string by '=' into an array
			$line = explode("=", $line);

			// Too few or too many array elements
			if(sizeof($line) !== 2)
				return false;

			// Trim white-space
			$line = array_map("trim", $line);

			return $line;

		}, $osRelease));

		// Loop os-release lines
		foreach($osRelease as &$line) {

			// Use only the ones we need
			switch($line[0]) {

				case "ID":
					self::$configuration['osystem'] = $line[1];
					break;

				case "VERSION_ID":
					// Double conversion to get the major version only
					self::$configuration['oversion'] = strval(intval($line[1]));
					break;

				case "PRETTY_NAME":
					self::$configuration['opname'] = $line[1];
					break;

			}

		}

		// Missing lines from os-release or we are running on a foreign system
		if(!isset(self::$configuration['osystem'], self::$configuration['oversion'], self::$configuration['opname'])) {

			Log::error("Could not determine system type and version");
			exit(9);

		}

		// Operating system check
		if(
			self::$configuration['osystem'] !== "fedora" &&
			self::$configuration['osystem'] !== "almalinux" &&
			self::$configuration['osystem'] !== "rocky" &&
			self::$configuration['osystem'] !== "ol"
		) {

			Log::error("Unsupported operating system");
			exit(9);

		}

		// Fedora
		if(
			self::$configuration['osystem'] === "fedora" &&
			self::$configuration['oversion'] < 34
		) {

			Log::error("Unsupported Fedora version");
			exit(9);

		}

		// AlmaLinux
		if(
			self::$configuration['osystem'] === "almalinux" &&
			self::$configuration['oversion'] !== "8" &&
			self::$configuration['oversion'] !== "9"
		) {

			Log::error("Unsupported AlmaLinux version");
			exit(9);

		}

		// Rocky
		if(
			self::$configuration['osystem'] === "rocky" &&
			self::$configuration['oversion'] !== "8" &&
			self::$configuration['oversion'] !== "9"
		) {

			Log::error("Unsupported Rocky version");
			exit(9);

		}

		// Oracle
		if(
			self::$configuration['osystem'] === "ol" &&
			self::$configuration['oversion'] !== "8" &&
			self::$configuration['oversion'] !== "9"
		) {

			Log::error("Unsupported Oracle Linux version");
			exit(9);

		}

		if(self::$configuration['osystem'] === "fedora")
			// Create a distro ID tag based on the system name (Fedora only)
			self::$configuration['distro'] = self::$configuration['osystem'];
		else
			// Create a distro ID tag by combining the system name and version
			self::$configuration['distro'] = "el" . self::$configuration['oversion'];

		Log::debug("Detected: " . self::$configuration['opname']);

	}

	/**
	 * Detect system memory
	 * @return void
	 */
	public static function detectMemory()
	{

		Log::debug("Check system memory");

		// Check file existence
		if(!is_file("/proc/meminfo")) {

			Log::error("The system file /proc/meminfo was not found");
			exit(9);

		}

		$memInfo = file_get_contents("/proc/meminfo");
		if($memInfo === false) {

			Log::error("The system file /proc/meminfo could not be read");
			exit(9);

		}

		// Matches is being used by reference
		$matches = null;

		// Extract MemTotal string
		$rc = preg_match('/MemTotal: *?(\d+) kB/', $memInfo, $matches);
		if(
			$rc !== 1 ||
			!isset($matches[1]) ||
			!is_numeric($matches[1])
		) {

			Log::error("Could not detect MemTotal from /proc/meminfo");
			exit(9);

		}

		// Convert kB to bytes
		$matches[1] = intval($matches[1]) * 1024;

		// Minimum memory (256MB)
		if($matches[1] < 268435456) {

			Log::warning("Detected memory (" . floor($matches[1] / 1024 / 1024) . "MiB)" .
				" is less than the advised minimum of 256MB");

			// Store the minimum default memory
			self::$configuration['memtotal'] = 268435456;

		} else {

			// Store the detected memory
			self::$configuration['memtotal'] = $matches[1];

			Log::debug("Detected: " . floor($matches[1] / 1024 / 1024) . "MiB");

		}

	}

	/**
	 * Detect CPU cores
	 * @return void
	 */
	public static function detectCores()
	{

		Log::debug("Check CPU cores");

		// Start with no cores
		$coreCount = 0;

		// Check file existence
		if(is_file("/usr/bin/nproc")) {

			$nproc = array();
			exec("/usr/bin/nproc", $nproc);
			if(
				isset($nproc[0]) &&
				is_numeric($nproc[0])
			)
				// Extract core count
				$coreCount = intval($nproc[0]);

		} elseif(is_file("/proc/cpuinfo")) {

			$cpuinfo = file_get_contents("/proc/cpuinfo");
			if($cpuinfo !== false) {

				// Matches is being used by reference
				$matches = null;

				// Extract siblings string
				$rc = preg_match('/siblings *?: (\d+)/', $cpuinfo, $matches);
				if(
					$rc === 1 &&
					isset($matches[1]) &&
					is_numeric($matches[1])
				)
					// Extract core count
					$coreCount = intval($matches[1]);

			}

		}

		// Check for a valid core count
		if($coreCount < 1) {

			Log::warning("Could not detect number of CPU cores");

			// Fail-safe in case of error
			$coreCount = 1;

		}

		// Store the detected CPU core count
		self::$configuration['cpucores'] = $coreCount;

		Log::debug("Detected: " . $coreCount);

	}

	/**
	 * Detect systemd
	 * @return void
	 */
	public static function detectSystemD()
	{

		Log::debug("Check systemd");

		// Define process 1 status file
		$procOne = "/proc/1/status";

		if(
			!is_file($procOne) ||
			!is_readable($procOne)
		) {

			Log::debug("Detected: other");

			return;

		}

		// Get process 1 status
		$statusOne = file_get_contents($procOne);
		if($statusOne === false) {

			Log::debug("Detected: other");

			return;

		}

		$rc = preg_match('/Name:(.*)/i', $statusOne, $matches);
		if(
			$rc !== 1 ||
			!isset($matches[1])
		) {

			Log::debug("Detected: other");

			return;

		}

		// Trim and clean
		$matches[1] = trim($matches[1]);

		if($matches[1] === "systemd") {

			Log::debug("Detected: systemd");

			// Set systemd to true
			self::$configuration['systemd'] = true;

		} else {

			Log::debug("Detected: other");

		}

	}

	/**
	 * Detect proxy
	 * @return void
	 */
	public static function detectProxy()
	{

		Log::debug("Check proxy");

		// Select the right package management configuration
		$conf = "/etc/dnf/dnf.conf";

		// Fail safe
		if(!is_file($conf))
			return;

		$conf = file_get_contents($conf);
		if($conf === false)
			return;

		$rc = preg_match('/^proxy=(.*?)$/im', $conf, $matches);
		if(
			$rc !== 1 ||
			!isset($matches[1])
		) {

			Log::debug("Detected: no proxy");
			return;

		}

		// Store the detected proxy
		self::$configuration['proxy'] = $matches[1];

		Log::debug("Detected: " . $matches[1]);

	}

	/**
	 * Detect package manager
	 * @return void
	 */
	public static function detectPackageManager()
	{

		Log::debug("Check package manager");

		// Scan for yum, microdnf and dnf
		if(
			is_file("/usr/bin/yum") &&
			!is_link("/usr/bin/yum") &&
			is_executable("/usr/bin/yum")
		)
			self::$configuration['pmanager'] = "/usr/bin/yum";
		elseif(
			is_file("/usr/bin/microdnf") &&
			is_executable("/usr/bin/microdnf")
		)
			self::$configuration['pmanager'] = "/usr/bin/microdnf";
		elseif(
			is_file("/usr/bin/dnf") &&
			is_executable("/usr/bin/dnf")
		)
			self::$configuration['pmanager'] = "/usr/bin/dnf";
		else {

			Log::error("Could not detect a suitable package manager");
			exit(9);

		}

		Log::debug("Detected: " . self::$configuration['pmanager']);

	}

	/**
	 * Detect system timezone
	 * @return void
	 */
	public static function detectTimezone()
	{

		Log::debug("Check system timezone");

		// Start with an empty timezone
		$timezone = "";

		// Try using timedatectl (requires systemd)
		$timedatectl = array();
		exec("/usr/bin/timedatectl 2>/dev/null", $timedatectl, $rc);
		if(
			$rc === 0 &&
			sizeof($timedatectl) > 0
		) {

			// Loop timedatectl output
			foreach($timedatectl as $t) {

				// Look for the timezone string
				if(strpos($t, "Time zone") !== false) {

					// Extract timezone
					$rc = preg_match('/Time zone: (.*) \(/U', $t, $matches);
					if(
						$rc === 1 &&
						isset($matches[1])
					) {

						// Match found, store and break from loop
						$timezone = trim($matches[1]);
						break;

					}

				}

			}

		}

		// No match so far... try using localtime
		if(
			empty($timezone) &&
			is_file("/etc/localtime") &&
			is_link("/etc/localtime")
		) {

			// Get the target of the symbolic link
			$rc = readlink("/etc/localtime");
			if($rc !== false) {

				$rc =  preg_match('/\/zoneinfo\/(.*)/', $rc, $matches);
				if(
					$rc === 1 &&
					isset($matches[1])
				)
					$timezone = $matches[1];

			}

		}

		// Failsafe
		if(empty($timezone)) {

			Log::warning("Could not detect the system timezone, using UTC");

			$timezone = "UTC";

		}

		// Set PHP timezone
		date_default_timezone_set($timezone);

		// Store the detected timezone
		self::$configuration['timezone'] = $timezone;

		Log::debug("Detected: " . self::$configuration['timezone']);

	}

	/**
	 * Setup configuration database, handle versioning and upgrades
	 * @return bool
	 */
	public static function setupDatabase()
	{

		Log::debug("Checking Aetolos database");

		// Global configuration table
		$rc = DbFactory::$db->query(self::$schemaConfig);
		if($rc === false) {

			Log::error("Error while creating table: config");
			return false;

		}

		// Verify that the table was created successfully
		DbFactory::$db->query("SELECT 1 FROM `sqlite_master` WHERE type='table' AND name='config'");
		DbFactory::$db->nextRow();
		if(
			!isset(DbFactory::$db->row[0]) ||
			DbFactory::$db->row[0] !== "1"
		) {

			Log::error("Error while checking table: config");
			return false;

		}

		// Check database version or check for an empty/new database
		DbFactory::$db->query("SELECT Value FROM `config` WHERE Config='aetolos|dbversion'");
		DbFactory::$db->nextRow();
		if(DbFactory::$db->row === false) {

			// New database, set default values
			$config = array();

			// Aetolos
			$config['aetolos|version']		= AET_VER;
			$config['aetolos|dbversion']		= AET_DB_VER;

			// Smarty
			$config['smarty|templateDirectory']	= dirname(__DIR__) . "/modules";
			$config['smarty|compileDirectory']	= dirname(__DIR__) . "/smarty/templates_c";
			$config['smarty|cacheDirectory']	= dirname(__DIR__) . "/smarty/cache";
			$config['smarty|configDirectory']	= dirname(__DIR__) . "/smarty/configs";
			$config['smarty|pluginsDirectory']	= dirname(__DIR__) . "/smarty/libs/plugins";
			$config['smarty|cacheLifetime']		= 14400;
			$config['smarty|debug']			= false;

			// PKI/TLS
			$config['pkitls|directoryCerts']	= "/etc/pki/tls/certs";
			$config['pkitls|directoryPrivate']	= "/etc/pki/tls/private";

			// Merge default parameters from loadable modules
			foreach(self::$modules as &$m)
				$config = array_merge($config, $m->parameters());

			// Write to global configuration and save to database
			foreach($config as $key => $value)
				self::write($key, $value);

			// Virtual host configuration table
			$rc = DbFactory::$db->query(self::$schemaVirtualHost);
			if($rc === false) {

				Log::error("Error while creating table: virtualHost");
				return false;

			}

			// DNS configuration table
			$rc = DbFactory::$db->query(self::$schemaVirtualHostNs);
			if($rc === false) {

				Log::error("Error while creating table: virtualHostNs");
				return false;

			}

			// Mail servers configuration table
			$rc = DbFactory::$db->query(self::$schemaVirtualHostMx);
			if($rc === false) {

				Log::error("Error while creating table: virtualHostMx");
				return false;

			}

			// HAproxy servers configuration table
			$rc = DbFactory::$db->query(self::$schemaHaproxyServer);
			if($rc === false) {

				Log::error("Error while creating table: haproxyServer");
				return false;

			}

			// Verify that the table was created successfully
			DbFactory::$db->query("SELECT 1 FROM `sqlite_master` WHERE type='table' AND name='virtualHost'");
			DbFactory::$db->nextRow();
			if(
				!isset(DbFactory::$db->row[0]) ||
				DbFactory::$db->row[0] !== "1"
			) {

				Log::error("Error while creating table: virtualHost");
				return false;

			}

		} else {

			if(
				isset(DbFactory::$db->row[0]) &&
				DbFactory::$db->row[0] !== AET_DB_VER
			) {

				// Current version number
				$ver = (int)DbFactory::$db->row[0];

				// Back to the future?
				if($ver > AET_DB_VER) {

					Log::error("The database version " . $ver .
						" is higher than the supported version " . AET_DB_VER . " of this Aetolos installation");
					return false;

				}

				// Loop while current version requires updating
				while($ver < AET_DB_VER) {

					// Version numbers
					switch($ver) {
						case 1:
							/*
							 * WTF?
							 *
							 * You are probably wondering what is the meaning of the following block of code,
							 * well so have I, like many others who have stumbled uppon this issue. The 1st
							 * iteration of the database used 'UNIQUE' on two columns that had to be removed
							 * by altering the table schema. Apparently this is not supported by SQlite3,
							 * thus the only solution was to create a temporary table, copy the rows, delete
							 * the old table, create a table with the new schema, copy the rows and finally
							 * delete the temporary table. Note to self: RTFM
							 *
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("CREATE TABLE `virtualHost_back" .
								"up` ( Id INTEGER PRIMARY KEY AUTOINCREMENT, Created" .
								" DATETIME DEFAULT CURRENT_TIMESTAMP, DomainName VAR" .
								"CHAR(253) NOT NULL UNIQUE, UnixName VARCHAR(32) NOT" .
								" NULL, DbPrefix VARCHAR(8) NOT NULL, IpAddress VARC" .
								"HAR(39) NOT NULL, Home VARCHAR(255) NOT NULL, Domai" .
								"nZoneVersion INTEGER DEFAULT 1, AdminEmail VARCHAR(" .
								"255), Quota INTEGER DEFAULT 0, BandwidthLimit INTEG" .
								"ER, MaxEmails INTEGER, MaxDatabases INTEGER, MaxSub" .
								"Domains INTEGER, MaxParkedDomains INTEGER, ParkedUn" .
								"der INTEGER, FOREIGN KEY(ParkedUnder) REFERENCES vi" .
								"rtualHost(Id) )");
							$rc2 = DbFactory::$db->query("INSERT INTO `virtualHost_backu" .
								"p` SELECT *,null FROM `virtualHost`");
							$rc3 = DbFactory::$db->query("DROP TABLE `virtualHost`");
							$rc4 = DbFactory::$db->query("CREATE TABLE `virtualHost` ( I" .
								"d INTEGER PRIMARY KEY AUTOINCREMENT, Created DATETI" .
								"ME DEFAULT CURRENT_TIMESTAMP, DomainName VARCHAR(25" .
								"3) NOT NULL UNIQUE, UnixName VARCHAR(32) NOT NULL, " .
								"DbPrefix VARCHAR(8) NOT NULL, IpAddress VARCHAR(39)" .
								" NOT NULL, Home VARCHAR(255) NOT NULL, DomainZoneVe" .
								"rsion INTEGER DEFAULT 1, AdminEmail VARCHAR(255), Q" .
								"uota INTEGER DEFAULT 0, BandwidthLimit INTEGER, Max" .
								"Emails INTEGER, MaxDatabases INTEGER, MaxSubDomains" .
								" INTEGER, MaxParkedDomains INTEGER, ParkedUnder INT" .
								"EGER, FOREIGN KEY(ParkedUnder) REFERENCES virtualHo" .
								"st(Id) )");
							$rc5 = DbFactory::$db->query("INSERT INTO `virtualHost` SELE" .
								"CT * FROM `virtualHost_backup`");
							$rc6 = DbFactory::$db->query("DROP TABLE `virtualHost_backup`");
							if(
								$rc1 === true &&
								$rc2 === true &&
								$rc3 === true &&
								$rc4 === true &&
								$rc5 === true &&
								$rc6 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='2' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 2:
							/*
							 * OpenDKIM configuration parameters
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('opendkim', 'enabled')");
							$rc2 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('opendkim|opendkimConfFile', '/etc/opendkim.conf')");
							$rc3 = DbFactory::$db->query("UPDATE `config` SET Value='php" .
								"|opendkim|nsd|clamav|spamassassin|postfix|dovecot|m" .
								"ariadb|roundcube|apache' WHERE Config='aetolos|features'");
							if(
								$rc1 === true &&
								$rc2 === true &&
								$rc3 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='3' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 3:
							/*
							 * Spamassassin configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('spamassassin|initPreFile" .
								"', '/etc/mail/spamassassin/init.pre')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='4' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 4:
							/*
							 * Spamassassin configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('spamassassin|milterFile'" .
								", '/etc/sysconfig/spamass-milter')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='5' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 5:
							/*
							 * Roundcubemail configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('roundcube|roundcubeDirec" .
								"tory', '/etc/roundcubemail')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='6' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 6:
							/*
							 * Postgrey configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('postgrey', 'disabled')");
							$rc2 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('postgrey|postgreyConfFil" .
								"e', '/etc/sysconfig/postgrey')");
							$rc3 = DbFactory::$db->query("UPDATE `config` SET Value='php" .
								"|opendkim|postgrey|nsd|clamav|spamassassin|postfix|" .
								"dovecot|mariadb|roundcube|apache' WHERE Config='aet" .
								"olos|features'");
							if(
								$rc1 === true &&
								$rc2 === true &&
								$rc3 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='7' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 7:
							/*
							 * Remove unused configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("DELETE FROM `config` WHERE Config='aetolos|features'");
							$rc2 = DbFactory::$db->query("INSERT OR REPLACE INTO `config`" .
								" (Config, Value) VALUES ('php', 'enabled')");
							$rc3 = DbFactory::$db->query("UPDATE `config` SET Value='" .
								dirname(__DIR__) . "/modules' WHERE Config='smarty|templateDirectory'");
							$rc4 = DbFactory::$db->query("INSERT OR REPLACE INTO `config`" .
								" (Config, Value) VALUES ('virtualhost', 'enabled')");
							if(
								$rc1 === true &&
								$rc2 === true &&
								$rc3 === true &&
								$rc4 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='8' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 8:
							/*
							 * Create HAproxy table
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query(self::$schemaHaproxyServer);
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='9' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 9:
							/*
							 * Apache configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config`" .
								" (Config, Value) VALUES ('apache|listenIp', '')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='10' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 10:
							/*
							 * Create HAproxy default parameters
							 */
							$rc1 = self::$modules['haproxy']->parameters();
							foreach($rc1 as $key => $value)
								self::write($key, $value);
							DbFactory::$db->query("UPDATE `config` SET Value='11' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 11:
							/*
							 * Create Dehydrated default parameters
							 */
							$rc1 = self::$modules['dehydrated']->parameters();
							foreach($rc1 as $key => $value)
								self::write($key, $value);
							DbFactory::$db->query("UPDATE `config` SET Value='12' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 12:
							/*
							 * Clamav-milter configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config`" .
								" (Config, Value) VALUES ('clamav|milterSelinuxModule', '')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='13' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 13:
							/*
							 * Create Apache24us and Php72us default parameters
							 */

							// EL7 specific modules
							if(Config::read("distro") === "el7") {

								$rc1 = self::$modules['apache24us']->parameters();
								foreach($rc1 as $key => $value) {

									// Ignore existing entries created by the Apache module
									if(self::read($key, true) === false)
										self::write($key, $value);

								}

								$rc2 = self::$modules['php72us']->parameters();
								foreach($rc2 as $key => $value)
									self::write($key, $value);

							}

							DbFactory::$db->query("UPDATE `config` SET Value='14' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 14:
							/*
							 * Create MariaDB default parameters
							 */
							$rc1 = self::$modules['mariadb']->parameters();

							// Ignore existing entry
							if(self::read("mariadb|limitnoFile", true) === false)
								self::write("mariadb|limitnoFile", $rc1['mariadb|limitnoFile']);

							DbFactory::$db->query("UPDATE `config` SET Value='15' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 15:
							/*
							 * Add optional domain prefix alias
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("ALTER TABLE `virtualHost` ADD" .
								" COLUMN PrefixAlias VARCHAR(32) DEFAULT 'www'");
							$rc2 = DbFactory::$db->query("UPDATE `virtualHost` SET PrefixAlias='www'");
							if(
								$rc1 === true &&
								$rc2 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='16' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 16:
							/*
							 * Create Php and Php72us default parameters
							 */
							$rc1 = self::$modules['php']->parameters();
							self::write("php|opcacheFile", $rc1['php|opcacheFile']);
							if(Config::read("distro") === "el7") {

								$rc2 = self::$modules['php72us']->parameters();
								self::write("php72us|opcacheFile", $rc2['php72us|opcacheFile']);

							}
							DbFactory::$db->query("UPDATE `config` SET Value='17' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 17:
							/*
							 * Create opendkim default parameters
							 */
							if(Config::read("opendkim", true) === false) {

								$rc1 = self::$modules['opendkim']->parameters();
								foreach($rc1 as $key => $value)
									self::write($key, $value);

							}
							DbFactory::$db->query("UPDATE `config` SET Value='18' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 18:
							/*
							 * Php configuration parameter
							 */
							if(
								Config::read("distro") === "el8" ||
								Config::read("distro") === "fedora"
							) {

								$rc1 = self::$modules['php']->parameters();
								self::write("php|override", $rc1['php|override']);

							}
							DbFactory::$db->query("UPDATE `config` SET Value='19' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 19:
							/*
							 * Create opendmarc default parameters
							 */
							$rc1 = self::$modules['opendmarc']->parameters();
							foreach($rc1 as $key => $value)
								self::write($key, $value);
							DbFactory::$db->query("UPDATE `config` SET Value='20' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 20:
							/*
							 * Create Apache default parameters
							 */
							if(
								Config::read("distro") === "el8" ||
								Config::read("distro") === "fedora"
							) {

								$rc1 = self::$modules['apache']->parameters();
								self::write("apache|logrotateDHttpd", $rc1['apache|logrotateDHttpd']);

							}
							DbFactory::$db->query("UPDATE `config` SET Value='21' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 21:
							/*
							 * MariaDB configuration parameter
							 */
							DbFactory::$db->conn->beginTransaction();
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('mariadb|memoryUsage', '50')");
							if($rc1 === true) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='22' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 22:
							/*
							 * Create MTA-STS default parameters
							 */
							if(
								Config::read("distro") === "el8" ||
								Config::read("distro") === "fedora"
							) {

								$rc1 = self::$modules['mtasts']->parameters();
								foreach($rc1 as $key => $value)
									self::write($key, $value);

							}
							DbFactory::$db->query("UPDATE `config` SET Value='23' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

						case 23:
							/*
							 * PHP-FPM configuration parameters
							 */
							DbFactory::$db->conn->beginTransaction();
							if(self::read("apache", true) === "enabled")
								$phpfpm = "enabled";
							else
								$phpfpm = "disabled";
							$rc1 = DbFactory::$db->query("INSERT OR REPLACE INTO `config" .
								"` (Config, Value) VALUES ('phpfpm', '" . $phpfpm . "')");
							$rc2 = DbFactory::$db->query("UPDATE `config` SET Config='phpfpm|directoryFpmD'" .
								" WHERE Config='php|directoryFpmD'");
							$rc3 = DbFactory::$db->query("UPDATE `config` SET Config='phpfpm|opcacheFile'" .
								" WHERE Config='php|opcacheFile'");
							$rc4 = DbFactory::$db->query("UPDATE `config` SET Config='phpfpm|override'" .
								" WHERE Config='php|override'");
							if(
								$rc1 === true &&
								$rc2 === true &&
								$rc3 === true &&
								$rc4 === true
							) {

								DbFactory::$db->conn->commit();

							} else {

								DbFactory::$db->conn->rollBack();
								return false;

							}
							DbFactory::$db->query("UPDATE `config` SET Value='24' WHERE Config='aetolos|dbversion'");
							$ver++;
							break;

					}

				}

			}

		}

		return true;

	}

	/**
	 * Ensure the database has been properly initialized
	 * @return bool
	 */
	public static function initDatabase()
	{

		Log::debug("Initialize Aetolos database");

		// Verify by looking for the config table
		DbFactory::$db->query("SELECT 1 FROM `sqlite_master` WHERE type='table' AND name='config'");
		DbFactory::$db->nextRow();
		if(
			!isset(DbFactory::$db->row[0]) ||
			DbFactory::$db->row[0] !== "1"
		)
			return false;

		return true;

	}

	/**
	 * Read a key/value pair from the global configuration
	 * @param string $key Configuration key
	 * @param bool $ignore Ignore errors if the key does not exist
	 * @return mixed
	 */
	public static function read($key, $ignore = false)
	{

		// Return global configuration value
		if(isset(self::$configuration[$key]))
			return self::$configuration[$key];

		// Get config value from the config table
		$preped = DbFactory::$db->conn->prepare("SELECT Value FROM `config` WHERE Config=:configname");

		// Bind parameter
		$preped->bindParam(":configname", $key);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {

			if($ignore === false)
				Log::error("Error while getting config value from the database table: config");

			return false;

		}

		// Fetch result
		$result = $preped->fetchAll(PDO::FETCH_COLUMN);
		if(isset($result[0]))
			return $result[0];

		if($ignore === false)
			Log::error("Configuration key not found: " . $key);

		return false;

	}

	/**
	 * Insert or update a key/value pair in the global configuration and save to database
	 * @param string $key Configuration key
	 * @param mixed $value Configuration value
	 * @return bool
	 */
	public static function write($key, $value)
	{

		// These configuration parameters are skipped from being saved to the database, only used as runtime parameters
		$skip = array(
			"version",
			"dbversion",
			"verbose",
			"json",
			"database",
			"memtotal",
			"cpucores",
			"proxy",
			"daemon",
			"pmanager",
			"timezone",
			"distro"
		);

		if(!in_array($key, $skip)) {

			// Prepare statement
			$preped = DbFactory::$db->conn->prepare("INSERT OR REPLACE INTO `config` (Value, Config)" .
				" VALUES (:value, :config)");

			// Bind parameter
			$preped->bindParam(":value", $value);
			$preped->bindParam(":config", $key);

			// Execute prepared statement
			$rc = $preped->execute();
			if($rc === false) {

				Log::error("Error while writing to database table: config");
				return false;

			}

			return true;

		}

		// Set variable
		self::$configuration[$key] = $value;

		return true;

	}

	/**
	 * Generate a hash from a plain text password, default algorithm is SHA512-CRYPT
	 * @param string $password Plain text password
	 * @return string
	 */
	public static function passwordHash($password)
	{

		// Remove newlines
		$password = str_replace("\n", "", $password);

		// Use random_bytes (PHP >= 7)
		if(function_exists("random_bytes"))
			$salt = str_replace("+", ".", substr(base64_encode(random_bytes(22)), 0, 22));

		// Use mcrypt_create_iv (PHP <= 7.1 + mcrypt module)
		elseif(
			function_exists("mcrypt_create_iv") &&
			defined("MCRYPT_DEV_URANDOM")
		)
			$salt = str_replace("+", ".", substr(base64_encode(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)), 0, 22));

		// Use openssl_random_pseudo_bytes (PHP >= 5.3)
		elseif(function_exists("openssl_random_pseudo_bytes"))
			$salt = str_replace("+", ".", substr(base64_encode(openssl_random_pseudo_bytes(17)), 0, 22));

		// Failsafe for old PHP versions that do not require a salt
		else
			return "{MD5-CRYPT}" . crypt($password);

		return "{SHA512-CRYPT}" . crypt($password, "\$6\$" . $salt . "\$");

	}

	/**
	 * Calculate the size of a directory tree in bytes
	 * @param string $path Full directory path
	 * @return int
	 */
	public static function dirSize($path)
	{

		// Start at zero
		$bytes = 0;

		// Get the canonicalized absolute path
		$path = realpath($path);

		// Stop the procedure if the path is invalid
		if(
			$path === false ||
			empty($path) ||
			!is_dir($path)
		)
			return 0;

		// Loop over a recursive interator
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {

			try {

				// Add to total bytes
				$bytes += $object->getSize();

			} catch(Exception $e) {

				// No-op

			}

		}

		return $bytes;

	}

	/**
	 * Limited grep capabilities
	 * @param string $path Full directory path
	 * @param string $regexp Regular expression
	 * @return array<string, string> Array key = filename, array value = contents
	 */
	public static function grep($path, $regexp)
	{

		// Matching files
		$files = array();

		// Get the canonicalized absolute path
		$path = realpath($path);

		// Stop the procedure if the path is invalid or the regular expression is empty
		if(
			$path === false ||
			empty($path) ||
			!is_dir($path) ||
			empty($regexp)
		)
			return array();

		// Loop over a recursive interator
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {

			try {

				// Input validation and readable files only
				if(
					!$object instanceof SplFileInfo ||
					!$object->isReadable()
				)
					continue;

				// Load file contents
				$data = file_get_contents($object->getPathname());
				if($data === false)
					continue;

				// Execute regular expression match
				$rc = preg_match($regexp, $data);

				// Match found, add to files array
				if($rc === 1)
					$files[$object->getPathname()] = $data;

				// Clean-up
				unset($data);

			} catch(Exception $e) {

				// No-op

			}

		}

		return $files;

	}

	/**
	 * Return true of the supplied package repository is installed and enabled
	 * @param string $repoName Repository tag name
	 * @return bool
	 */
	public static function isRepoEnabled($repoName)
	{

		// Search for repository files matching the repository name tag
		$repoFiles = Config::grep("/etc/yum.repos.d/", '/^\[' . $repoName . '\]$/m');

		// Only one repository file should match
		foreach($repoFiles as $data) {

			// Extract matching repository block
			$rc = preg_match('/^\[' . $repoName . '\].*(\n\n|\[|\Z)/msU', $data, $matches);
			if(
				$rc === 1 &&
				isset($matches[0])
			) {

				// Extract enabled option
				$rc = preg_match('/enabled.*?=(.*)/', $matches[0], $enabled);
				if(
					$rc === 1 &&
					isset($enabled[1]) &&
					(
						$enabled[1] === "1" ||
						$enabled[1] === "True"
					)
				)
					return true;

			}

		}

		return false;

	}

	/**
	 * Enable package repository
	 * @param string $repoName Repository tag name
	 * @return bool
	 */
	public static function enableRepo($repoName)
	{

		// Search for repository files matching the repository name tag
		$repoFiles = Config::grep("/etc/yum.repos.d/", '/^\[' . $repoName . '\]$/m');

		// Only one repository file should match
		foreach($repoFiles as $file => $data) {

			// Find the position of the tag name
			$pos = strpos($data, "[" . $repoName . "]");
			if($pos === false)
				return false;

			// Find the enabled option
			$start = strpos($data, "\nenabled", $pos);
			if($start === false)
				return false;

			// Find the end of the line
			$end = strpos($data, "\n", $start + 2);
			if($end === false)
				return false;

			// Replace with our own string
			$data = substr($data, 0, $start) . "\nenabled=1" . substr($data, $end);

			// Save repository file
			$rc = file_put_contents($file, $data);
			if($rc !== false)
				return true;

		}

		return false;

	}

}

