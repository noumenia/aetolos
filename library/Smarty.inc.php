<?php
/**
 * Aetolos - Fake Smarty
 *
 * A template engine replacement for Smarty, which supports the following statements:
 * - variable `{$...}`
 * - if `{if ...}`
 * - elseif `{elseif ...}`
 * - else `{else}`
 * - foreach `{foreach ...}`
 * - continue `{continue}`
 * - literal `{literal}`
 * - assign `{assign ...}`
 * - nocache `{nocache}`
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage smarty
 */

/**
 * Fake Smarty
 *
 * @package aetolos
 * @subpackage smarty
 */
class Smarty {

	/**
	 * Smarty version
	 * @var string
	 */
	const SMARTY_VERSION = "3.1.34";

	/**
	 * Caching off
	 * @var int
	 */
	const CACHING_OFF = 0;

	/**
	 * Caching lifetime current
	 * @var int
	 */
	const CACHING_LIFETIME_CURRENT = 1;

	/**
	 * Caching lifetime saved
	 * @var int
	 */
	const CACHING_LIFETIME_SAVED = 2;

	/**
	 * Compile check off
	 * @var int
	 */
	const COMPILECHECK_OFF = 0;

	/**
	 * Compile check on
	 * @var int
	 */
	const COMPILECHECK_ON = 1;

	/**
	 * Compile check cache miss
	 * @var int
	 */
	const COMPILECHECK_CACHEMISS = 2;

	/**
	 * Template directories
	 * @var array<string>
	 */
	protected $templateDir = array("./templates/");

	/**
	 * Force compile
	 * @var bool
	 */
	public $force_compile = false;

	/**
	 * Compile check
	 * @var int
	 */
	public $compile_check = 0;

	/**
	 * Cacing enabled/disabled
	 * @var int
	 */
	public $caching = 0;

	/**
	 * Template variables
	 * @var array<string, mixed>
	 */
	private $templateVars = array();

	/**
	 * Template regular expression patterns for {$VAR}
	 * @var array<string, string>
	 */
	private $templateVarRegExType1 = array();

	/**
	 * Template regular expression patterns for $VAR
	 * @var array<string, string>
	 */
	private $templateVarRegExType2 = array();

	/**
	 * Positional chain of variables
	 * @var array<array<string, mixed>>
	 */
	private $chain = array();

	/**
	 * Template data
	 * @var string
	 */
	private $data = "";

	/**
	 * Set template directory
	 * @param string|array<string> $templateDir
	 * @param bool $isConfig
	 * @return Smarty
	 */
	public function setTemplateDir($templateDir, $isConfig = false)
	{

		if(is_string($templateDir))
			$this->templateDir = array($templateDir);
		else
			$this->templateDir = $templateDir;

		return $this;

	}

	/**
	 * Set compile directory
	 * @param string $compileDir
	 * @return Smarty
	 */
	public function setCompileDir($compileDir)
	{

		return $this;

	}

	/**
	 * Set cache directory
	 * @param string $cacheDir
	 * @return Smarty
	 */
	public function setCacheDir($cacheDir)
	{

		return $this;

	}

	/**
	 * Set config directory
	 * @param string $configDir
	 * @return Smarty
	 */
	public function setConfigDir($configDir)
	{

		return $this;

	}

	/**
	 * Set plugins directory
	 * @param string $pluginsDir
	 * @return Smarty
	 */
	public function setPluginsDir($pluginsDir)
	{

		return $this;

	}

	/**
	 * Set cache lifetime
	 * @param int $cacheLifetime
	 * @return void
	 */
	public function setCacheLifetime($cacheLifetime)
	{

		// No-op

	}

	/**
	 * Set cache lifetime
	 * @param bool $forceCompile
	 * @return void
	 */
	public function setForceCompile($forceCompile)
	{

		$this->force_compile = $forceCompile;

	}

	/**
	 * Set compile check
	 * @param int $compileCheck
	 * @return void
	 */
	public function setCompileCheck($compileCheck)
	{

		$this->compile_check = $compileCheck;

	}

	/**
	 * Set caching
	 * @param int $caching
	 * @return void
	 */
	public function setCaching($caching)
	{

		$this->caching = $caching;

	}

	/**
	 * Enable security
	 * @param string $securityClass
	 * @return Smarty
	 */
	public function enableSecurity($securityClass = null)
	{

		return $this;

	}

	/**
	 * Disable security
	 * @return Smarty
	 */
	public function disableSecurity()
	{

		return $this;

	}

	/**
	 * Get template variables
	 * @param string $varName
	 * @return mixed
	 */
	public function getTemplateVars($varName = null)
	{

		// Get all variables or a single variable, false for error
		if($varName === null)
			return $this->templateVars;
		elseif(isset($this->templateVars[$varName]))
			return $this->templateVars[$varName];
		else
			return false;

	}

	/**
	 * Assign a template variable
	 * @param string|array<string> $name
	 * @param mixed $value
	 * @param bool $noCache
	 * @return Smarty
	 */
	public function assign($name, $value = null, $noCache = false)
	{

		// Array type
		if(is_array($name)) {

			// Set multiple template variables with the same value
			foreach($name as &$n) {

				// Value
				$this->templateVars[$n] = $value;

				// Regular expression pattern type 1
				$this->templateVarRegExType1[$n] = '/\{\$' . $n . '\}/i';

				// Regular expression pattern type 2
				$this->templateVarRegExType2[$n] = '/\$' . $n . '\b/i';

			}

		} else {

			// Value
			$this->templateVars[$name] = $value;

			// Regular expression pattern type 1
			$this->templateVarRegExType1[$name] = '/\{\$' . $name . '\}/i';

			// Regular expression pattern type 2
			$this->templateVarRegExType2[$name] = '/\$' . $name . '\b/i';

		}

		return $this;

	}

	/**
	 * Assign a template variable by reference
	 * @param string $name
	 * @param mixed &$value
	 * @param bool $noCache
	 * @return Smarty
	 */
	public function assignByRef($name, &$value, $noCache = false)
	{

		return $this->assign($name, $value, $noCache);

	}

	/**
	 * Clear assigned template variable
	 * @param string|array<string> $name
	 * @return Smarty
	 */
	public function clearAssign($name)
	{

		// Array type
		if(is_array($name)) {

			// Set multiple template variables with the same value
			foreach($name as &$n) {

				// Value
				unset($this->templateVars[$n]);

				// Regular expression pattern type 1
				unset($this->templateVarRegExType1[$n]);

				// Regular expression pattern type 2
				unset($this->templateVarRegExType2[$n]);

			}

		} else {

			// Value
			unset($this->templateVars[$name]);

			// Regular expression pattern type 1
			unset($this->templateVarRegExType1[$name]);

			// Regular expression pattern type 2
			unset($this->templateVarRegExType2[$name]);

		}

		return $this;

	}

	/**
	 * Evaluate 'if' statement
	 * @param string $statement
	 * @return bool
	 */
	private function evaluateIfStatement($statement)
	{

		// Default return code
		$rc = false;

		// Loop template variable keys
		foreach(array_keys($this->templateVars) as $key) {

			// Input validation
			if(
				!isset($this->templateVarRegExType2[$key]) ||
				empty($this->templateVarRegExType2[$key])
			)
				continue;

			// Match key
			$rcc = preg_match($this->templateVarRegExType2[$key], $statement);
			if($rcc === 1) {

				// Replace with array access
				$statement = preg_replace(
					$this->templateVarRegExType2[$key],
					'$this->templateVars[\'' . $key . '\']',
					$statement
				);
				if($statement === null)
					throw new \Exception("Failed to replace IF statement");

			}

		}

		// Evil evaluate!
		eval("if(" . $statement . ') { $rc = true; } else { $rc = false; }');

		return $rc;

	}

	/**
	 * Process 'if' and 'elseif'
	 * @param string $theIf
	 * @return bool
	 * @throws \Exception
	 */
	private function processIf($theIf)
	{

		// Parse into the matches variable
		preg_match('/\{(?:else)?if (.*)?\}/mUs', $theIf, $matches);

		// Evaluate
		if(isset($matches[0], $matches[1]))
			return $this->evaluateIfStatement($matches[1]);
		else
			throw new \Exception("Failed to parse IF statement");

	}

	/**
	 * Process 'foreach'
	 * @param string $theFor
	 * @return array{0: array<mixed>, 1: string, 2: string}
	 * @throws \Exception
	 */
	private function processFor($theFor)
	{

		// Parse into the matches variable
		preg_match('/\{(?:else)?foreach (.*)?\}/mUs', $theFor, $matches);
		if(!isset($matches[0], $matches[1]))
			throw new \Exception("Failed to parse FOR statement");

		// Test for key
		if(strpos($matches[1], "=>") !== false) {

			// Split array from variables
			$rc = preg_match('/(.*)?\s+as\s+(.*)?\s+=>\s+(.*)?/i', $matches[1], $vars);
			if(
				$rc !== 1 ||
				!isset($vars[0], $vars[1], $vars[2], $vars[3])
			)
				throw new \Exception("Failed to parse array from variables");

		} else {

			// Split array from variable
			$rc = preg_match('/(.*)?\s+as\s+(.*)?/i', $matches[1], $vars);
			if(
				$rc !== 1 ||
				!isset($vars[0], $vars[1], $vars[2])
			)
				throw new \Exception("Failed to parse array from variable");

			// Add our own key
			$vars[3] = $vars[2];
			$vars[2] = '$_AETOLOS_INTERNAL_KEY_' . $vars[3];

		}

		// Remove whitespace
		$vars = array_map("trim", $vars);

		// Object property
		$objectProp = "";

		// Test for objects
		if(strpos($vars[1], "->") !== false) {

			// Split object access
			$rc = preg_match('/\$(.*)?->(.*)?/i', $vars[1], $object);
			if(
				$rc !== 1 ||
				!isset($object[0], $object[1], $object[2])
			)
				throw new \Exception("Failed to parse object from variable");

			$vars[1] = $object[1];
			$objectProp = $object[2];

			// Clean-up
			unset($object);

		}

		// Only get the variable name
		$vars = str_replace('$', "", $vars);

		// Get the array in reverse order
		if(
			!empty($objectProp) &&
			isset($this->templateVars[$vars[1]]) &&
			is_object($this->templateVars[$vars[1]])
		) {

			if(property_exists($this->templateVars[$vars[1]], $objectProp))
				$vars[1] = array_reverse($this->templateVars[$vars[1]]->$objectProp, true);
			else
				$vars[1] = array();

		} elseif(
			isset($this->templateVars[$vars[1]]) &&
			is_array($this->templateVars[$vars[1]])
		) {

			$vars[1] = array_reverse($this->templateVars[$vars[1]], true);

		} else {

			throw new \Exception("Failed to match FOREACH variable");

		}

		if(is_string($vars[2]))
			$varIntKey = $vars[2];
		else
			throw new \Exception("Failed to parse varIntKey");

		if(is_string($vars[3]))
			$varLetter = $vars[3];
		else
			throw new \Exception("Failed to parse varLetter");

		return array($vars[1], $varIntKey, $varLetter);

	}

	/**
	 * Get the position of the '{' of the next command
	 * @param string &$data
	 * @param int $position
	 * @return int|false
	 */
	private function nextCommandPosition(&$data, $position = 0)
	{

		do {

			// Out of space
			if($position > strlen($data))
				return false;

			// Scan for next command
			$position = strpos($data, "{", $position);
			if($position === false)
				return false;

			// Extract second character
			$secondChar = trim(substr($data, $position, 2));
			if(empty($secondChar)) {

				return false;

			} elseif($secondChar === "{") {

				// Skip orphan bracket
				$position++;

				continue;

			} elseif(
				$secondChar === "{}" ||
				$secondChar === "{{"
			) {

				// Skip double brackets
				$position++;

				continue;

			}

			return $position;

		} while(strpos($data, "{", $position) !== false);

		return false;

	}

	/**
	 * Get the command prefix along with the opening '{'
	 * @param string &$data
	 * @param int $position
	 * @return string|false
	 */
	private function getCommand(&$data, $position = 0)
	{

		// Read-ahead for the ending bracket
		$end = strpos($data, "}", ($position + 1));
		if($end === false)
			return false;

		// Move to the next character position
		$end++;

		// Read-ahead for commands without parameters
		$readAhead = substr($data, $position, ($end - $position));

		// Detect plain variables
		if(strpos($readAhead, '{$') === 0)
			return '{$';

		// Read-ahead for space
		$space = strpos($data, " ", $position);
		if(
			$space === false ||
			$space > $end
		)
			return $readAhead;
		else
			// Extract command
			return substr($data, $position, ($space - $position));

	}

	/**
	 * Get the position of the closing '}' of the current command
	 * @param string &$data
	 * @param int $position
	 * @return int|false
	 */
	private function endCommandPosition(&$data, $position = 0)
	{

		// Scan for closing '}'
		return strpos($data, "}", $position);

	}


	/**
	 * Read ahead and get the position of the closing tag
	 * @param string &$data
	 * @param int $position
	 * @param string $tag
	 * @param int $depth
	 * @return int
	 * @throws \Exception
	 */
	private function readAheadClosingTag(&$data, $position, $tag, $depth = 1)
	{

		// Get position
		$pos = $this->nextCommandPosition($data, $position);
		if($pos === false)
			throw new \Exception("Failed to find the next command: " . $tag);

		$command = $this->getCommand($data, $pos);
		if($command === false)
			throw new \Exception("Failed to get the next command: " . $tag);

		// Match tag
		if($command === "{" . $tag) {

			// Opening tag
			$depth++;

		} elseif(
			$tag === "if" &&
			(
				$command === "{else}" ||
				$command === "{elseif"
			)
		) {

			if($depth === 1)
				return $pos;

		} elseif($command === "{/" . $tag . "}") {

			// Closing tag
			$depth--;
			if($depth === 0)
				return $pos;

		}

		return $this->readAheadClosingTag($data, ($pos + strlen($command)), $tag, $depth);

	}

	/**
	 * Replace type 1 variables
	 * @param string &$data
	 * @param string $command
	 * @param int $start
	 * @param int $end
	 * @return void
	 * @throws \Exception
	 */
	private function replaceType1Variables(&$data, $command, $start, $end)
	{

		// First test for plain variables, then for arrays and then for objects
		if(
			strpos($command, '{$') === 0 &&
			strpos($command, "[") === false &&
			strpos($command, "->") === false
		) {

			// Split variable access
			$rc = preg_match('/\{\$(.*)?\}/i', $command, $varName);
			if(
				$rc !== 1 ||
				!isset($varName[0], $varName[1])
			)
				throw new \Exception("Failed to parse variable from command");

			// Special case for the iterator counter
			$itPos = strpos($varName[1], "@iteration");
			if($itPos !== false)
				$varName[1] = "_AETOLOS_INTERNAL_ITERATOR_" . substr($varName[1], 0, $itPos);

			// Plain variable
			$data = substr($data, 0, $start) . $this->templateVars[$varName[1]] . substr($data, ($end + 1));

			// Clean-up
			unset($varName, $itPos);

		} elseif(
			strpos($command, '{$') === 0 &&
			strpos($command, "[") !== false &&
			strpos($command, "->") === false
		) {

			// Split array access
			$rc = preg_match('/\{\$(.*)?\[(.*)?\]\}/i', $command, $array);
			if(
				$rc !== 1 ||
				!isset($array[0], $array[1], $array[2])
			)
				throw new \Exception("Failed to parse array from command");

			// Array
			$data = substr($data, 0, $start) . $this->templateVars[$array[1]][$array[2]] . substr($data, ($end + 1));

			// Clean-up
			unset($array);

		} elseif(
			strpos($command, '{$') === 0 &&
			strpos($command, "[") === false &&
			strpos($command, "->") !== false
		) {

			// Split object access
			$rc = preg_match('/\{\$(.*)?->(.*)?\}/i', $command, $object);
			if(
				$rc !== 1 ||
				!isset($object[0], $object[1], $object[2])
			)
				throw new \Exception("Failed to parse object from command");

			// Object
			$data = substr($data, 0, $start) . $this->templateVars[$object[1]]->{$object[2]} . substr($data, ($end + 1));

			// Clean-up
			unset($object);

		}

	}

	/**
	 * The parser
	 * @param string &$data
	 * @param int $start
	 * @return void
	 * @throws \Exception
	 */
	private function parser(&$data, $start = 0)
	{

		// Remove comments
		$rc = @preg_replace('/\{\*.*?\*\}/mUs', "", $data);
		if($rc !== null)
			$data = $rc;

		// Extract all literal data
		preg_match_all('/\{literal\}(.*)?\{\/literal\}/imUs', $data, $literals, PREG_OFFSET_CAPTURE);
		if(
			isset($literals[0], $literals[1]) &&
			sizeof($literals[0]) > 0
		) {

			// Reverse the arrays
			$literals[0] = array_reverse($literals[0], true);
			$literals[1] = array_reverse($literals[1], true);

			// Loop literal data
			foreach($literals[0] as $lKey => $lValue) {

				// Replace literal with a temporary string
				$data = substr($data, 0, $lValue[1]) . "_AETOLOS_INTERNAL_LITERAL_" . $lKey .
					substr($data, ($lValue[1] + strlen($lValue[0])));

			}

			// Clean-up
			unset($lKey, $lValue);

		}

		do {

			// Get position
			$rc = $this->nextCommandPosition($data, $start);
			if($rc === false)
				break;

			// Update with new position
			$start = $rc;

			// Get command at the current position
			$command = $this->getCommand($data, $start);
			if($command === false)
				break;

			// Get partial positions
			$end = $this->endCommandPosition($data, $start);
			if($end === false)
				break;

			// Switch possible commands
			switch($command) {
				case '{$':
					// Extract variable `{$VAR}`
					$theVar = substr($data, $start, ($end + 1 - $start));

					// Replace variables
					$this->replaceType1Variables($data, $theVar, $start, $end);
					break;

				case "{if":
					// Extract statement `{if ...}`
					$theIf = substr($data, $start, ($end + 1 - $start));

					// Process statement
					$theIf = $this->processIf($theIf);

					// Closing tag `{elseif ...}` or `{else}` or `{/if}`
					$ifEnd = $this->readAheadClosingTag($data, $end, "if");
					if($theIf === true) {

						// Remove statement
						$data = substr($data, 0, $start) . substr($data, ($end + 1));

						// Keep 'if' body of the statement

					} else {

						// Read-ahead for the ending bracket
						$end = strpos($data, "}", ($ifEnd + 1));
						if($end === false)
							break;

						// Remove the statement and the 'if' body
						$data = substr($data, 0, $start) . substr($data, $ifEnd);

					}

					// Add to chain
					$this->chain[] = array(
						'command'	=> "if",
						'start'		=> $start,
						'result'	=> $theIf
					);

					// Clean-up
					unset($theIf, $ifEnd);
					break;

				case "{elseif":
					// Remove from chain
					do {

						$link = array_pop($this->chain);
						if($link === null)
							throw new \Exception("Failed to find closing `if` command within `elseif` loop");

					} while($link['command'] !== "if");

					// Closing tag
					$ifEnd = $this->readAheadClosingTag($data, $end, "if");

					// If the previous statement is a success, ignore this part
					if($link['result'] === true) {

						// Read-ahead for the ending bracket
						$end = strpos($data, "}", ($ifEnd + 1));
						if($end === false)
							break;

						// Show result
						$data = substr($data, 0, $start) . substr($data, $ifEnd);

					} else {

						// Extract statement `{elseif ...}`
						$theIf = substr($data, $start, ($end + 1 - $start));

						// Process statement
						$theIf = $this->processIf($theIf);

						if($theIf === true) {

							// Remove statement
							$data = substr($data, 0, $start) . substr($data, ($end + 1));

							// Update the result
							$link['result'] = true;

						} else {

							// Remove elseif + statement
							$data = substr($data, 0, $start) . substr($data, $ifEnd);

						}

					}

					// Add to chain
					$this->chain[] = array(
						'command'	=> "if",
						'start'		=> $start,
						'result'	=> $link['result']
					);

					// Clean-up
					unset($theIf, $ifEnd);
					break;

				case "{else}":
					// Remove from chain
					do {

						$link = array_pop($this->chain);
						if($link === null)
							throw new \Exception("Failed to find closing `if` command within `else` loop");

					} while($link['command'] !== "if");

					// Closing tag
					$ifEnd = $this->readAheadClosingTag($data, $end, "if");

					// Keep first part or the next
					if($link['result'] === true) {

						// Read-ahead for the ending bracket
						$end = strpos($data, "}", ($ifEnd + 1));
						if($end === false)
							break;

						// Show result
						$data = substr($data, 0, $start) . substr($data, $ifEnd);

					} else {

						// Remove result
						$data = substr($data, 0, $link['start']) . substr($data, ($end + 1));

						// Our data changed, find the next position
						$rc = $this->nextCommandPosition($data, $link['start']);
						if($rc === false) {

							// Reset position
							$start = 0;

							break;

						}

						// Update with new position
						$start = $rc;

					}

					// Add to chain
					$this->chain[] = array(
						'command'	=> "else",
						'start'		=> $start,
						'result'	=> $link['result']
					);

					// Clean-up
					unset($link);
					break;

				case "{/if}":
					// Remove from chain
					do {

						$link = array_pop($this->chain);
						if($link === null)
							throw new \Exception("Failed to find closing `if` command");

					} while($link['command'] !== "if" && $link['command'] !== "else");

					// Remove statement
					$data = substr($data, 0, $start) . substr($data, ($end + 1));

					// Clean-up
					unset($link);
					break;

				case "{foreach":
					// Read ahead and get the position of the closing foreach
					$foreachEnd = $this->readAheadClosingTag($data, $end, "foreach");

					// Get the foreach template part
					$foreachTemplate = substr($data, ($end + 1), ($foreachEnd - 1 - $end));

					// Extract statement `{foreach ...}`
					$theFor = substr($data, $start, ($end + 1 - $start));

					// Process statement
					list($theFor, $key, $var) = $this->processFor($theFor);

					// Start with an empty template result
					$templateResult = "";

					// Keep a copy of the chain
					$chain = $this->chain;

					// Prepare the iterator
					$iterator = 0;

					while(sizeof($theFor) > 0) {

						// Prepare a new template part
						$foreachTemplateModified = $foreachTemplate;

						// Prepare a new chain
						$this->chain = $chain;

						// Remove one element from the FOR array
						$elementKey = array_keys($theFor);
						$elementKey = array_pop($elementKey);
						$elementValue = array_pop($theFor);

						// Assign as template variables
						$this->assign($key, $elementKey);
						$this->assign($var, $elementValue);

						// Increment and assign the iterator
						$iterator++;
						$this->assign("_AETOLOS_INTERNAL_ITERATOR_" . $var, $iterator);

						// Run the parser over the template data
						$this->parser($foreachTemplateModified);

						// Clear assignments
						$this->clearAssign($key);
						$this->clearAssign($var);
						$this->clearAssign("_AETOLOS_INTERNAL_ITERATOR_" . $var);

						// Append to the template result
						$templateResult .= $foreachTemplateModified;

					}

					// Restore chain
					$this->chain = $chain;

					// Replace foreach with template result
					$templateResult = substr($data, 0, $start) . $templateResult;

					// Get the new position after the replaced data
					$newPos = strlen($templateResult);

					// Append the rest of the template
					$data = $templateResult . substr($data, ($foreachEnd + 10));

					// Increment position
					$start = $newPos;

					// Clean-up
					unset(
						$foreachEnd,
						$foreachTemplate,
						$theFor,
						$key,
						$var,
						$templateResult,
						$chain,
						$iterator,
						$elementKey,
						$elementValue,
						$newPos
					);
					break;

				case "{literal}":
					// We should not be here
					throw new \Exception("Failed to remove literal section");

				case "{assign":
					// Extract statement
					$theAssign = substr($data, $start, ($end + 1 - $start));

					// Remove statement
					$data = substr($data, 0, $start) . substr($data, ($end + 1));

					// Extract values
					preg_match('/\{assign\s+var=(.*)?\s+value=(.*)?\}/imUs', $theAssign, $assigns);
					if(!isset($assigns[0], $assigns[1], $assigns[2]))
						throw new \Exception("Failed to extract assign values");

					// Check for variables within the value
					if(strpos($assigns[2], '$') === 0) {

						// Add brackets
						$assigns[2] = "{" . $assigns[2] . "}";

						// Replace variable
						$this->replaceType1Variables($assigns[2], $assigns[2], 0, strlen($assigns[2]));

					}

					// Assign as template variable
					$this->assign($assigns[1], $assigns[2]);
					break;

				case "{continue}":
					// Ignore the rest of the template
					$data = substr($data, 0, $start);
					return;

				case "{nocache}":
				case "{/nocache}":
					// Remove statement
					$data = substr($data, 0, $start) . substr($data, ($end + 1));
					break;

				default:
					// Ignore unknown commands/statements by incrementing the starting position
					$start++;
					break;

			}

		} while($this->nextCommandPosition($data, $start) !== false);

		// If we have literal data
		if(
			isset($literals[0], $literals[1]) &&
			sizeof($literals[0]) > 0
		) {

			// Loop literal data
			foreach($literals[1] as $lKey => $lValue) {

				// Replace temporary string with the literal equivalent
				$data = str_replace("_AETOLOS_INTERNAL_LITERAL_" . $lKey, $lValue[0], $data);

			}

		}

		return;

	}

	/**
	 * Fetch a template
	 * @param string|null $template
	 * @param mixed $cache_id
	 * @param mixed $compile_id
	 * @param object $parent
	 * @return string
	 */
	public function fetch($template = "", $cache_id = null, $compile_id = null, $parent = null)
	{

		if(
			$template === null ||
			empty($template)
		) {

			// Return early on no input
			return "";

		} elseif(strpos($template, "extends:string:base64:") === 0) {

			// Blocks are not supported
			die("blocks are not supported");

		} elseif(is_file($template)) {

			// Load the template data
			$rc = file_get_contents($template);
			if($rc === false)
				return "";

			$this->data = $rc;

			// Clean-up
			unset($rc);

			try {

				// Run the parser over the template data
				$this->parser($this->data);

			} catch(\Exception $e) {

				Log::error($e->getMessage());
				return "";

			}

			return $this->data;

		} else {

			// Not a file?
			return "";

		}

	}

}

