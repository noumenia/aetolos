<?php
/**
 * Aetolos - Aetolos Daemon
 *
 * API daemon for communication over JSON
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage aetolosdaemon
 */

class AetolosDaemon extends SocketManager {

	/**
	 * HTTP Basic authentication username
	 * (Authentication is disabled if both username and password are empty)
	 * @var string
	 */
	private $username = "";

	/**
	 * HTTP Basic authentication password
	 * (Authentication is disabled if both username and password are empty)
	 * @var string
	 */
	private $password = "";

	/**
	 * Constructor
	 * @param string $effectiveUser Effective process user owner
	 * @param string $effectiveGroup Effective process group owner
	 * @param string $filePid PID file
	 * @param int $processLimit Limit the number of forked processes
	 * @param string $connection Socket file or IPv4/IPv6 address/port string ("unix|socket:/path/to/file.sock" or "inet:port@IP")
	 * @param array<string, bool|int|string> $ssl SSL/TLS context options (@see https://www.php.net/manual/en/context.ssl.php)
	 * @param string $username HTTP Basic authentication username
	 * @param string $password HTTP Basic authentication password
	 * @return void
	 */
	public function __construct(
		string $effectiveUser = "",
		string $effectiveGroup = "",
		string $filePid = "",
		int $processLimit = 100,
		string $connection = "",
		array $ssl = array(),
		string $username = "",
		string $password = ""
	) {

		// HTTP Basic authentication (disabled if both username and password are empty)
		if(!empty($username))
			$this->username = $username;
		if(!empty($password))
			$this->password = $password;

		// Call the socket manager constructor
		parent::__construct($effectiveUser, $effectiveGroup, $filePid, $processLimit, $connection, $ssl);

	}

	/**
	 * Send a message
	 * @param string|array<mixed> $reply The message body
	 * @return bool
	 */
	private function sendHttpMessage($reply): bool
	{

		// Message
		$message = array(
			'reply'		=> $reply,
			'log'		=> Config::$log,
			'error'		=> ""
		);

		// Encode array to JSON
		$message = json_encode($message);
		if($message === false)
			return false;

		return $this->socketWriteByHttp(array(
			'http-version'	=> "HTTP/1.1",
			'status-code'	=> 200,
			'reason-phrase'	=> "OK",
			'header'	=> array(
				"Server: Aetolos",
				"Content-Type: application/json"
			),
			'body'		=> $message
		));

	}

	/**
	 * Send an error
	 * @param int $statusCode The HTTP status code (errors from 400+)
	 * @param string $reply The message body
	 * @param string $reasonPhrase An optional reason related to the status code
	 * @param array<string> $extraHeaders Additional headers
	 * @return bool
	 */
	private function sendHttpError(int $statusCode, string $reply = "", string $reasonPhrase = "", array $extraHeaders = array()): bool
	{

		// Errors from 400+
		if($statusCode < 400)
			return false;

		// Message
		$message = array(
			'reply'		=> $reply,
			'log'		=> Config::$log,
			'error'		=> $statusCode
		);

		// Encode array to JSON
		$message = json_encode($message);
		if($message === false)
			return false;

		// Default headers
		$headers = array(
			"Server: Aetolos",
			"Content-Type: application/json"
		);

		// Append extra headers
		if(sizeof($extraHeaders) > 0)
			$headers = array_merge($headers, $extraHeaders);

		return $this->socketWriteByHttp(array(
			'http-version'	=> "HTTP/1.1",
			'status-code'	=> $statusCode,
			'reason-phrase'	=> $reasonPhrase,
			'header'	=> $headers,
			'body'		=> $message
		));

	}

	/**
	 * Receive a remote HTTP message
	 * @return array{method: string, request-uri: array<string>, http-version: string, header: array<string>, body: array<mixed>, status-code: int}|false
	 */
	private function receiveHttpMessage()
	{

		Log::debug("[AetolosDaemon: receiveHttpMessage]");

		// Read remote message
		$message = $this->socketReadByHttp();

		// Check for error messages
		if($message['status-code'] >= 300) {

			// Send reply
			$this->sendHttpError($message['status-code']);

			return false;

		}

		// Only support GET and POST
		if(
			$message['method'] !== "GET" &&
			$message['method'] !== "POST"
		) {

			// Send reply
			$this->sendHttpError(405);

			return false;

		}

		// Reject requests with invalid or invisible UTF-8 characters
		if(preg_match("/\p{C}/", $message['request-uri']) === 1) {

			// Send reply
			$this->sendHttpError(404);

			return false;

		}

		// Only accept alphanumeric characters, dash, forward slash, question mark and dot
		$message['request-uri'] = preg_replace('/[^a-zA-Z0-9\-\/\?\.]+/', "", $message['request-uri']);

		// Reject empty requests
		if(empty($message['request-uri'])) {

			// Send reply
			$this->sendHttpError(404);

			return false;

		}

		// Convert the URI string into an array based on forward-slash separators
		$message['request-uri'] = array_values(array_filter(explode("/", $message['request-uri'], 3)));

		// Reject requests without a command or with an unexpected array size
		if(
			!isset($message['request-uri'][0]) ||
			sizeof($message['request-uri']) > 2
		) {

			// Send reply
			$this->sendHttpError(404);

			return false;

		}

		// Restrictions for POST requests only
		if(
			$message['method'] === "POST" &&
			!empty($message['body'])
		) {

			// Require a content-type for POST requests
			$rc = preg_grep('/^Content-Type: /i', $message['header']);
			if(
				$rc === false ||
				sizeof($rc) === 0
			) {

				// Send reply
				$this->sendHttpError(415);

				return false;

			}

			// Get the first result
			$rc = array_shift($rc);

			// Require a JSON body
			if(strtolower($rc) !== "content-type: application/json") {

				// Send reply
				$this->sendHttpError(415);

				return false;

			}

			// Crude validation before we proceed to json_decode
			if(
				empty($message['body']) ||
				$message['body'][0] !== "{" ||
				substr($message['body'], -1) !== "}"
			) {

				// Send reply
				$this->sendHttpError(415);

				return false;

			}

			// Decode JSON to PHP (force the result as an array)
			$message['body'] = json_decode($message['body'], true);

			// Input validation
			if(!is_array($message['body'])) {

				// Send reply
				$this->sendHttpError(415);

				return false;

			}

		} else {

			// Ignore the body in all other cases
			$message['body'] = array();

		}

		return $message;

	}

	/**
	 * Basic Authentication RFC7235
	 * @see https://datatracker.ietf.org/doc/html/rfc7235
	 * @param array{method: string, request-uri: array<string>, http-version: string, header: array<string>, body: array<mixed>, status-code: int} &$message
	 * @return bool
	 */
	private function basicAuthentication(&$message): bool
	{

		// Always authenticate if the username/password are empty
		if(
			empty($this->username) &&
			empty($this->password)
		) {

			Log::debug("[AetolosDaemon: HTTP basic authentication disabled]");

			return true;

		}

		// Get the Authorization header
		$auth = preg_grep('/^Authorization: Basic (.*?)/i', $message['header']);
		if(
			$auth === false ||
			sizeof($auth) === 0
		) {

			// Send reply
			$this->sendHttpError(401, "", "", array("WWW-Authenticate: Basic realm=\"Aetolos\""));

			return false;

		}

		// Get the string out of the array
		$auth = array_pop($auth);

		// Remove the header title and the authentication scheme
		$auth = preg_replace('/^Authorization: Basic /i', "", $auth);
		if(empty($auth)) {

			// Send reply
			$this->sendHttpError(403);

			return false;

		}

		// Base64 decode of the basic scheme
		$auth = base64_decode($auth, true);
		if($auth === false) {

			// Send reply
			$this->sendHttpError(403);

			return false;

		}

		// Get the user/password separator character (colon)
		$colPos = strpos($auth, ":");
		if($colPos === false) {

			// Send reply
			$this->sendHttpError(403);

			return false;

		}

		// Extract the username and password
		$username = substr($auth, 0, $colPos);
		if(
			empty($username) ||
			$this->username !== $username
		) {

			// Send reply
			$this->sendHttpError(403);

			return false;

		}

		$password = substr($auth, ($colPos + 1));
		if(
			empty($password) ||
			$this->password !== $password
		) {

			// Send reply
			$this->sendHttpError(403);

			return false;

		}

		Log::debug("[AetolosDaemon: HTTP basic authentication passed for user: " . $username . "]");

		return true;

	}

	/**
	 * Process as a child, called when the parent process forks
	 * @return bool
	 */
	public function processChild(): bool
	{

		Log::debug("[AetolosDaemon: running child process]");

		// Mark this as a daemon instance
		Config::write("daemon", true);

		// Only output JSON
		Config::write("json", true);

		// Open our own database connection
		$rc = DbFactory::open();
		if($rc === false)
			return false;

		// Receive a remote command
		$message = $this->receiveHttpMessage();
		if($message === false)
			return false;

		// Basic Authentication RFC7235
		$rc = $this->basicAuthentication($message);
		if($rc === false)
			return false;

		if(Config::read("verbose") === true) {

			// Verbose output to array
			Log::setDestination(new LogDestinationArray(), LOG_DEBUG);

		} else {

			// Warnings or higher priority messages to array
			Log::setDestination(new LogDestinationArray(), LOG_WARNING);

		}

		// Start with a clean log
		Config::$log = array();

		switch($message['request-uri'][0]) {
			case "version":
				Log::debug("[AetolosDaemon: process command: version]");

				$this->sendHttpMessage("Aetolos v" . AET_VER . " " . AET_COPYRIGHT . ", " . AET_LICENSE);

				break;

			case "setup":
				Log::debug("[AetolosDaemon: process command: setup]");

				include dirname(__DIR__) . "/controller/setup.inc.php";

				$this->sendHttpMessage("setup successful");

				break;

			case "status":
				Log::debug("[AetolosDaemon: process command: status]");

				/** @var array<string, array<mixed>> $statusArray */
				$statusArray = array();

				include dirname(__DIR__) . "/controller/status.inc.php";

				$this->sendHttpMessage($statusArray);

				break;

			case "import":
				Log::debug("[AetolosDaemon: process command: import]");

				// Require the file parameter
				if(!isset($message['body']['file'])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(!is_file($message['body']['file'])) {

					// Send reply
					$this->sendHttpError(404, "Import file not found");

					return false;

				}

				$cmdParameters = array('import' => $message['body']['file']);

				include dirname(__DIR__) . "/controller/import.inc.php";

				$this->sendHttpMessage("import successful");

				break;

			case "export":
				Log::debug("[AetolosDaemon: process command: export]");

				// Require the file and virtualhost parameters
				if(
					!isset($message['body']['file']) ||
					!isset($message['body']['virtualhost'])
				) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				}

				$cmdParameters = array(
					'export'	=> $message['body']['file'],
					'virtualhost'	=> $message['body']['virtualhost']
				);
				if(isset($message['body']['extradb']))
					$cmdParameters['extradb'] = $message['body']['extradb'];

				include dirname(__DIR__) . "/controller/export.inc.php";

				$this->sendHttpMessage("export successful");

				break;

			case "enable":
				Log::debug("[AetolosDaemon: process command: enable]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = array('enable' => $message['request-uri'][1]);

				include dirname(__DIR__) . "/controller/enable.inc.php";

				$this->sendHttpMessage("enable module successful");

				break;

			case "disable":
				Log::debug("[AetolosDaemon: process command: disable]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = array('disable' => $message['request-uri'][1]);

				include dirname(__DIR__) . "/controller/disable.inc.php";

				$this->sendHttpMessage("disable module successful");

				break;

			case "start":
				Log::debug("[AetolosDaemon: process command: start]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = array('start' => $message['request-uri'][1]);

				include dirname(__DIR__) . "/controller/start.inc.php";

				$this->sendHttpMessage("start module successful");

				break;

			case "stop":
				Log::debug("[AetolosDaemon: process command: stop]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = array('stop' => $message['request-uri'][1]);

				include dirname(__DIR__) . "/controller/stop.inc.php";

				$this->sendHttpMessage("stop module successful");

				break;

			case "reconfigure":
				Log::debug("[AetolosDaemon: process command: reconfigure]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = array('reconfigure' => $message['request-uri'][1]);

				include dirname(__DIR__) . "/controller/reconfigure.inc.php";

				$this->sendHttpMessage("reconfigure module successful");

				break;

			case "module":
				Log::debug("[AetolosDaemon: process command: module]");

				// Require the module parameter
				if(!isset($message['request-uri'][1])) {

					// Send reply
					$this->sendHttpError(406);

					return false;

				} elseif(class_exists($message['request-uri'][1]) === false) {

					// Send reply
					$this->sendHttpError(404);

					return false;

				}

				$cmdParameters = $message['body'];

				// Pass processing to the module controller
				$rc = Config::$modules[$message['request-uri'][1]]->controller($cmdParameters);
				if($rc === true)
					$this->sendHttpMessage("module successful");
				elseif($rc === false)
					$this->sendHttpMessage("modune completed with an error");
				else
					$this->sendHttpMessage($rc);

				break;

			default:
				// Send reply
				$this->sendHttpError(404);

				break;
		}

		return true;

	}

}

