<?php
/**
 * Aetolos - Setup for Fedora
 *
 * Installation and setup of Aetolos
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage setupfedora
 */

/**
 * Class to cover all installation and setup functionality
 *
 * @package aetolos
 * @subpackage setupfedora
 */
class SetupFedora implements SetupInterface {

	/**
	 * Setup system repositories
	 * @return bool
	 */
	public function repository()
	{

		Log::debug("Checking repository dependencies");

		// Array of repositories
		$repositories = array();

		// Merge repository requirements from loadable modules
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules
			if(Config::read($moduleName) !== "enabled")
				continue;

			// Merge required packages
			$repositories = array_merge($repositories, $module->dependencies()['repository']);

		}

		// Remove duplicates
		$repositories = array_unique($repositories);

		// Sort repositories
		asort($repositories);

		// Loop each repository
		foreach($repositories as $r) {

			// Not needed in Fedora
			if($r === "epel-release")
				continue;

			// RPM query
			$output = null;
			$rc = null;
			exec("/usr/bin/rpm -q " . escapeshellarg($r) . " 2>/dev/null", $output, $rc);
			if($rc !== 0) {

				Log::debug("Installing repository: " . $r);

				// Install repository
				exec(Config::read("pmanager") . " -y install " . escapeshellarg($r) . " 2>/dev/null");

				// Re-check
				exec("/usr/bin/rpm -q " . escapeshellarg($r) . " 2>/dev/null", $output, $rc);
				if($rc !== 0) {

					Log::error("Error while installing repository dependencies");
					return false;

				}

				// Force update to latest repository
				exec(Config::read("pmanager") . " -y update " . escapeshellarg($r) . " 2>/dev/null");

			}

		}

		return true;

	}

	/**
	 * Setup system RPM packages
	 * @return bool
	 */
	public function package()
	{

		Log::debug("Checking package dependencies");

		// Array of packages
		$packages = array(
			"unzip",
			"bzip2",
			"tar",
			"selinux-policy",
			"selinux-policy-targeted",
			"python3-policycoreutils",
			"policycoreutils-python-utils"
		);

		// Array of conflicting packages
		$conflicts = array();

		// Merge package requirements from loadable modules
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules
			if(Config::read($moduleName) !== "enabled")
				continue;

			// Merge package conflicts
			$conflicts = array_merge($conflicts, $module->dependencies()['conflict']);

			// Merge required packages
			$packages = array_merge($packages, $module->packages());

		}

		// Loop each conflict
		foreach($conflicts as $c) {

			// RPM query
			$output = null;
			$rc = null;
			exec("/usr/bin/rpm -q " . escapeshellarg($c) . " 2>/dev/null", $output, $rc);
			if($rc === 0) {

				Log::debug("Removing conflicting package: " . $c);

				// Package found, try to remove
				exec(Config::read("pmanager") . " -y remove " . escapeshellarg($c) . " 2>/dev/null", $output, $rc);
				if($rc !== 0) {

					Log::error("Error while removing conflicting package: " . $c);
					return false;

				}

			}

		}

		if(Config::read("verbose") === true)
			$devNull = "";
		else
			$devNull = " 2>/dev/null";

		// Loop each package
		foreach($packages as $p) {

			// RPM query
			$output = null;
			$rc = null;
			exec("/usr/bin/rpm -q " . escapeshellarg($p) . $devNull, $output, $rc);
			if($rc === 1) {

				Log::debug("Installing package: " . $p);

				// Package not found, try to install
				exec(Config::read("pmanager") . " --setopt=install_weak_deps=0 -y install " . escapeshellarg($p) . $devNull);

				// Re-check
				exec("/usr/bin/rpm -q " . escapeshellarg($p) . $devNull, $output, $rc);
				if($rc === 1) {

					Log::error("Error while installing package dependencies");
					return false;

				}

			}

		}

		return true;

	}

	/**
	 * Setup SELinux
	 * @return bool
	 */
	public function seLinux()
	{

		Log::debug("Checking SELinux requirements");

		// Get current status
		$status = shell_exec("/usr/sbin/sestatus | grep 'SELinux status:'");
		if(
			$status === false ||
			$status === null
		) {

			Log::error("Error running sestatus");
			return false;

		}

		if(stripos($status, "disabled") !== false)
			return true;

		// Grab existing custom contexts
		$contexts = strval(shell_exec("/usr/sbin/semanage fcontext --list -C"));

		// Setup SELinux requirements per loadable modules
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules
			if(Config::read($moduleName) !== "enabled")
				continue;

			// Run SELinux module requirements
			$module->selinux($contexts);

		}

		return true;

	}

	/**
	 * Setup a self-signed certificate
	 * @return bool
	 */
	public function certificate()
	{

		Log::debug("Checking certificate requirements");

		// Default file names
		$cert = Config::read("pkitls|directoryCerts") . "/localhost.crt";
		$chain = Config::read("pkitls|directoryCerts") . "/localhost.fullchain";
		$key = Config::read("pkitls|directoryPrivate") . "/localhost.key";

		// Check for an existing certificate
		if(
			is_file($cert) &&
			is_file($key)
		) {

			// Check for the fullchain file
			if(!is_file($chain)) {

				// Use the certificate as a fullchain
				copy($cert, $chain);

				// Enforce permissions
				chmod($chain, 0600);

			}

			return true;

		}

		Log::debug("Generating a self-signed certificate");

		// Get hostname
		$hostName = gethostname();
		if($hostName === false) {

			Log::error("Error getting the hostname of the system");
			return false;

		}

		// Generate a new private/public key pair
		$keyPair = openssl_pkey_new(array(
			'private_key_bits'	=> 4096,
			'private_key_type'	=> OPENSSL_KEYTYPE_RSA,
		));

		// Generate a CSR based on the above key
		$csr = openssl_csr_new(array(
			'countryName'			=> "--",
			'stateOrProvinceName'		=> "SomeState",
			'localityName'			=> "SomeCity",
			'organizationName'		=> "SomeOrganization",
			'organizationalUnitName'	=> "SomeOrganizationalUnit",
			'commonName'			=> $hostName,
			'emailAddress'			=> "root@" . $hostName
		), $keyPair, array('digest_alg' => "sha256"));
		if($csr === false) {

			Log::error("Error generating CSR");
			return false;

		}

		// Generate a certificate based on the above key
		$x509 = openssl_csr_sign($csr, null, $keyPair, 365, array(
			'x509_extensions'	=> "v3_req",
			'digest_alg'		=> "sha256"
		));
		if($x509 === false) {

			Log::error("Error signing x509 certificate");
			return false;

		}

		// Save key
		$rc = openssl_pkey_export_to_file($keyPair, $key, null);
		if($rc === false) {

			Log::error("Error exporting key to: " . $key);
			return false;

		}

		// Save certificate
		$rc = openssl_x509_export_to_file($x509, $cert);
		if($rc === false) {

			Log::error("Error exporting certificate to: " . $cert);
			return false;

		}

		// Check again
		if(
			is_file($cert) &&
			is_file($key)
		) {

			// Use the certificate as a fullchain
			copy($cert, $chain);

			// Enforce permissions
			chmod($key, 0600);
			chmod($chain, 0600);
			chmod($cert, 0600);

			Log::debug("Certificate: " . $cert);
			Log::debug("Fullchain: " . $chain);
			Log::debug("Key: " . $key);

			return true;

		} else {

			Log::error("Error generating self-signed certificate");
			return false;

		}

	}

	/**
	 * Setup ACME
	 * @return bool
	 */
	public function acme()
	{

		// Skip disabled module dehydrated
		if(Config::read("dehydrated") !== "enabled")
			return true;

		Log::debug("Checking ACME client");

		// Supported version
		$dhVersion = "0.7.1";

		// Get current status
		if(is_file("/root/dehydrated/dehydrated") === true) {

			// Load existing dehydrated script
			$rc = file_get_contents("/root/dehydrated/dehydrated");
			if($rc === false) {

				Log::error("Error while reading /root/dehydrated/dehydrated");
				return false;

			}

			// Read existing version
			$rc = preg_match('/^VERSION="(.*?)"$/m', $rc, $currentVersion);
			if(
				$rc !== 1 ||
				!isset($currentVersion[1])
			) {

				Log::error("Error while reading dehydrated version");
				return false;

			}

			// Version match
			if($dhVersion === $currentVersion[1])
				return true;

		}

		// Check for installation directory
		if(!is_dir("/root/dehydrated/")) {

			// Create installation directory
			$rc = mkdir("/root/dehydrated/", 0700);
			if($rc === false) {

				Log::error("Error while creating /root/dehydrated/ directory");
				return false;

			}

		}

		// Check for certificate directory
		if(!is_dir(Config::read("dehydrated|basedir"))) {

			// Create installation directory
			$rc = mkdir(Config::read("dehydrated|basedir"), 0755, true);
			if($rc === false) {

				Log::error("Error while creating " . Config::read("dehydrated|basedir") . " directory");
				return false;

			}

		}

		// Check for wellknown directory
		if(!is_dir(Config::read("dehydrated|wellknown"))) {

			// Create wellknown directory
			$rc = mkdir(Config::read("dehydrated|wellknown"), 0755, true);
			if($rc === false) {

				Log::error("Error while creating " . Config::read("dehydrated|basedir") . " directory");
				return false;

			}

		}

		Log::debug("Downloading ACME client: dehydrated " . $dhVersion);

		// Set CURL options
		$options = array(
			CURLOPT_URL		=> "https://raw.githubusercontent.com/dehydrated-io/dehydrated/v" . $dhVersion . "/dehydrated",
			CURLOPT_CONNECTTIMEOUT	=> 10,
			CURLOPT_TIMEOUT		=> 10,
			CURLOPT_RETURNTRANSFER	=> true
		);

		// Use the package manager proxy, if one is set
		if(!empty(Config::read("proxy")))
			$options[CURLOPT_PROXY] = Config::read("proxy");

		// Download latest ACME client (dehydrated)
		$curl = curl_init();
		curl_setopt_array($curl, $options);
		$rc = curl_exec($curl);
		curl_close($curl);
		if(is_bool($rc)) {

			Log::error("Error while downloading dehydrated from github");
			return false;

		}

		Log::debug("Installing ACME client under: /root/dehydrated/");

		// Save to file
		$rc = file_put_contents("/root/dehydrated/dehydrated", $rc);
		if($rc === false) {

			Log::error("Error while saving to file /root/dehydrated/dehydrated");
			return false;

		}

		// Set permissions
		$rc = chmod("/root/dehydrated/dehydrated", 0744);
		if($rc === false) {

			Log::error("Error while setting permissions to file /root/dehydrated/dehydrated");
			return false;

		}

		// Email address for ACME access
		if(empty(Config::read("dehydrated|email"))) {

			// Generate an email address
			$email = "postmaster@" . php_uname("n");

			Log::warning("ACME registration email set to: " . $email);
			Log::warning("Please change the ACME registration email by executing: '/root/aetolos/aetolos" .
				" --verbose --module=dehydrated --registration-email=new-email'");

			// Save email address
			Config::write("dehydrated|email", $email);

		}

		return true;

	}

	/**
	 * Run installation and setup
	 * @return bool
	 */
	public function run()
	{

		Log::debug("Starting operating system setup");

		// Requirements for the setup procedure
		if(class_exists("PDO", false) === false) {

			Log::error("The PDO package is required by the setup procedure. Please install it by running" .
				" 'dnf install php-pdo'.");
			return false;

		}

		if(function_exists("mb_substr") === false) {

			Log::error("The multibyte package is required by the setup procedure. Please install it by" .
				" running 'dnf install php-mbstring'.");
			return false;

		}

		// Crude test for Smarty
		$smarty = TemplateFactory::create();
		unset($smarty);

		// Required repositories
		$rc = $this->repository();
		if($rc === false)
			return false;

		// Required packages
		$rc = $this->package();
		if($rc === false)
			return false;

		// Setup SELinux
		$rc = $this->seLinux();
		if($rc === false)
			return false;

		// Setup a self-signed certificate
		$rc = $this->certificate();
		if($rc === false)
			return false;

		// Setup ACME
		$rc = $this->acme();
		if($rc === false)
			return false;

		// Check for missing directoryFpmD parameter
		if(Config::read("phpfpm|directoryFpmD", true) === false)
			Config::write("phpfpm|directoryFpmD", "/etc/php-fpm.d");

		Log::debug("Checking module dependencies");

		// Preemptively prepare OpenDKIM keys before NSD setup
		if(
			Config::read("nsd") === "enabled" &&
			Config::read("opendkim") === "enabled" &&
			(
				!is_file("/etc/opendkim/keys/default.private") ||
				!is_file("/etc/opendkim/keys/default.txt") ||
				filesize("/etc/opendkim/keys/default.private") === 0
			)
		) {

			$rc = Config::$modules['opendkim']->saveConfiguration();
			if($rc === false)
				return false;

		}

		// Check module dependencies
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules
			if(Config::read($moduleName) !== "enabled")
				continue;

			// Get module dependencies
			$deps = $module->dependencies()['module'];

			// No dependencies
			if(sizeof($deps) === 0)
				continue;

			// Loop dependencies
			foreach($deps as &$depName) {

				if(Config::read($depName) !== "enabled") {

					Log::error("Module " . $moduleName . " depends on: " . implode(", ", $deps));
					Log::error("The module " . $moduleName .
						" is enabled but it depends on the module " . $depName .
						" which is not enabled");
					return false;

				}

			}

		}

		// Reload systemd flag
		$daemonReload = false;

		// Setup loadable modules
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules
			if(Config::read($moduleName) !== "enabled")
				continue;

			$rc = $module->saveConfiguration();
			if($rc === false)
				return false;

			// Module requires systemd reload
			if(
				isset($module->daemonReload) &&
				$daemonReload === false &&
				$module->daemonReload === true
			)
				$daemonReload = true;

		}

		// Reload systemd
		if($daemonReload === true) {

			Log::debug("Reloading systemd");
			exec("/usr/bin/systemctl daemon-reload");

		}

		// Enable loadable modules
		foreach(Config::$modules as $moduleName => &$module) {

			// Skip disabled modules and the virtualHost module
			if(
				Config::read($moduleName) !== "enabled" ||
				$moduleName === "virtualhost"
			)
				continue;

			// Enable module and all its dependencies
			$module->enable();

		}

		return true;

	}

}

