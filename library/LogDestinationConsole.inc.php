<?php
/**
 * Log destination to console implementation class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package log
 * @subpackage log
 */
class LogDestinationConsole implements LogDestinationInterface {

	/**
	 * Source prefix
	 * @var string
	 */
	private $prefix = "";

	/**
	 * Selected output
	 * @var resource
	 */
	private $output = STDOUT;

	/**
	 * Constructor.
	 * Accepts the following parameters:
	 * - 'prefix' Prefix name added to each message
	 * - 'output' Select between normal (stdout) or error (stderr) output
	 * @param array<string, int|string> $parameters Various parameters relevant to the Destination
	 * @return void
	 */
	public function __construct($parameters = array())
	{

		// Set the prefix based on parameters or the script filename
		if(
			isset($parameters['prefix']) &&
			is_string($parameters['prefix'])
		)
			$this->prefix = $parameters['prefix'];
		elseif(
			isset($_SERVER['PHP_SELF']) &&
			!empty($_SERVER['PHP_SELF']) &&
			is_scalar($_SERVER['PHP_SELF'])
		)
			$this->prefix = basename(strval($_SERVER['PHP_SELF']));
		else
			$this->prefix = "daemon";

		// Select between normal (stdout) or error (stderr) output
		if(
			isset($parameters['output']) &&
			is_string($parameters['output'])
		) {

			switch($parameters['output']) {
				case "stderr":
					// Set output for this log
					$this->output = STDERR;
					break;

				case "stdout":
				default:
					// Set output for this log
					$this->output = STDOUT;
					break;
			}

		}

	}

	/**
	 * Write string to output
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		// JSON
		if(in_array("json", $parameters)) {

			// Do not make changes to the string
			$parameters[] = "nobeautification";

			switch($priorityLevel) {

				case LOG_EMERG:
					$priorityLevelString = "EMERGENCY";
					break;

				case LOG_ALERT:
					$priorityLevelString = "ALERT";
					break;

				case LOG_CRIT:
					$priorityLevelString = "CRITICAL";
					break;

				case LOG_ERR:
					$priorityLevelString = "ERROR";
					break;

				case LOG_WARNING:
					$priorityLevelString = "WARNING";
					break;

				case LOG_NOTICE:
					$priorityLevelString = "NOTICE";
					break;

				case LOG_INFO:
					$priorityLevelString = "INFO";
					break;

				case LOG_DEBUG:
					$priorityLevelString = "DEBUG";
					break;

				default:
					$priorityLevelString = "";
					break;

			}

			// Generate a JSON encoded object
			$string = (string)json_encode(array(
				'source'	=> $this->prefix,
				'priority'	=> $priorityLevelString,
				'message'	=> $string
			));

		}

		// Beautification
		if(!in_array("nobeautification", $parameters)) {

			switch($priorityLevel) {

				case LOG_EMERG:
					$string = "[" . chr(27) . "[41;1mEMERGENCY" . chr(27) . "[0m] " . $string;
					break;

				case LOG_ALERT:
					$string = "[" . chr(27) . "[41;1mALERT" . chr(27) . "[0m] " . $string;
					break;

				case LOG_CRIT:
					$string = "[" . chr(27) . "[45;1mCRITICAL" . chr(27) . "[0m] " . $string;
					break;

				case LOG_ERR:
					$string = "[" . chr(27) . "[31;1mERROR" . chr(27) . "[0m] " . $string;
					break;

				case LOG_WARNING:
					$string = "[" . chr(27) . "[33;1mWARNING" . chr(27) . "[0m] " . $string;
					break;

				case LOG_NOTICE:
					$string = "[" . chr(27) . "[34;1mNOTICE" . chr(27) . "[0m] " . $string;
					break;

				case LOG_INFO:
					$string = "[" . chr(27) . "[34;1mINFO" . chr(27) . "[0m] " . $string;
					break;

				case LOG_DEBUG:
					$string = "[" . chr(27) . "[34;1mDEBUG" . chr(27) . "[0m] " . $string;
					break;

			}

		}

		// Newline
		if(!in_array("nonewline", $parameters))
			$string .= "\n";

		// Output
		fwrite($this->output, $string);

	}

}

