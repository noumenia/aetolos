<?php
/**
 * Aetolos - Daemon Configuration
 *
 * Load configuration options from the daemon process
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage configd
 */

/**
 * Class to load configuration options
 *
 * @package aetolos
 * @subpackage configd
 */
class DaemonConfigd {

	/**
	 * Configuration file
	 * @var string
	 */
	private $configFile = "";

	/**
	 * Parsed configuration options
	 *
	 * - effectiveUser	= valid system user
	 * - effectiveGroup	= valid system group
	 * - filePid		= path and file to a PID file
	 * - processLimit	= number of maximum forked processes
	 * - connection		= connection string
	 * - ssl		= array of SSL context options
	 * - username		= username for Basic Authentication RFC7235
	 * - password		= password for Basic Authentication RFC7235
	 *
	 * @see https://www.php.net/manual/en/context.ssl.php
	 * @var array{effectiveUser: string, effectiveGroup: string, filePid: string, processLimit: int, connection: string, ssl: array<string, bool|int|string>, username: string, password: string}
	 */
	public $options = array(
		'effectiveUser'		=> "root",
		'effectiveGroup'	=> "root",
		'filePid'		=> "/run/aetolosDaemon.pid",
		'processLimit'		=> 10,
		'connection'		=> "unix:/run/aetolosDaemon.sock",
		'ssl'			=> array(),
		'username'		=> "",
		'password'		=> ""
	);

	/**
	 * Constructor
	 * @param string $configFile Configuration file
	 * @return void
	 */
	public function __construct(string $configFile)
	{

		// Validate config file
		if(
			empty($configFile) ||
			!is_file($configFile)
		) {

			Log::error("Could not find aetolosd config file: " . $configFile);
			exit(9);

		}

		// Configuration file
		$this->configFile = $configFile;

		// Load the configuration file into memory
		$lines = file_get_contents($this->configFile);
		if($lines === false) {

			Log::error("Could not load aetolosd config file: " . $configFile);
			exit(9);

		}

		// Convert lines to an array
		$lines = explode("\n", $lines);

		// Parse lines
		foreach($lines as &$line) {

			// Ignore comments
			if(
				isset($line[0]) &&
				$line[0] === "#"
			)
				$line = "";

		}

		// Clean-up
		unset($line);

		// Match lines
		$rc = preg_match_all('/^(.*?)\w?=\w?(.*?)$/m', implode("\n", $lines), $matches);
		if($rc === false) {

			Log::error("Could not parse aetolosd config file: " . $configFile);
			exit(9);

		}

		// Clean-up
		unset($lines);

		// Get the size of the array
		$sizeOfMatches = sizeof($matches[1]);

		// Parse matches
		for($i = 0; $i < $sizeOfMatches; $i++) {

			// Trim whitespace and double quotes
			$matches[1][$i] = trim($matches[1][$i]);
			$matches[2][$i] = trim(strval($matches[2][$i]), " \n\r\t\v\x00\"");

			// Strict types (boolean and integer)
			if(
				$matches[2][$i] === "true" ||
				$matches[2][$i] === "false"
			)
				$matches[2][$i] = filter_var($matches[2][$i], FILTER_VALIDATE_BOOLEAN);
			elseif(is_numeric($matches[2][$i]))
				$matches[2][$i] = filter_var($matches[2][$i], FILTER_VALIDATE_INT);

			// Check for multi-option
			if(strpos($matches[1][$i], ".") !== false) {

				$multi = explode(".", $matches[1][$i], 2);
				if(
					!isset($multi[0], $multi[1]) ||
					empty($multi[0]) ||
					empty($multi[1])
				) {

					Log::error("Could not parse aetolosd config file: " . $configFile);
					exit(9);

				}

				// Append to the array
				if($multi[0] === "ssl")
					$this->options['ssl'][$multi[1]] = $matches[2][$i];
				else
					Log::warning("Unknown array option: " . $multi[0]);

				// Clean-up
				unset($multi);

			} else {

				// Append to the array
				switch($matches[1][$i]) {
					case "effectiveUser":
						$this->options['effectiveUser'] = strval($matches[2][$i]);
						break;

					case "effectiveGroup":
						$this->options['effectiveGroup'] = strval($matches[2][$i]);
						break;

					case "filePid":
						$this->options['filePid'] = strval($matches[2][$i]);
						break;

					case "processLimit":
						$this->options['processLimit'] = intval($matches[2][$i]);
						break;

					case "connection":
						$this->options['connection'] = strval($matches[2][$i]);
						break;

					case "username":
						$this->options['username'] = strval($matches[2][$i]);
						break;

					case "password":
						$this->options['password'] = strval($matches[2][$i]);
						break;

					default:
						Log::warning("Unknown option: " . $matches[1][$i]);
						break;
				}

			}

		}

	}

}

