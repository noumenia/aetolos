<?php
/**
 * Aetolos - HAproxy manager
 *
 * Support and implementation for HAproxy cluster servers.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage haproxy
 */

/**
 * HAproxy manager implementation class
 *
 * @package aetolos
 * @subpackage haproxy
 */
class HaproxyManager {

	/**
	 * Check the existence of a HAproxy server
	 * @param string $server Server string without parameters
	 * @return bool
	 */
	private function exists($server)
	{

		// Prepare statement
		$preped = DbFactory::$db->conn->prepare("SELECT Server FROM `haproxyServer` WHERE Server=:server");

		// Bind parameter
		$preped->bindParam(":server", $server);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {
			Log::error("Error while selecting HAproxy server from the database table: haproxyServer");
			return false;
		}

		// Fetch all results in one array
		$result = $preped->fetchAll(PDO::FETCH_NUM);
		if($result === false)
			return false;

		// Check the result for a possible match
		if(sizeof($result) > 0)
			return true;
		else
			return false;

	}

	/**
	 * Add a new HAproxy server
	 * @param string $server Server string in the HAproxy format (address[:port] [parameters])
	 * @return bool
	 */
	public function add($server)
	{

		// Start with empty parameters by default
		$parameters = "";

		// Detect presense of extra parameters in the server string
		$pos = mb_strpos($server, " ");
		if($pos !== false) {

			// Split server address/port from the parameters
			$parameters = trim(mb_substr($server, $pos));
			$server = mb_substr($server, 0, $pos);

		}

		// Check if server already exists in the database
		$rc = $this->exists($server);
		if($rc === true) {
			Log::error("The specified HAproxy server already exists in the database: " . $server);
			return false;
		}

		// Add to database

		// Prepare statement
		$preped = DbFactory::$db->conn->prepare("INSERT INTO `haproxyServer` (Server, Parameters) VALUES (:server, :parameters)");

		// Bind parameters
		$preped->bindParam(":server", $server);
		$preped->bindParam(":parameters", $parameters);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {
			Log::error("Error while inserting HAproxy server into the database table: haproxyServer");
			return false;
		}

		return true;

	}

	/**
	 * Remove a HAproxy server
	 * @param string $server Server string in the HAproxy format (address[:port])
	 * @return bool
	 */
	public function remove($server)
	{

		// Detect presense of extra parameters in the server string
		$pos = mb_strpos($server, " ");
		if($pos !== false)
			// Split server address/port from the parameters
			$server = mb_substr($server, 0, $pos);

		// Check if server already exists in the database
		$rc = $this->exists($server);
		if($rc === false) {
			Log::error("The specified HAproxy server does not exist in the database: " . $server);
			return false;
		}

		// Prepare statement
		$preped = DbFactory::$db->conn->prepare("DELETE FROM `haproxyServer` WHERE Server=:server");

		// Bind parameter
		$preped->bindParam(":server", $server);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {
			Log::error("Error while deleting HAproxy server from the database table: haproxyServer");
			return false;
		}

		return true;

	}

}

