<?php
/**
 * Aetolos - Constants
 *
 * Define system-wide constants
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage constants
 */

// Set application version
define("AET_VER", "2.5-dev");

// Set database version
define("AET_DB_VER", "24");

// Set application copyright
define("AET_COPYRIGHT", "Copyright Noumenia 2023");

// Set application license
define("AET_LICENSE", "GNU GPL v3.0");

