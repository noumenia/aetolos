<?php
/**
 * Aetolos - AutoLoader
 *
 * PHP autoloader class functionality
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage autoloader
 */

/**
 * Autoloader function for file classes
 * @param string $className Class name as requested by PHP
 * @return void
 */
function NoumeniaAetolosAutoloader($className)
{

	// Convert and sanitize class name
	$rc = preg_replace('/(DBRecord|Record|List|Stats|Soap).*$/', "", $className);
	if($rc === null)
		return;
	else
		$className = $rc;

	$classFile = $className . ".inc.php";

	// Load file from library
	$filePath = dirname(__DIR__) . "/library/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

	// Load file from interface
	$filePath = dirname(__DIR__) . "/interface/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

	// Load file from modules/distro subdirectory
	$filePath = dirname(__DIR__) . "/modules/" . Config::read("distro") . "/" . strtolower($className) . "/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

}

