<?php
/**
 * Aetolos - SQlite3 Database interface
 *
 * An interface which defines a database connection for SQlite3.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage databasesqlite3
 */

/**
 * Database class
 *
 * @package aetolos
 * @subpackage databasesqlite3
 */
class DatabaseSqlite3 implements DatabaseInterface {

	/**
	 * PDO database connection object
	 * @var PDO
	 */
	public $conn;

	/**
	 * Database result after a query is executed
	 * @var PDOStatement|null
	 */
	public $res = null;

	/**
	 * Numerical array of fetched row
	 * @var mixed
	 */
	public $row = null;

	/**
	 * Connection check
	 * @var bool
	 */
	public $connected = false;

	/**
	 * Last error produced by query
	 * @var array<string>
	 */
	public $last_error = array();

	/**
	 * Open new database connection
	 * @return bool
	 */
	public function open()
	{

		// Start with a disabled connection
		$this->connected = false;

		try {

			// Check for PDO module
			if(!extension_loaded("PDO")) {

				Log::error("The PDO extension is not loaded, please install php-pdo (php72u-pdo for " .
					"php72us) and re-run Aetolos for the changes to take effect.");
				return false;

			}

			// Open PDO connection to SQlite3 file
			$this->conn = new PDO(Config::read("database"));

			// Convert numeric values to strings
			$this->conn->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, true);

			// Enabled
			$this->connected = true;

			// Enable and enforce foreign key constraints, as required by SQlite3
			$this->conn->exec("PRAGMA foreign_keys = ON");

			// Enable WAL mode, for concurrent access
			$this->conn->exec("PRAGMA journal_mode = WAL");

		} catch(PDOException $e) {

			Log::error("Error while opening database");

			// Failed to open database
			return false;

		}

		return $this->connected;

	}

	/**
	 * Close existing database connection
	 * @return bool
	 */
	public function close()
	{

		if($this->connected !== true)
			return false;

		return true;

	}

	/**
	 * Execute an SQL query
	 * @param string $sql SQL query
	 * @return bool
	 */
	public function query($sql)
	{

		if($this->connected !== true) {

			$this->res = null;
			return false;

		}

		// Execute a single query and return a PDO statement object
		$res = @$this->conn->query($sql);
		if($res === false) {

			$this->last_error = $this->conn->errorInfo();
			$this->res = null;

			Log::error("Database query error: " . implode(", ", $this->last_error));

			return false;

		}

		$this->res = $res;

		return true;

	}

	/**
	 * Execute multiple SQL queries
	 * @NOTE Not implemented
	 * @param string $sql One or more SQL queries
	 * @return bool
	 */
	public function multiQuery($sql)
	{

		Log::warning("Multi query not implemented or not applicable for this database");

		// Not implemented
		return false;

	}

	/**
	 * Get inserted row ID from last SQL query
	 * @param string $name Optional name of the sequence object from which the ID should be returned
	 * @return mixed
	 */
	public function insertId($name = null)
	{

		if($this->connected !== true)
			return false;

		// Return last insert ID, with optional name
		return $this->conn->lastInsertId($name);

	}

	/**
	 * Get affected rows from last SQL query
	 * @return int|false
	 */
	public function affectedRows()
	{

		if(
			$this->connected !== true ||
			$this->res === null
		)
			return false;
		else
			return $this->res->rowCount();

	}

	/**
	 * Get next row from the results of the last SQL query
	 * @param int $style PDO fetch style
	 * @return bool
	 */
	public function nextRow($style = PDO::FETCH_NUM)
	{

		if(
			$this->connected !== true ||
			$this->res === null
		)
			return false;

		$this->row = $this->res->fetch($style);
		if($this->row === false)
			return false;
		else
			return true;

	}

	/**
	 * Get an association array of the results of the last SQL query
	 * @return array<string, mixed>
	 */
	public function getAssocArray()
	{

		// Start with an empty association array
		$assocArray = array();

		// Loop over each row and append the result to the association array
		do {

			$rc = $this->nextRow(PDO::FETCH_ASSOC);
			if($this->row !== false)
				$assocArray = array_merge_recursive($assocArray, $this->row);

		} while($rc !== false);

		return $assocArray;

	}

	/**
	 * Protect SQL queries with escape string
	 * @param string $value SQL query
	 * @return string
	 */
	public function sqlProtect($value)
	{

		// Quote string
		return $this->conn->quote($value);

	}

}

