<?php
/**
 * Aetolos - MariaDB Database interface
 *
 * An interface which defines a database connection for MariaDB.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage databasemariadb
 */

/**
 * Database class
 *
 * @package aetolos
 * @subpackage databasemariadb
 */
class DatabaseMariadb implements DatabaseInterface {

	/**
	 * PDO database connection object
	 * @var PDO
	 */
	public $conn;

	/**
	 * Database result after a query is executed
	 * @var PDOStatement|null
	 */
	public $res = null;

	/**
	 * Numerical array of fetched row
	 * @var mixed
	 */
	public $row = null;

	/**
	 * Connection check
	 * @var bool
	 */
	public $connected = false;

	/**
	 * Last error produced by query
	 * @var array<string>
	 */
	public $last_error = array();

	/**
	 * Get MariaDB access credentials if "~/.my.cnf" exists.
	 * @return array<string, string>|false
	 */
	private function getCredentials()
	{

		// Get the HOME environment variable
		$mycnf = getenv("HOME");
		if($mycnf === false)
			return false;

		$mycnf = $mycnf . "/.my.cnf";
		$mdbuser = "root";
		$mdbpass = "";

		if(is_file($mycnf)) {

			$mycnf = file_get_contents($mycnf);
			if($mycnf === false)
				return false;

			$rc = preg_match('/user="(.*)"|\'(.*)\'/', $mycnf, $matches);
			if($rc === 1)
				$matches = array_values(array_filter($matches));
			if(isset($matches[1]))
				$mdbuser = $matches[1];

			$rc = preg_match('/pass(?:word)?="(.*)"|\'(.*)\'/', $mycnf, $matches);
			if($rc === 1)
				$matches = array_values(array_filter($matches));
			if(isset($matches[1]))
				$mdbpass = $matches[1];

		}

		return array('mdbuser' => $mdbuser, 'mdbpass' => $mdbpass);

	}

	/**
	 * Open new database connection
	 * @return bool
	 */
	public function open()
	{

		// Start with a disabled connection
		$this->connected = false;

		try {

			// Check for PDO module
			if(!extension_loaded("PDO")) {

				Log::error("The PDO extension is not loaded, please install php-pdo (php72u-pdo for " .
					"php72us) and re-run Aetolos for the changes to take effect.");
				return false;

			}

			// Check for MYSQLND module
			if(
				!defined("PDO::MYSQL_ATTR_INIT_COMMAND") ||
				!extension_loaded("mysqlnd")
			) {

				Log::error("The mysqlnd extension is not loaded, please install php-mysqlnd (" .
					"php72u-mysqlnd for php72us) and re-run Aetolos for the changes to take effect.");
				return false;

			}

			// Get MariaDB credentials (in case /usr/bin/mysql_secure_installation has already been
			// executed, otherwise use empty values by default)
			$mdb = $this->getCredentials();
			if($mdb === false) {

				Log::error("Error while reading MariaDB access credentials from ~/.my.cnf");
				return false;

			}

			// Open PDO connection
			$this->conn = new PDO(
				"mysql:host=localhost;charset=utf8mb4",
				$mdb['mdbuser'],
				$mdb['mdbpass'],
				array(
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'",
					PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT
				)
			);

			// Enabled
			$this->connected = true;

		} catch(PDOException $e) {

			Log::error("Error while trying to connect to MariaDB daemon (localhost)");
			Log::warning("If MariaDB has not been setup, please start MariaDB with 'systemctl start mari" .
				"adb' and execute '/usr/bin/mysql_secure_installation'. Otherwise, please make sure " .
				"MariaDB is running and the credentials have been set in /root/.my.cnf (user, passwo" .
				"rd and default-character-set).");

			// Failed to open database
			return false;

		}

		return $this->connected;

	}

	/**
	 * Close existing database connection
	 * @return bool
	 */
	public function close()
	{

		if($this->connected !== true)
			return false;

		return true;

	}

	/**
	 * Execute an SQL query
	 * @param string $sql SQL query
	 * @return bool
	 */
	public function query($sql)
	{

		if($this->connected !== true) {

			$this->res = null;
			return false;

		}

		// Execute a single query and return a PDO statement object
		$res = @$this->conn->query($sql);
		if($res === false) {

			$this->last_error = $this->conn->errorInfo();
			$this->res = null;

			Log::error("Database query error: " . implode(", ", $this->last_error));

			return false;

		}

		$this->res = $res;

		return true;

	}

	/**
	 * Execute multiple SQL queries
	 * @NOTE Not implemented
	 * @param string $sql One or more SQL queries
	 * @return bool
	 */
	public function multiQuery($sql)
	{

		Log::warning("Multi query not implemented or not applicable for this database");

		// Not implemented
		return false;

	}

	/**
	 * Get inserted row ID from last SQL query
	 * @param string $name Optional name of the sequence object from which the ID should be returned
	 * @return mixed
	 */
	public function insertId($name = null)
	{

		if($this->connected !== true)
			return false;

		// Return last insert ID, with optional name
		return $this->conn->lastInsertId($name);

	}

	/**
	 * Get affected rows from last SQL query
	 * @return int|false
	 */
	public function affectedRows()
	{

		if(
			$this->connected !== true ||
			$this->res === null
		)
			return false;
		else
			return $this->res->rowCount();

	}

	/**
	 * Get next row from the results of the last SQL query
	 * @param int $style PDO fetch style
	 * @return bool
	 */
	public function nextRow($style = PDO::FETCH_NUM)
	{

		if(
			$this->connected !== true ||
			$this->res === null
		)
			return false;

		$this->row = $this->res->fetch($style);
		if($this->row === false)
			return false;
		else
			return true;

	}

	/**
	 * Get an association array of the results of the last SQL query
	 * @return array<string, mixed>
	 */
	public function getAssocArray()
	{

		// Start with an empty association array
		$assocArray = array();

		// Loop over each row and append the result to the association array
		do {

			$rc = $this->nextRow(PDO::FETCH_ASSOC);
			if($this->row !== false)
				$assocArray = array_merge_recursive($assocArray, $this->row);

		} while($rc !== false);

		return $assocArray;

	}

	/**
	 * Protect SQL queries with escape string
	 * @param string $value SQL query
	 * @return string
	 */
	public function sqlProtect($value)
	{

		Log::warning("SQL escaping is deprecated, PDO uses prepared statements");

		// Not implemented
		return "";

	}

}

