<?php
/**
 * Aetolos - Database manager
 *
 * Support and implementation for database access by virtual hosts.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage mariadb
 */

/**
 * Database implementation class
 *
 * @package aetolos
 * @subpackage mariadb
 */
class DatabaseManager {

	/**
	 * Virtual host object
	 * @var VirtualHostManager
	 */
	public $vhost;

	/**
	 * Database object
	 * @var DatabaseMariadb
	 */
	private $mdb;

	/**
	 * Constructor.
	 * @param VirtualHostManager $vhost Virtual host object
	 * @return void
	 */
	public function __construct($vhost)
	{

		// Set virtual host object
		$this->vhost = $vhost;

		// Create database object
		$this->mdb = new DatabaseMariadb();

		// Connect to database
		$this->connect();

	}

	/**
	 * Create and open a database connection
	 * @return bool
	 */
	private function connect()
	{

		// Open database connection
		$this->mdb->open();
		if($this->mdb->connected === false) {

			Log::error("Error while opening connection to database");
			return false;

		}

		return true;

	}

	/**
	 * Parse and/or append host
	 * @param string &$string String to check and modify for host
	 * @return string
	 */
	private function parseHost(&$string)
	{

		// Host divider position
		$pos = mb_strpos($string, "@");

		// Check for host divider
		if($pos === false) {

			// Return localhost
			return "localhost";

		} else {

			// Extract host
			$host = explode("@", $string);
			if(sizeof($host) !== 2) {

				Log::error("Could not detect a host from: " . $string);
				return "";

			}

			// Modify user string
			$string = $host[0];

			return $host[1];

		}

	}

	/**
	 * Parse and/or append prefix
	 * @param string &$string String to check and modify for prefix
	 * @return bool
	 */
	private function parsePrefix(&$string)
	{

		// Prefix divider position
		$pos = mb_strpos($string, "_");

		// Check for prefix divider
		if($pos === false) {

			// Append prefix
			$string = $this->vhost->dbPrefix . "_" . $string;

		} else {

			// Extract prefix
			$prefix = mb_substr($string, 0, $pos);
			if(empty($prefix)) {

				Log::error("Could not detect a virtual host prefix from: " . $string);
				return false;

			}

			// Check ownership
			if($prefix !== $this->vhost->dbPrefix) {

				Log::error("Prefix does not match the selected virtual host: " . $string . "@" . $this->vhost->domainName);
				return false;

			}

		}

		return true;

	}

	/**
	 * Add a new user
	 * @param string $user User name (with or without prefix or host)
	 * @param string $password Password (plain text or in HASH encoding)
	 * @return bool
	 */
	public function addUser($user, $password)
	{

		// Input validation
		if(
			empty($user) ||
			empty($password)
		) {

			Log::error("Input validation failed");
			return false;

		}

		$host = $this->parseHost($user);
		if(empty($host))
			return false;

		$rc = $this->parsePrefix($user);
		if($rc === false)
			return false;

		// Validate length
		if(mb_strlen($user) > 16) {

			Log::error("Database user name is longer than 16 characters: " . $user . "@" . $host);
			return false;

		}

		// Validate user name
		$rc = preg_match('/[a-z_][a-z0-9_-]*[$]?/', $user);
		if($rc !== 1) {

			Log::error("Database user name failed validation: " . $user . "@" . $host);
			return false;

		}

		Log::debug("Add database user: " . $user . "@" . $host);

		// Check for plain text or HASH encoded password & prepare statement
		if(mb_strlen($password) === 41 && mb_stripos($password, "*") === 0)
			$preped = $this->mdb->conn->prepare("CREATE USER :user@:host IDENTIFIED BY PASSWORD :password");
		else
			$preped = $this->mdb->conn->prepare("CREATE USER :user@:host IDENTIFIED BY :password");

		// Bind parameter
		$preped->bindParam(":user", $user);
		$preped->bindParam(":host", $host);
		$preped->bindParam(":password", $password);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {

			Log::error("Error while running CREATE USER: " . $user . "@" . $host);
			return false;

		}

		return true;

	}

	/**
	 * Remove an existing user and revoke all related privileges
	 * @param string $user User name (with or without prefix or host)
	 * @return bool
	 */
	public function removeUser($user)
	{

		// Input validation
		if(empty($user)) {

			Log::error("Input validation failed");
			return false;

		}

		$host = $this->parseHost($user);
		if(empty($host))
			return false;

		$rc = $this->parsePrefix($user);
		if($rc === false)
			return false;

		Log::debug("Remove database user: " . $user . "@" . $host);

		// Prepare statement
		$preped = $this->mdb->conn->prepare("REVOKE ALL PRIVILEGES, GRANT OPTION FROM :user@:host; DROP USER :user2@:host2");

		// Bind parameter
		$preped->bindParam(":user", $user);
		$preped->bindParam(":host", $host);
		$preped->bindParam(":user2", $user);
		$preped->bindParam(":host2", $host);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {

			Log::error("Error while dropping user: " . $user . "@" . $host);
			return false;

		}

		return true;

	}

	/**
	 * Add a new database
	 * @param string $database Database name (with or without prefix)
	 * @return bool
	 */
	public function addDatabase($database)
	{

		// Input validation
		if(empty($database)) {

			Log::error("Input validation failed");
			return false;

		}

		$rc = $this->parsePrefix($database);
		if($rc === false)
			return false;

		// Validate length
		if(mb_strlen($database) > 64) {

			Log::error("Database name is longer than 64 characters: " . $database);
			return false;

		}

		// Validate database name
		$rc = preg_match('/[a-z_][a-z0-9_-]*[$]?/', $database);
		if($rc !== 1) {

			Log::error("Database name failed validation: " . $database);
			return false;

		}

		// Prepare statement
		$preped = $this->mdb->conn->prepare("SHOW DATABASES LIKE :database");

		// Bind parameter
		$preped->bindParam(":database", $database);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc !== true)
			return false;

		// Fetch results
		$rc = $preped->fetchAll(PDO::FETCH_NUM);
		if(
			$rc === false ||
			sizeof($rc) !== 0
		) {
			Log::error("Database already exists: " . $database);
			return false;
		}

		Log::debug("Add database: " . $database);

		// Create database
		// @TODO use a stored procedure because PDO can not be used with this type of query
		$rc = $this->mdb->query("CREATE DATABASE `" . $database . "`");
		if($rc === false) {

			Log::error("Error while creating database: " . $database);
			return false;

		}

		return true;

	}

	/**
	 * Remove an existing database
	 * @param string $database Database name (with or without prefix)
	 * @return bool
	 */
	public function removeDatabase($database)
	{

		// Input validation
		if(empty($database)) {

			Log::error("Input validation failed");
			return false;

		}

		$rc = $this->parsePrefix($database);
		if($rc === false)
			return false;

		// Validate database name
		$rc = preg_match('/[a-z_][a-z0-9_-]*[$]?/', $database);
		if($rc !== 1) {

			Log::error("Database name failed validation: " . $database);
			return false;

		}

		// Prepare statement
		$preped = $this->mdb->conn->prepare("SHOW DATABASES LIKE :database");

		// Bind parameter
		$preped->bindParam(":database", $database);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc !== true)
			return false;

		// Fetch results
		$rc = $preped->fetchAll(PDO::FETCH_NUM);
		if(
			$rc === false ||
			sizeof($rc) === 0
		) {

			Log::error("Database does not exist: " . $database);
			return false;

		}

		Log::debug("Remove database: " . $database);

		// Drop database
		// @TODO use a stored procedure because PDO can not be used with this type of query
		$rc = $this->mdb->query("DROP DATABASE `" . $database . "`");
		if($rc === false) {

			Log::error("Error while dropping database: " . $database);
			return false;

		}

		return true;

	}

	/**
	 * Grant privileges to user@database
	 * @param string $user User name (with or without prefix)
	 * @param string $database Database name (with or without prefix)
	 * @param string $priv Privileges, by default limited to: SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, TRIGGER
	 * @param string $table Table name(s), default set to all
	 * @return bool
	 */
	public function grant($user, $database, $priv = "", $table = "*")
	{

		// Input validation
		if(
			empty($user) ||
			empty($database)
		) {

			Log::error("Input validation failed");
			return false;

		}

		// Set default privileges to a limited subset
		if(empty($priv))
			$priv = "USAGE, SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, TRIGGER";

		$host = $this->parseHost($user);
		if(empty($host))
			return false;

		$rc = $this->parsePrefix($user);
		if($rc === false)
			return false;

		$rc = $this->parsePrefix($database);
		if($rc === false)
			return false;

		Log::debug("Grant privileges to: " . $user . "@" . $host . "/" . $database . "." . $table);

		// Execute GRANT for all privileges - using query() instead of a prepared statement due to bindParam() over-quoting the input
		$rc = $this->mdb->query("GRANT " . $priv . " ON `" . $database . "`." . $table . " TO '" . $user . "'@'" . $host . "'");
		if($rc === false) {

			Log::error("Error while granting privileges: " . $priv . " to " . $user . "@" . $host . "/" . $database . "." . $table);
			return false;

		}

		return true;

	}

	/**
	 * List all users associated with a virtual host
	 * @return array<array<string>>
	 */
	public function listUsers()
	{

		$vhostPrefix = $this->vhost->dbPrefix . "_%";

		// Prepare statement
		$preped = $this->mdb->conn->prepare("SELECT User, Host FROM mysql.user WHERE User LIKE :user");

		// Bind parameter
		$preped->bindParam(":user", $vhostPrefix);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc !== true)
			return array();

		// Fetch results
		$rc = $preped->fetchAll(PDO::FETCH_NUM);
		if($rc === false) {

			Log::error("Error in SELECT user for the virtual host prefix: " . $this->vhost->dbPrefix);
			return array();

		} elseif(sizeof($rc) === 0) {

			Log::warning("No users found for the virtual host prefix: " . $this->vhost->dbPrefix);
			return array();

		}

		Log::debug("Users for " . $this->vhost->dbPrefix . ": " . sizeof($rc));

		return $rc;

	}

}

