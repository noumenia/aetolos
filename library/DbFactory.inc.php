<?php
/**
 * Aetolos - Database factory
 *
 * Static database access
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage db
 */

/**
 * Database factory class
 *
 * @package aetolos
 * @subpackage db
 */
class DbFactory {

	/**
	 * Database object
	 * @var DatabaseSqlite3
	 */
	public static $db;

	/**
	 * Create a new database object and attempt to open a connection
	 * @return bool
	 */
	public static function open()
	{

		// Open a database connection
		self::$db = new DatabaseSqlite3();
		$rc = self::$db->open();
		if($rc === false)
			Log::error("Error while opening database");
		return $rc;

	}

}

