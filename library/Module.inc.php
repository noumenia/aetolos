<?php
/**
 * Aetolos - Module
 *
 * Generic module class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage module
 */

/**
 * Module class
 *
 * @package aetolos
 * @subpackage module
 */
class Module implements ModuleInterface {

	/**
	 * Smarty template engine
	 * @var Smarty
	 */
	protected $smarty;

	/**
	 * Constructor.
	 * @return void
	 */
	public function __construct()
	{

		$this->smarty = TemplateFactory::create();
		if(!$this->smarty instanceof Smarty)
			Log::error("Smarty instance error");

	}

	/**
	 * Supported module features
	 * @api
	 * @return array<string>
	 */
	public function features()
	{

		return array();

	}

	/**
	 * Supported module parameters
	 * @api
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array();

	}

	/**
	 * Required RPM packages
	 * @api
	 * @return array<string>
	 */
	public function packages()
	{

		return array();

	}

	/**
	 * Module dependency
	 * @api
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @api
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array();

	}

	/**
	 * Help
	 * @api
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array();

	}

	/**
	 * Module controller
	 * @api
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @api
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		return true;

	}

	/**
	 * Save configuration
	 * @api
	 * @return bool
	 */
	public function saveConfiguration()
	{

		return true;

	}

	/**
	 * Export virtual host
	 * @api
	 * @param VirtualHostManager $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array<string, string> $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array())
	{

		return true;

	}

	/**
	 * Enable module
	 * @api
	 * @return bool
	 */
	public function enable()
	{

		return true;

	}

	/**
	 * Disable module
	 * @api
	 * @return bool
	 */
	public function disable()
	{

		return true;

	}

	/**
	 * Start service
	 * @api
	 * @return bool
	 */
	public function start()
	{

		return true;

	}

	/**
	 * Stop service
	 * @api
	 * @return bool
	 */
	public function stop()
	{

		return true;

	}

	/**
	 * Reload service
	 * @api
	 * @return bool
	 */
	public function reload()
	{

		return true;

	}

	/**
	 * Service status
	 * @api
	 * @return string
	 */
	public function status()
	{

		return "";

	}

}

