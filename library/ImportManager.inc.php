<?php
/**
 * Aetolos - Import manager
 *
 * Manipulate tar files, convert cPanel configuration and install homedir files.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage importmanager
 */

/**
 * ImportManager implementation class
 *
 * @package aetolos
 * @subpackage importmanager
 */
class ImportManager {

	/**
	 * Read the contents of a single file within a tar archive
	 * @param string $archive Compressed backup file name (.tar.gz)
	 * @param string $fileToRead Path (within the archive) and file name to read
	 * @param bool $oneLine When enabled the function will return only the first line of the output
	 * @return string
	 */
	public static function tarReadFile($archive, $fileToRead, $oneLine = false)
	{

		// Execute tar to-stdout and return output
		if($oneLine === true)
			$rc = exec("/usr/bin/tar --extract -f " . escapeshellarg($archive) . " --to-stdout " .
				escapeshellarg("*/" . $fileToRead) . " 2>/dev/null");
		else
			$rc = shell_exec("/usr/bin/tar --extract -f " . escapeshellarg($archive) . " --to-stdout " .
				escapeshellarg("*/" . $fileToRead) . " 2>/dev/null");

		// Strict type check for null|false returned values
		if(!is_string($rc))
			$rc = "";

		return $rc;

	}

	/**
	 * List the contents of a directory within a tar archive
	 * @param string $archive Compressed backup file name (.tar.gz)
	 * @param string $dirToList Directory within the archive
	 * @return array<string>
	 */
	public static function tarReadDirectory($archive, $dirToList)
	{

		// Execute tar
		$list = shell_exec("/usr/bin/tar --list -f " . escapeshellarg($archive) . " " .
			escapeshellarg("*/" . $dirToList . "/") . " 2>/dev/null");
		if($list === false) {

			// Tar error
			Log::error("Error while listing the contents of '" . $dirToList . "' from the tar file: " . $archive);
			return array();

		} elseif($list === null) {

			// Directory not found
			return array();

		}

		// Escape directory
		$dirToList = str_replace("/", "\/", $dirToList);

		// Remove same dir referece and similar directory references
		$list = preg_replace(array('/^.*\/' . $dirToList . '\//mi', '/^.*\/.*$(?:\r\n|\n)?/m'), "", $list);
		if($list === null) {

			Log::error("Error while parsing tar file directory references: " . $archive);
			return array();

		}

		// Remove empty lines and return
		return array_filter(explode("\n", $list));

	}

	/**
	 * Check if a file exists within a tar archive
	 * @param string $archive Compressed backup file name (.tar.gz)
	 * @param string $fileToCheck Path (within the archive) and file name to check
	 * @return bool
	 */
	public static function tarFileExists($archive, $fileToCheck)
	{

		// Execute tar to-stdout and return output
		$rc = exec("/usr/bin/tar --list -f " . escapeshellarg($archive) . " " .
			escapeshellarg("*/" . $fileToCheck) . " 2>/dev/null");
		if(!empty($rc))
			return true;

		return false;

	}

	/**
	 * List the full contents (permissions, owner/group, size, date, time, name) of a directory within a tar archive
	 * @param string $archive Compressed backup file name (.tar.gz)
	 * @param string $dirToList Directory within the archive
	 * @return array<string>
	 */
	public static function tarVerboseDirectory($archive, $dirToList)
	{

		// Execute tar
		$list = shell_exec("/usr/bin/tar --list --verbose -f " . escapeshellarg($archive) . " " .
			escapeshellarg("*/" . $dirToList . "/") . " 2>/dev/null");
		if(
			$list === false ||
			$list === null
		) {

			Log::error("Error while reading tar file: " . $archive);
			return array();

		}

		// Explode to array
		$list = explode("\n", $list);

		// Loop
		foreach($list as &$l) {

			// Clean whitespace characters
			$l = preg_replace('/\s+/', " ", $l);

			// Skip empty lines
			if(
				$l === null ||
				empty($l)
			)
				continue;

			// Explode to array
			$l = explode(" ", $l);

		}

		// Remove empty lines and return result
		return array_filter($list);

	}

	/**
	 * Convert forwarders to Postfix hash format
	 * @param string $input Forwarders in old format
	 * @return string
	 */
	public static function convertForwarders($input)
	{

		// Remove colon
		$input = str_replace(": ", " ", $input);

		// Remove wildcard filters
		$rc = preg_replace('/^.*\*.*$(?:\r\n|\n)?/m', "", $input);
		if($rc === null)
			return "";

		return $rc;

	}

	/**
	 * Save data to file and properly set permissions and ownership
	 * @param VirtualHostManager $vhost Virtual host
	 * @param string $data Data to save
	 * @param string $file Full path and file name
	 * @param int $chmod Chmode in octal
	 * @param string $user User
	 * @param string $group Group
	 * @return void
	 */
	public static function installFile($vhost, &$data, $file, $chmod, $user = "", $group = "")
	{

		$rc = file_put_contents($file, $data);
		if($rc === false) {

			Log::error("Error while writing to file: " . $file);
			exit(9);

		}

		if(empty($user))
			$user = $vhost->unixName;
		if(empty($group))
			$group = $vhost->unixName;

		chown($file, $user);
		chgrp($file, $group);
		chmod($file, $chmod);

	}

	/**
	 * Install forwarders (forwarders & forwarders.db)
	 * @param VirtualHostManager $vhost Virtual host
	 * @param string $domain Domain name
	 * @param string $forwarders Forwarders
	 * @return void
	 */
	public static function installForwarder($vhost, $domain, $forwarders)
	{

		if(Config::read("postfix") === "enabled")
			$unixName = "postfix";
		else
			$unixName = $vhost->unixName;

		// Forwarder file variables
		$for1 = $vhost->home . "/" . $vhost->unixName . "/etc/" . $domain . "/forwarders";
		$for2 = $vhost->home . "/" . $vhost->unixName . "/etc/" . $domain . "/forwarders.db";

		// Avoid overwriting existing forwarder file
		if(is_file($for1)) {

			chmod($for1, 0664);
			chgrp($for1, $unixName);

		} else {

			if(!empty($forwarders)) {

				Log::debug("Installing forwarders for: " . $domain);

				// Save imported forwarders
				ImportManager::installFile($vhost, $forwarders, $for1, 0664, "", $unixName);

			} else {

				// Empty forwarders

				$emptydata = "";
				ImportManager::installFile($vhost, $emptydata, $for1, 0664, "", $unixName);

			}

		}

		// Start with an empty db
		$emptydata = "";
		ImportManager::installFile($vhost, $emptydata, $for2, 0664, "", $unixName);

		// Now generate a proper db with postmap
		exec("/usr/sbin/postmap " . escapeshellarg($for1) . " 2>/dev/null");

		// SELinux postfix access to forward & forward.db
		exec("/usr/bin/chcon -R -t postfix_etc_t " . escapeshellarg($for1) . " 2>/dev/null");
		exec("/usr/bin/chcon -R -t postfix_etc_t " . escapeshellarg($for2) . " 2>/dev/null");

	}

	/**
	 * Convert passwd to use the new uid/gid
	 * @param string $passwd Passwd file
	 * @param string $mailDir Mail directory
	 * @param string $uid User ID
	 * @param string $gid Group ID
	 * @return bool
	 */
	public static function convertPasswd($passwd, $mailDir, $uid, $gid)
	{

		// File may not exist, that is ok
		if(!is_file($passwd))
			return true;

		// Load passwd data
		$data = file_get_contents($passwd);
		if($data === false)
			return false;

		// Empty file, that is ok
		if(mb_strlen($data) == 0)
			return true;

		// Convert to an array
		$data = explode("\n", $data);

		// Remove empty lines
		$data = array_filter($data);

		// Loop
		foreach($data as &$d) {

			// Check for the existence of colon
			if(mb_strpos($d, ":") === false)
				continue;

			// Parse columns
			$d = explode(":", $d);

			// Input validation
			if(!isset($d[0], $d[1], $d[2], $d[3], $d[4], $d[5]))
				continue;

			// Set new uid
			$d[2] = $uid;

			// Set new gid
			$d[3] = $gid;

			// Mail directory
			if(!is_dir($d[5])) {

				// Generate a new mail directory
				$newMailDir = $mailDir . "/" . $d[0];

				// Test to make sure its what we expect
				if(is_dir($newMailDir)) {

					Log::warning("Backup mail directory (" . $d[5] . ") changed to: " . $newMailDir);

					$d[5] = $newMailDir;

				} else {

					Log::warning("Mail directory (" . $d[5] . ") is invalid and needs manual correction");

				}

			}

			// Force nologin shell
			$d[6] = "/sbin/nologin";

			// Convert array back to string
			$d = implode(":", $d);

		}

		// Convert array back to string
		$data = implode("\n", $data) . "\n";

		// Save to passwd file
		$rc = file_put_contents($passwd, $data);
		if($rc === false)
			return false;
		else
			return true;

	}

}

