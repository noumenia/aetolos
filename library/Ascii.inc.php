<?php
/**
 * Aetolos - ASCII drawing implementation
 *
 * Use ASCII characters to draw fancy stuff in the terminal.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage ascii
 */

/**
 * ASCII drawing class
 *
 * @package aetolos
 * @subpackage ascii
 */
class Ascii {

	/**
	 * Multibyte string pad to a certain length with another string
	 * @param string $string The string
	 * @param int $length Target length
	 * @param string $padString Pad string
	 * @param int $padType Optional pad type (STR_PAD_RIGHT, STR_PAD_LEFT, or STR_PAD_BOTH)
	 * @param string $encoding Encoding
	 * @return string
	 */
	public static function mbStrPad($string, $length, $padString = " ", $padType = STR_PAD_RIGHT, $encoding = "UTF-8")
	{
		// Get the real length with support for multibyte strings
		$realLength = mb_strlen($string, $encoding);

		// If the string is too long, then return it sized appropriately
		if($realLength > $length)
			return mb_substr($string, 0, $length, $encoding);

		// Correct length
		$length -= $realLength;

		// Padding
		$pad = str_repeat($padString, $length);

		if($padType === STR_PAD_LEFT)
			return $pad . $string;
		elseif($padType === STR_PAD_RIGHT)
			return $string . $pad;

		$split = (int)round($realLength / 2);
		return mb_substr($pad, 0, $split) . $string . mb_substr($pad, $split);

	}

	/**
	 * Ascii table
	 * @param array<array<string, int|string>> &$columns Column array
	 * @param array<array<bool|string|null>> &$data Table data in array format
	 * @return void
	 */
	public static function drawTable(&$columns, &$data)
	{

		// Loop over the data and calculate the max string length
		foreach($data as &$d) {

			// Loop over columns
			foreach($columns as &$c) {

				// Input validation
				if(!isset($c['arrayKey'])) {

					Log::error("Table column is missing arrayKey");
					die(9);

				}

				// Header
				if(!isset($c['header']))
					$c['header'] = "";

				// Cell size
				if(!isset($c['maxSize']))
					$c['maxSize'] = 0;

				// Header size
				if(is_string($d[$c['arrayKey']]))
					$size = mb_strlen(strval($d[$c['arrayKey']])) + 1;
				else
					$size = 3;

				// Size matters!
				if($size > $c['maxSize'])
					$c['maxSize'] = $size;

			}

		}

		// Clean-up
		unset($d, $c, $size);

		// Start with an empty header
		$header = "";

		// and an empty separator
		$separator = "";

		// Calculate the total number of columns
		$last = sizeof($columns);

		// Current column counter
		$counter = 0;

		// Loop over the columns
		foreach($columns as $c) {

			// Increment column counter
			$counter++;

			// Input validation
			if(!is_string($c['header']))
				$c['header'] = "";
			if(!is_int($c['maxSize']))
				$c['maxSize'] = 0;

			if($counter === 1)
				$header .= "| ";
			else
				$header .= " | ";

			// Append to the header string
			$header .= self::mbStrPad($c['header'], $c['maxSize'], " ", STR_PAD_RIGHT);

			// The last column
			if($counter >= $last)
				$header .= " |";

			// Append to the separator string
			$separator .= "+" . self::mbStrPad("", ($c['maxSize'] + 2), "-", STR_PAD_RIGHT);

			// The last column
			if($counter >= $last)
				$separator .= "+\n";

		}

		if(str_replace(array("|", " "), "", $header) === "")
			// Hide empty header
			echo $separator;
		else
			// Display header
			echo $separator . $header . "\n" . $separator;

		// Loop over data
		foreach($data as &$d) {

			// Current column counter
			$counter = 0;

			// Loop over columns
			foreach($columns as &$c) {

				// Increment column counter
				$counter++;

				// Input validation
				if(!is_int($c['maxSize']))
					$c['maxSize'] = 0;

				// Rewrite boolean values as checkmarks
				if($d[$c['arrayKey']] === true)
					$d[$c['arrayKey']] = "[✓]";
				elseif($d[$c['arrayKey']] === false)
					$d[$c['arrayKey']] = "[ ]";

				// Align left/right
				if(
					isset($c['align']) &&
					$c['align'] === "right"
				)
					$align = STR_PAD_LEFT;
				else
					$align = STR_PAD_RIGHT;

				if($counter === 1)
					echo "| ";
				else
					echo " | ";

				echo self::mbStrPad(strval($d[$c['arrayKey']]), $c['maxSize'], " ", $align);

				// The last column
				if($counter >= $last)
					echo " |";

			}

			echo "\n";

		}

		// Close the table with a separator
		echo $separator;

	}

}

