<?php
/**
 * Aetolos - Email manager
 *
 * Support and implementation for email addresses within virtual hosts.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage dovecot
 */

/**
 * Email implementation class
 *
 * @package aetolos
 * @subpackage dovecot
 */
class EmailManager {

	/**
	 * Virtual host object
	 * @var VirtualHostManager
	 */
	public $vhost;

	/**
	 * Constructor.
	 * @param VirtualHostManager $vhost Virtual host object
	 * @return void
	 */
	public function __construct($vhost)
	{

		// Set virtual host object
		$this->vhost = $vhost;

	}

	/**
	 * Email validation based on the W3C HTML5 specification
	 * @see https://www.w3.org/TR/html5/forms.html#valid-e-mail-address
	 * @param string &$string Email to validate
	 * @return bool
	 */
	private function emailValidate(&$string)
	{

		$rc = preg_match('/^[a-zA-Z0-9.!#$%&*+=?^_{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/um', $string);
		if($rc !== 1) {

			Log::error("Email validation failed");
			return false;

		}

		return true;

	}

	/**
	 * Get the number of days since 01-01-1970
	 * @return string|false
	 */
	private function getModifiedDays()
	{

		try {

			// Since 01-01-1970
			$from = new DateTime("1970-01-01");

			// Till now
			$now = new DateTime("now");

			// Get the difference
			$diff = $from->diff($now);

		} catch(\Exception $e) {

			return false;

		}

		// Return the number of days
		return $diff->format("%a");

	}

	/**
	 * Validate and get passwd and shadow files
	 * @return array<string>|false Array of: 0 => passwd file, 1 => passwd contents, 2 => shadow file, 3 => shadow contents
	 */
	private function getFiles()
	{

		// Virtual address passwd
		$passwdFile = $this->vhost->home . "/" . $this->vhost->unixName . "/etc/" . $this->vhost->domainName . "/passwd";
		if(!is_file($passwdFile)) {

			// Create empty file
			$rc = touch($passwdFile);
			if($rc === false) {

				Log::error("Error while creating passwd file: " . $passwdFile);
				return false;

			}

			$passwd = "";

		} else {

			// Load existing file
			$passwd = file_get_contents($passwdFile);
			if($passwd === false) {

				Log::error("Error while loading passwd file: " . $passwdFile);
				return false;

			}

		}

		// Get user ownership
		$uid = fileowner($passwdFile);
		if($uid === false) {

			Log::error("Error while reading the user ID of the passwd file");
			return false;

		}

		// ID to name
		$uid = posix_getpwuid($uid);
		if($uid === false) {

			Log::error("Error while reading the group owner of the passwd file");
			return false;

		}

		// If needed, set the correct user owner
		if($uid['name'] !== $this->vhost->unixName) {

			// Set proper user ownership
			$rc = chown($passwdFile, $this->vhost->unixName);
			if($rc === false) {

				Log::error("Error while setting user ownerhsip of passwd file: " . $passwdFile);
				return false;

			}

		}

		// Get group ownership
		$gid = filegroup($passwdFile);
		if($gid === false) {

			Log::error("Error while reading the group ID of the passwd file");
			return false;

		}

		// ID to name
		$gid = posix_getgrgid($gid);
		if($gid === false) {

			Log::error("Error while reading the group owner of the passwd file");
			return false;

		}

		// If needed, set the correct group owner
		if(
			Config::read("dovecot") === "enabled" &&
			$gid['name'] !== "dovecot"
		) {

			// Set proper group ownership
			$rc = chgrp($passwdFile, "dovecot");
			if($rc === false) {

				Log::error("Error while setting group (dovecot) ownerhsip of the passwd file: " . $passwdFile);
				return false;

			}

		} elseif(
			Config::read("dovecot") === "disabled" &&
			$gid['name'] !== $this->vhost->unixName
		) {

			// Set proper group ownership
			$rc = chgrp($passwdFile, $this->vhost->unixName);
			if($rc === false) {

				Log::error("Error while setting group (unixName) ownerhsip of the passwd file: " . $passwdFile);
				return false;

			}

		}

		// Check the file permissions
		if(decoct(fileperms($passwdFile) & 0777) !== "640") {

			// Set proper permissions
			$rc = chmod($passwdFile, 0640);
			if($rc === false) {

				Log::error("Error while setting file permissions (0640) of the passwd file: " . $passwdFile);
				return false;

			}

		}

		// Virtual address shadow
		$shadowFile = $this->vhost->home . "/" . $this->vhost->unixName . "/etc/" . $this->vhost->domainName . "/shadow";
		if(!is_file($shadowFile)) {

			// Create empty file
			$rc = touch($shadowFile);
			if($rc === false) {

				Log::error("Error while creating shadow file: " . $shadowFile);
				return false;

			}

			$shadow = "";

		} else {

			// Load existing file
			$shadow = file_get_contents($shadowFile);
			if($shadow === false) {

				Log::error("Error while loading shadow file: " . $shadowFile);
				return false;

			}

		}

		// Get user ownership
		$uid = fileowner($shadowFile);
		if($uid === false) {

			Log::error("Error while reading the user ID of the shadow file");
			return false;

		}

		// ID to name
		$uid = posix_getpwuid($uid);
		if($uid === false) {

			Log::error("Error while reading the group owner of the shadow file");
			return false;

		}

		// If needed, set the correct user owner
		if($uid['name'] !== $this->vhost->unixName) {

			// Set proper user ownership
			$rc = chown($shadowFile, $this->vhost->unixName);
			if($rc === false) {

				Log::error("Error while setting user ownerhsip of shadow file: " . $shadowFile);
				return false;

			}

		}

		// Get group ownership
		$gid = filegroup($shadowFile);
		if($gid === false) {

			Log::error("Error while reading the group ID of the shadow file");
			return false;

		}

		// ID to name
		$gid = posix_getgrgid($gid);
		if($gid === false) {

			Log::error("Error while reading the group owner of the shadow file");
			return false;

		}

		// If needed, set the correct group owner
		if(
			Config::read("dovecot") === "enabled" &&
			$gid['name'] !== "dovecot"
		) {

			// Set proper group ownership
			$rc = chgrp($shadowFile, "dovecot");
			if($rc === false) {

				Log::error("Error while setting group (dovecot) ownerhsip of the shadow file: " . $shadowFile);
				return false;

			}

		} elseif(
			Config::read("dovecot") === "disabled" &&
			$gid['name'] !== $this->vhost->unixName
		) {

			// Set proper group ownership
			$rc = chgrp($shadowFile, $this->vhost->unixName);
			if($rc === false) {

				Log::error("Error while setting group (unixName) ownerhsip of the shadow file: " . $shadowFile);
				return false;

			}

		}

		// Check the file permissions
		if(decoct(fileperms($shadowFile) & 0777) !== "640") {

			// Set proper permissions
			$rc = chmod($shadowFile, 0640);
			if($rc === false) {

				Log::error("Error while setting file permissions (0640) of the shadow file: " . $shadowFile);
				return false;

			}

		}

		return array($passwdFile, $passwd, $shadowFile, $shadow);

	}

	/**
	 * Explode shadow line into parts
	 * @param string &$shadowLine Line from the shadow file
	 * @param string &$partial Partial email address (without the @domain part)
	 * @param string &$shadowFile The shadow file
	 * @return array<string>|false
	 */
	private function explodeShadowParts(&$shadowLine, &$partial, &$shadowFile)
	{

		try {

			// Split the shadow line into array parts
			$shadowParts = explode(":", $shadowLine);

			if(
				!isset($shadowParts[0]) ||
				$shadowParts[0] !== $partial
			) {

				Log::error("Error while parsing partial email from shadow file: " . $shadowFile);
				return false;

			}

			if(!isset($shadowParts[1])) {

				Log::error("Error while parsing password from shadow file: " . $shadowFile);
				return false;

			}

			// Loop for all 8 parts
			for($i = 2; $i <= 8; $i++) {

				// If a part is missing, add it as an empty string
				if(!isset($shadowParts[$i]))
					$shadowParts[$i] = "";

			}

		} catch(\Exception $e) {

			Log::error("Error while parsing shadow file: " . $shadowFile);
			return false;

		}

		return $shadowParts;

	}

	/**
	 * Add a new email address
	 * @param string $email Email address (the @domain part is optional)
	 * @param string $password Password (plain text or encrypted)
	 * @param int $quota Quota (0 = unlimited)
	 * @return bool
	 */
	public function add($email, $password, $quota = 0)
	{

		// Input validation
		if(
			empty($email) ||
			empty($password) ||
			$quota < 0
		) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list($passwdFile, $passwd, $shadowFile,) = $rc;

		// Check for duplicates
		$rc = preg_match('/^' . $partial . ':/im', $passwd);
		if($rc !== 0) {

			Log::error("Duplicate email: " . $email);
			return false;

		}

		Log::debug("Add email: " . $email);

		// Find the real UID
		$uid = exec("/usr/bin/id -u " . escapeshellarg($this->vhost->unixName));
		if(!is_numeric($uid)) {

			Log::error("Error while detecting UID");
			return false;

		}

		// Find the real GID
		$gid = exec("/usr/bin/id -g " . escapeshellarg($this->vhost->unixName));
		if(!is_numeric($gid)) {

			Log::error("Error while detecting GID");
			return false;

		}

		// Generate mail directory
		$mailDir = $this->vhost->home . "/" . $this->vhost->unixName . "/mail/" . $this->vhost->domainName . "/" . $partial;
		if(!is_dir($mailDir)) {

			$rc = mkdir($mailDir);
			if($rc === false) {

				Log::error("Could not create mail directory: " . $mailDir);
				return false;

			}

		}

		// Set ownership and permissions
		chown($mailDir, $this->vhost->unixName);
		chgrp($mailDir, $this->vhost->unixName);
		chmod($mailDir, 0751);

		// Generate passwd string (user:password:uid:gid:(gecos):home:(shell):extra_fields)
		$passwd = $partial . ":x:" . $uid . ":" . $gid . "::" . $mailDir . ":/sbin/nologin\n";

		// Check for plain text password and generate a hash
		if(
			mb_stripos($password, "{MD5-CRYPT}") !== 0 &&
			mb_stripos($password, "{SHA512-CRYPT}") !== 0
		)
			$password = Config::passwordHash($password);

		// Get modified days
		$diff = $this->getModifiedDays();
		if($diff === false) {

			Log::error("Error while calculating days since 01-01-1970");
			return false;

		}

		// Generate shadow string (user:password:last password change::::::)
		$shadow = $partial . ":" . $password . ":" . $diff . "::::::\n";

		// Save passwd file
		$rc = file_put_contents($passwdFile, $passwd, FILE_APPEND);
		if($rc === false) {

			Log::error("Error while writing passwd file: " . $passwdFile);
			return false;

		}

		// Save shadow file
		$rc = file_put_contents($shadowFile, $shadow, FILE_APPEND);
		if($rc === false) {

			Log::error("Error while writing shadow file: " . $shadowFile);
			return false;

		}

		// Quota
		if($quota > 0)
			return $this->setQuota($email, $quota);

		return true;

	}

	/**
	 * Remove an email address
	 * @param string $email Email address (the @domain part is optional)
	 * @return bool
	 */
	public function remove($email)
	{

		// Input validation
		if(empty($email)) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list($passwdFile, $passwd, $shadowFile, $shadow) = $rc;

		// Check for email in passwd
		$rc = preg_match('/^' . $partial . ':/im', $passwd);
		if($rc === 0) {

			Log::error("Email address not found: " . $email);
			return false;

		}

		Log::debug("Remove email: " . $email);

		// Remove email from passwd
		$passwd = preg_replace('/^' . $partial . ':.*\n/mi', "", $passwd);

		// Remove email from shadow
		$shadow = preg_replace('/^' . $partial . ':.*\n/mi', "", $shadow);

		// Save passwd file
		$rc = file_put_contents($passwdFile, $passwd);
		if($rc === false) {

			Log::error("Error while writing passwd file: " . $passwdFile);
			return false;

		}

		// Save shadow file
		$rc = file_put_contents($shadowFile, $shadow);
		if($rc === false) {

			Log::error("Error while writing shadow file: " . $shadowFile);
			return false;

		}

		// Delete mail directory
		$mailDir = $this->vhost->home . "/" . $this->vhost->unixName . "/mail/" . $this->vhost->domainName . "/" . $partial;
		if(is_dir($mailDir))
			exec("/usr/bin/rm -rf " . escapeshellarg($mailDir));

		return true;

	}

	/**
	 * Set email address password
	 * @param string $email Email address (the @domain part is optional)
	 * @param string $password Password (plain text or encrypted)
	 * @return bool
	 */
	public function setPassword($email, $password)
	{

		// Input validation
		if(
			empty($email) ||
			empty($password)
		) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list(,, $shadowFile, $shadow) = $rc;

		// Check for email in shadow
		$rc = preg_match('/^' . $partial . ':(.*?)$/im', $shadow, $sMatch);
		if($rc === 0) {

			Log::error("Email address not found: " . $email);
			return false;

		}

		Log::debug("Set password for email: " . $email);

		$shadowParts = $this->explodeShadowParts($sMatch[0], $partial, $shadowFile);
		if($shadowParts === false)
			return false;

		// Check for plain text password and generate a hash
		if(
			mb_stripos($password, "{MD5-CRYPT}") !== 0 &&
			mb_stripos($password, "{SHA512-CRYPT}") !== 0
		)
			$password = Config::passwordHash($password);

		// Replace password
		$shadowParts[1] = $password;

		// Get modified days
		$diff = $this->getModifiedDays();
		if($diff === false) {

			Log::error("Error while calculating days since 01-01-1970");
			return false;

		}

		// Replace password modified days
		$shadowParts[2] = $diff;

		// Replace line via regular expressions with our own keyword, replaced later as a plain string
		$shadow = preg_replace('/^' . $partial . ':.*\n/mi', "{{AETOLOS}}", $shadow);
		if($shadow === null) {

			Log::error("Error while matching and replacing the shadow keyword");
			return false;

		}

		// Generate shadow string (user:password:last password change:::::allow_nets:)
		$shadow = str_replace("{{AETOLOS}}", implode(":", $shadowParts) . "\n", $shadow);

		// Save shadow file
		$rc = file_put_contents($shadowFile, $shadow);
		if($rc === false) {

			Log::error("Error while writing shadow file: " . $shadowFile);
			return false;

		}

		return true;

	}

	/**
	 * Set email address quota
	 * @param string $email Email address (the @domain part is optional)
	 * @param int $quota Quota size in bytes
	 * @return bool
	 */
	public function setQuota($email, $quota)
	{

		// Input validation
		if(
			empty($email) ||
			$quota < 0
		) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list(, $passwd,,) = $rc;

		// Check for email in passwd
		$rc = preg_match('/^' . $partial . ':/im', $passwd);
		if($rc === 0) {

			Log::error("Email address not found: " . $email);
			return false;

		}

		Log::debug("Set quota for email: " . $email);

		// Mail directory
		$mailDir = $this->vhost->home . "/" . $this->vhost->unixName . "/mail/" . $this->vhost->domainName . "/" . $partial;
		if(!is_dir($mailDir)) {

			Log::error("Could not find the mail directory: " . $mailDir);
			return false;

		}

		// Quota file
		$quotaFile = $mailDir . "/maildirsize";

		// Save quota file
		$rc = file_put_contents($quotaFile, $quota . "S,0C\n");
		if($rc === false) {

			Log::error("Error while writing quota file: " . $quotaFile);
			return false;

		}

		// Set ownership and permissions
		chown($quotaFile, $this->vhost->unixName);
		chgrp($quotaFile, $this->vhost->unixName);
		chmod($quotaFile, 0640);

		// Tell dovecot to recalculate the quota limit
		exec("/usr/bin/doveadm quota recalc -u " . escapeshellarg($email) . " 2>/dev/null");

		return true;

	}

	/**
	 * Enable an email address
	 * @param string $email Email address (the @domain part is optional)
	 * @return bool
	 */
	public function enable($email)
	{

		// Input validation
		if(empty($email)) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list(,, $shadowFile, $shadow) = $rc;

		// Check for email in shadow
		$rc = preg_match('/^' . $partial . ':(.*?)$/im', $shadow, $sMatch);
		if($rc === 0) {

			Log::error("Email address not found: " . $email);
			return false;

		}

		Log::debug("Enable email: " . $email);

		$shadowParts = $this->explodeShadowParts($sMatch[0], $partial, $shadowFile);
		if($shadowParts === false)
			return false;

		// Check for disabled email address, else check for disabled email address, else return error
		if(empty($shadowParts[7])) {

			Log::warning("Email address already enabled");
			return true;

		} elseif($shadowParts[7] === "allow_nets=127.0.0.255/32") {

			$shadowParts[7] = "";

		} else {

			Log::error("The 8th part of the shadow line contains an unknown string");
			return false;

		}

		// Replace line via regular expressions with our own keyword, replaced later as a plain string
		$shadow = preg_replace('/^' . $partial . ':.*\n/mi', "{{AETOLOS}}", $shadow);
		if($shadow === null) {

			Log::error("Error while matching and replacing the shadow keyword");
			return false;

		}

		// Generate shadow string (user:password:last password change:::::allow_nets:)
		$shadow = str_replace("{{AETOLOS}}", implode(":", $shadowParts) . "\n", $shadow);

		// Save shadow file
		$rc = file_put_contents($shadowFile, $shadow);
		if($rc === false) {

			Log::error("Error while writing shadow file: " . $shadowFile);
			return false;

		}

		return true;

	}

	/**
	 * Disable an email address
	 * @param string $email Email address (the @domain part is optional)
	 * @return bool
	 */
	public function disable($email)
	{

		// Input validation
		if(empty($email)) {

			Log::error("Input validation failed");
			return false;

		}

		// Check for @ sign and create full and partial emails
		$pos = mb_stripos($email, "@");
		if($pos === false) {

			$partial = $email;
			$email .= "@" . $this->vhost->domainName;

		} else {

			$partial = mb_substr($email, 0, $pos);

		}

		// Validate email
		$rc = $this->emailValidate($email);
		if($rc === false)
			return false;

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return false;
		else
			list(,, $shadowFile, $shadow) = $rc;

		// Check for email in shadow
		$rc = preg_match('/^' . $partial . ':(.*?)$/im', $shadow, $sMatch);
		if($rc === 0) {

			Log::error("Email address not found: " . $email);
			return false;

		}

		Log::debug("Disable email: " . $email);

		$shadowParts = $this->explodeShadowParts($sMatch[0], $partial, $shadowFile);
		if($shadowParts === false)
			return false;

		// Check for already disabled email address
		if($shadowParts[7] === "allow_nets=127.0.0.255/32") {

			Log::warning("Email address already disabled");
			return true;

		}

		// Anything else is unknown and will cause the procedure to fail
		if(!empty($shadowParts[7])) {

			Log::error("The 8th part of the shadow line contains an unknown string");
			return false;

		}

		// Prevent postfix/dovecot access
		$shadowParts[7] = "allow_nets=127.0.0.255/32";

		// Replace line via regular expressions with our own keyword, replaced later as a plain string
		$shadow = preg_replace('/^' . $partial . ':.*\n/mi', "{{AETOLOS}}", $shadow);
		if($shadow === null) {

			Log::error("Error while matching and replacing the shadow keyword");
			return false;

		}

		// Generate shadow string (user:password:last password change:::::allow_nets:)
		$shadow = str_replace("{{AETOLOS}}", implode(":", $shadowParts) . "\n", $shadow);

		// Save shadow file
		$rc = file_put_contents($shadowFile, $shadow);
		if($rc === false) {

			Log::error("Error while writing shadow file: " . $shadowFile);
			return false;

		}

		return true;

	}

	/**
	 * List all emails associated with a virtual host
	 * @return array<array<int|string>> Array of array: 0 => email, 1 => quota, 2 => % usage, 3 => last password change
	 */
	public function listEmails()
	{

		// Validate and get passwd and shadow files
		$rc = $this->getFiles();
		if($rc === false)
			return array();
		else
			list($passwdFile, $passwd,, $shadow) = $rc;

		try {

			// Split lines into an array
			$passwd = explode("\n", $passwd);

		} catch(\Exception $e) {

			Log::error("Error while loading passwd file: " . $passwdFile);
			return array();

		}

		// Get the appropriate locale
		$locale = getenv("LC_TIME");
		if(empty($locale))
			$locale = Locale::getDefault();

		// Remove the UTF-8 string which confuses the IntlDateFormatter functions
		$locale = str_replace(".UTF-8", "", $locale);

		// Create a date formatter
		$format = IntlDateFormatter::create($locale, IntlDateFormatter::SHORT, IntlDateFormatter::NONE);

		$emails = array();

		// Loop passwd lines
		foreach($passwd as &$p) {

			// Skip empty lines
			if(empty($p))
				continue;

			// Split passwd in parts
			$pParts = explode(":", $p);

			// Skip invalid lines
			if(!isset($pParts[0], $pParts[5]))
				continue;

			// Generate the mail directory
			$mailDir = $this->vhost->home . "/" . $this->vhost->unixName . "/mail/" . $this->vhost->domainName . "/" . $pParts[0];
			if($mailDir !== $pParts[5]) {

				Log::error("The virtual host mail directory does not match the" .
					" directory read from the passwd file: " . $mailDir . " !== " . $pParts[5]);
				return array();

			}

			$quota = 0;
			$usage = 0;

			if(is_dir($mailDir)) {

				if(is_file($mailDir . "/maildirsize")) {

					$quota = file_get_contents($mailDir . "/maildirsize");
					if($quota !== false) {

						$quota = explode(",", $quota);
						if(isset($quota[0]))
							$quota = intval($quota[0]);
						else
							$quota = 0;

					} else {

						$quota = 0;

					}

				}

				// Calculate directory size only if there is a quota set
				if($quota > 0) {

					// Calculate directory size
					$mailDirSize = Config::dirSize($mailDir);
					if($mailDirSize > 0) {

						$usage = round($mailDirSize / $quota * 100);
						if($usage < 0)
							$usage = 0;
						elseif($usage > 100)
							$usage = 100;

					}

				}

			}

			// Extract shadow line by email address
			$rc = preg_match('/^' . $pParts[0] . ':(.*?)$/im', $shadow, $sParts);
			if(
				$rc === false ||
				!isset($sParts[0])
			) {

				$days = "";

			} else {

				try {

					// Split shadow parts into an array
					$sParts = explode(":", $sParts[0]);
					if(
						isset($sParts[2]) &&
						!empty($sParts[2])
					) {

						$days = new DateTime("@" . (intval($sParts[2]) * 86400));
						if($format === null)
							$days = $days->format("d-m-Y");
						else
							$days = $format->format($days);

						if($days === false)
							$days = "";

					} else {

						$days = "";

					}

				} catch(\Exception $e) {

					$days = "";

				}

			}

			// If there is a quota set, then display the usage percentage
			if($quota > 0)
				$usage = $usage . "%";
			else
				$usage = "";

			$emails[] = array($pParts[0], $quota, $usage, $days);

		}

		// Clean-up
		unset($p, $pParts, $quota, $usage, $mailDirSize);

		Log::debug("Emails for " . $this->vhost->domainName . ": " . sizeof($emails));

		return $emails;

	}

}

