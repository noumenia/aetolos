<?php
/**
 * Aetolos - Helper functions for working with systemd services
 *
 * Start/stop/reload/restart systemd services
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage systemdhelper
 */

/**
 * SystemdHelper trait
 *
 * @package aetolos
 * @subpackage systemdhelper
 */
trait SystemdHelper {

	/**
	 * Require daemon reload flag
	 * @var bool
	 */
	public $daemonReload = false;

	/**
	 * Execute systemctl for all available services
	 * @param string $cmd Systemctl command (enable, disable, start, restart, stop, reload)
	 * @return bool
	 */
	protected function systemCtl($cmd)
	{

		// Do not run systemd commands on a system that is not running systemd
		if(Config::read("systemd") === false)
			return true;

		// Allowed commands
		$allowedCommands = array("enable", "disable", "start", "restart", "stop", "reload");
		if(!in_array($cmd, $allowedCommands))
			return false;

		// Get related services
		$services = $this->dependencies()['service'];

		// Get all virtual hosts
		$vhosts = VirtualHostFactory::populate();

		// Loop services
		foreach($services as $key => $service) {

			// Look for service instances (unit templates)
			if(strpos($service, "@") !== false) {

				// Loop virtual hosts
				foreach($vhosts as &$v) {

					// Skip parked domains
					if(!empty($v->parkedUnder))
						continue;

					// Add virtual host instance
					$services[] = $service . $v->domainName;

				}

				// Remove service instance
				unset($services[$key]);

			}

		}

		// SystemD controller
		if(Config::read("verbose") === true) {

			foreach($services as $service) {

				$command = "/usr/bin/systemctl " . escapeshellarg($cmd) . " " .	escapeshellarg($service);
				Log::debug($command);
				passthru($command);

			}

		} else {

			foreach($services as $service)
				exec("/usr/bin/systemctl " . escapeshellarg($cmd) . " " . escapeshellarg($service) . " 2>/dev/null");

		}

		return true;

	}

	/**
	 * Enable module and service
	 * @return bool
	 */
	public function enable()
	{

		// Loop dependencies
		foreach($this->dependencies()['module'] as $dependency) {

			// Return if a dependency is not enabled
			if(Config::read($dependency) !== "enabled") {

				Log::warning("Module is not enabled: " . $dependency);
				return false;

			}

		}

		// Get module name
		$module = strtolower(get_called_class());

		Log::debug("Enabling module: " . $module);

		// Secondary functionality from the module
		if(method_exists($this, "enableSecondary")) {

			$rc = $this->enableSecondary();
			if($rc === false)
				return false;

		}

		// Configuration
		Config::write($module, "enabled");

		// SystemD controller
		$this->systemCtl("enable");

		return true;

	}

	/**
	 * Disable module and service
	 * @return bool
	 */
	public function disable()
	{

		// Get module name
		$module = strtolower(get_called_class());

		Log::debug("Disabling module: " . $module);

		// Secondary functionality from the module
		if(method_exists($this, "disableSecondary")) {

			$rc = $this->disableSecondary();
			if($rc === false)
				return false;

		}

		// Configuration
		Config::write($module, "disabled");

		// SystemD controller
		$this->systemCtl("disable");

		return true;

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		// Loop dependencies
		foreach($this->dependencies()['module'] as $dependency) {

			// Return if a dependency is not enabled
			if(Config::read($dependency) !== "enabled") {

				Log::warning("Module is not enabled: " . $dependency);
				return false;

			}

		}

		// Get module name
		$module = strtolower(get_called_class());

		// Return if self is not enabled
		if(Config::read($module) !== "enabled") {

			Log::warning("Module is not enabled: " . $module);
			return false;

		}

		Log::debug("Starting module: " . $module);

		// SystemD controller
		$this->systemCtl("restart");

		return true;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		// Get module name
		$module = strtolower(get_called_class());

		// Return if self is not enabled
		if(Config::read($module) !== "enabled") {

			Log::warning("Module is not enabled: " . $module);
			return false;

		}

		Log::debug("Stopping module: " . $module);

		// SystemD controller
		$this->systemCtl("stop");

		return true;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		// Modules that do not support reload functionality
		$notSupported = array(
			"clamav",
			"dehydrated",
			"mariadb",
			"postgrey",
			"roundcube",
			"spamassassin",
			"virtualhost"
		);

		// Get module name
		$module = strtolower(get_called_class());

		// Fail silently if self does not support reload
		if(in_array($module, $notSupported)) {

			Log::warning("Reload not implemented or not applicable for module: " . $module);
			return false;

		}

		// Return if self is not enabled
		if(Config::read($module) !== "enabled") {

			Log::warning("Module is not enabled: " . $module);
			return false;

		}

		Log::debug("Reloading module: " . $module);

		// SystemD controller
		$this->systemCtl("reload");

		return true;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		// No service
		if(!isset($this->dependencies()['service'][0]))
			return "";

		// SystemD controller status command
		$command = "/usr/bin/systemctl --property=SubState show " .
			escapeshellarg($this->dependencies()['service'][0]) . " 2>/dev/null";

		if(Config::read("verbose") === true)
			Log::debug($command);

		// Execute systemctl
		$rc = exec($command);
		if($rc === false)
			return "";
		else
			// Return substate only
			return str_replace("SubState=", "", $rc);

	}

}

