<?php
/**
 * Log destination to array implementation class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package log
 * @subpackage log
 */
class LogDestinationArray implements LogDestinationInterface {

	/**
	 * Constructor.
	 * @param array<string, int|string> $parameters Various parameters relevant to the Destination
	 * @return void
	 */
	public function __construct($parameters = array())
	{

		// No-op

	}

	/**
	 * Write string to syslog
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		switch($priorityLevel) {

			case LOG_EMERG:
				$priorityLevelString = "[EMERGENCY] ";
				break;

			case LOG_ALERT:
				$priorityLevelString = "[ALERT] ";
				break;

			case LOG_CRIT:
				$priorityLevelString = "[CRITICAL] ";
				break;

			case LOG_ERR:
				$priorityLevelString = "[ERROR] ";
				break;

			case LOG_WARNING:
				$priorityLevelString = "[WARNING] ";
				break;

			case LOG_NOTICE:
				$priorityLevelString = "[NOTICE] ";
				break;

			case LOG_INFO:
				$priorityLevelString = "[INFO] ";
				break;

			case LOG_DEBUG:
				$priorityLevelString = "[DEBUG] ";
				break;

			default:
				$priorityLevelString = "";
				break;

		}

		// Store to array log
		Config::$log[] = $priorityLevelString . $string;

	}

}

