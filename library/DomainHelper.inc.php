<?php
/**
 * Aetolos - Helper functions for working with domain names
 *
 * Perform various tasks, like verfication, on domain names
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage domainhelper
 */

/**
 * DomainHelper trait
 *
 * @package aetolos
 * @subpackage domainhelper
 */
trait DomainHelper {

	/**
	 * Convert IDN names and verify the validity and specification constraints
	 * @param string $domainName Domain name
	 * @return string|false
	 */
	private function verifyDomainName($domainName)
	{

		Log::debug("Verify domain: " . $domainName);

		// Since PHP does not support IDN domain names, convert to ASCII by default
		if(function_exists("idn_to_ascii")) {

			$rc = idn_to_ascii(
				trim($domainName),
				IDNA_NONTRANSITIONAL_TO_ASCII,
				INTL_IDNA_VARIANT_UTS46
			);
			if($rc === false) {

				Log::error("Could not convert IDN to ASCII characters");
				return false;

			}

			$domainName = strtolower($rc);

		} else {

			$domainName = strtolower(trim($domainName));

		}

		// Validate domain name length, max length 253
		if(strlen($domainName) > 253) {

			Log::error("Domain name exceedes the max length of 253 characters: " . $domainName);
			return false;

		}

		// Make sure there is at least one "dot"
		if(strpos($domainName, ".") === false) {

			Log::error("Domain name is missing a dot character: " . $domainName);
			return false;

		}

		// Explode "dot" separated parts
		$parts = explode(".", $domainName);

		// Validate parts
		foreach($parts as $p) {

			// Max length 63 characters
			if(strlen($p) > 63) {

				Log::error("A domain name part exceedes the max length of 63 characters: " . $p);
				return false;

			}

			// Validate characters
			if(preg_match('/^[-a-z0-9]+$/i', $p) !== 1) {

				Log::error("Invalid characters in the domain name part: " . $p);
				return false;

			}

		}

		return $domainName;

	}

}

