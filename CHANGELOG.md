# Changelog Aetolos

## Development

- [general] Store and compare PID as an integer
- [general] Imrovements for strict type checking
- [clamav] ClamAV update package renamed to freshclam
- [general] Remove SORBS which bas been discontinued
- [general] Remove CentOS 7 as it has reached its EOL
- [general] Change the boolean value to support microdnf
- [roundcube] Remove dependencies to deprecated packages
- [general] Improve ASCII tables
- [general] Ensure the database prefix is unique
- [general] Allow database prefix less than 8 characters
- [postfix] Remove base64 file extension matches

## Release 2.4

- [php] Migrate old module templates to the new phpfpm module
- [apache] Remove mod_auth_digest
- [general] Ensure exec parameters are clean and in the expected type
- [general] Optimize socket read functions
- [general] Set maxMessageSize as a protected variable
- [virtualhost] Require a localhost certificate
- [general] Migrate php module parameters to phpfpm
- [roundcube] The roundcube module depends on the php-fpm module
- [php] Separate the php module from the php-fpm functions
- [phpfpm] New PHP-FPM module
- [general] Do not install weak dependencies
- [postfix] Deny relaying on port 25 from localhost
- [clamav] Oracle EL9 freshclam package name change
- [general] Use a common Module class for all modules
- [postfix] Enhance base64 file extension detection
- [general] Implement the Log facility parameter for syslog
- [postfix] Block the xlam extension
- [general] Correct effective user setting
- [opendmarc] Re-enable SPFSelfValidate after libspf2 is fixed
- [general] Enable socket reuse address mode

## Release 2.3

- [opendmarc] Disable SPFSelfValidate due to a vulnerability in libspf2
- [general] Disable slow debug messages
- [general] Improve socket read size calculation
- [general] Log to console also prints to STDERR
- [general] Log to console on system logger error
- [general] Documentation updates for the SocketManager
- [general] Make external objects more portable
- [spamassassin] Improvements for the sa-update timer service
- [general] Show systemctl errors when verbose mode is enabled
- [clamav] Improvements for the freshclam package name change
- [general] Detect if systemd is being used
- [general] Skip systemd commands if systemd is not being used
- [general] Improve support for Docker and K8S containers
- [general] Remove dependency on config-manager
- [general] Remove dependency on openssl
- [php] Support for timezone setting via /etc/localtime
- [haproxy] Make sure the /etc/sysctl.d directory exists
- [general] Re-install clamd if sample clamd.conf is missing
- [general] Support for microdnf
- [general] Add message when no proxy is detected
- [import] Override /etc/pki/tls files with symbolic links to dehydrated

## Release 2.2

- [general] Minor code improvements about empty equality
- [spamassassin] Remove the deprecated Hashcash plugin
- [clamav] Rename the update package to freshclam
- [api] OpenAPI documentation
- [api] Implement daemon configuration parser
- [api] Implement daemon help controller
- [general] Move the status command under the general section
- [general] Share sqlite database between processes
- [api] Log the remote IP address
- [general] Strict types for returned values
- [general] Move controller code into class files
- [api] Modules return data for API calls
- [api] Support for HTTP Basic authentication
- [api] Support for HTTP/HTTPS
- [api] Support for unix sockets
- [api] Support for JSON messages
- [api] Implement the Aetolos daemon
- [api] Implement the SSL/TLS SocketManager
- [api] Implement the multi-process DaemonManager

## Release 2.1

- [general] Output log messages to an array variable
- [general] Rename files and functions as StydlyCaps
- [general] Preemptively enable systemd services
- [general] Display general system information
- [general] Hide the table header when all cells are empty
- [general] Detect the proxy used by the package manager
- [dehydrated] Use the proxy to download dehydrated
- [opendmarc] Use the proxy to download the public suffix list

## Release 2.0

- [postfix] Remove the client IP address from email headers
- [mariadb] Improve the display of help text
- [dovecot] Implement enable/disable for email accounts
- [dovecot] Display the last password change date in list-emails output
- [dovecot] Remove the forward slash as an accepted email character
- [postfix] Disable SASL authentication on port 25
- [apache] Add mp4, webm and webp as static files
- [dehydrated] Update dehydrated to v0.7.1
- [dovecot] Improve the list-emails table output
- [general] PHP 8.2 compatiblity
- [general] Log destination to syslog
- [general] Improved coding style
- [dovecot] Validate quota value as a numeric integer
- [dovecot] Ensure correct file permissions
- [general] Merge common managers
- [postfix] Lower recipient limits
- [roundcube] Support for the new 1.6 configuration
- [general] Display dnf output on verbose mode
- [general] Implement ascii table output
- [virtualhost] List virtual hosts
- [dovecot] List email addresses per virtual host
- [general] Separate JSON output with a newline

## Release 1.9

- [mariadb] Lower wait_timeout from 8 hours down to 1 hour
- [export] Add the extradb parameter to pass additional databases
- [export] Log the name of each module during export
- [systemd] Use a dynamic service name for systemd instances
- [import] Do not return error if a directory is not found
- [spamassassin] Add sa-update as a service dependency
- [mariadb] Increase max connections per thread memory
- [mariadb] Use the 1% rule for the table size
- [mariadb] Use a smaller MyISAM key buffer
- [apache] Add a default value for AsyncRequestWorkerFactor
- [apache] Implement dynamic process and thread counters
- [mariadb] Max connections have a minimum value of 10 since MariaDB v10.3.6
- [mariadb] Limit the usable memory between 10% and 90% of the total memory
- [php] Increment pm.max_requests
- [apache] Enable HTTP/2
- [apache] Increment MaxConnectionsPerChild
- [general] detect CPU core count
- [php] Set max_spare_servers to half of max_children

## Release 1.8

- [general] Return output as JSON
- [general] Add new j/json parameter
- [dovecot] Improve the method that updates passwords
- [postfix] Add option check_helo_access
- [general] Refactor the autoload function
- [general] Add support for the CRB repository
- [general] Add support for the iteration Smarty variable
- [haproxy] Create required cert directory
- [haproxy] Update haproxy stats configuration parameter
- [general] Remove unused files after virtual host delete
- [opendkim] Add dependecy for opendkim-tools
- [general] Support for Rocky Linux 9
- [general] Support for Oracle Linux 9
- [general] Support for AlmaLinux 9
- [clamav] Detect freshclam preg_replace errors
- [general] Support for prepend/append template files
- [general] Optimize strict types
- [general] Detect semanage errors
- [general] Simplify the saveConfigFile function
- [general] Remove unsupported Smarty blocks
- [postfix] Add option smtpd_client_new_tls_session_rate_limit
- [postfix] Lower smtpd_client_auth_rate_limit value
- [general] Initial support for EL9
- [general] Support for DNF config-manager
- [general] Convert PDO numeric values to strings
- [mtasts] Implement support for MTA-STS
- [mariadb] Improve help messages
- [postfix] Backport updates from EL8 to EL7
- [general] Remove the deprecated smarty directory

## Release 1.7

- This release marks a milestone in the development of Aetolos. It has been 8 years of development, the first two in private development as an internal tool and six years licensed as GPLv3, placed in a public repository. To bring Aetolos in step with current development practices, we have introduced static analysis for code safety and CI/CD testing for all supported distributions.

