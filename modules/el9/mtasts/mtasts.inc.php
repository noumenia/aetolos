<?php
/**
 * Aetolos - MTA Strict Transport Security (MTA-STS)
 *
 * MTA-STS support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage mtasts
 */

/**
 * MTA-STS class
 *
 * @package aetolos
 * @subpackage mtasts
 */
class Mtasts extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"dependencies",
			"cmdParameters",
			"help",
			"controller",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'mtasts'			=> "disabled",
			'mtasts|wellknown'		=> "/var/www/html/.well-known/mta-sts.txt",
			'mtasts|mx'			=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array();

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("apache"),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"set-mx:",
			"no-mx"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=mtasts",
			array('short' => "",	'long' => "set-mx=[HOST]",		'desc' => "Custom MX to use in the mta-sts.txt file"),
			array('short' => "",	'long' => "no-mx",			'desc' => "Empty custom MX"),
			"  Use set-mx to set a custom MX or no-mx to remove the custom MX and use autodetection of the server name",
			""
		);

	}

	/**
	 * Module controller
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		if(isset($cmdParameters['set-mx'])) {

			// MX to use in the mta-sts.txt file
			Config::write("mtasts|mx", $cmdParameters['set-mx']);

		} elseif(isset($cmdParameters['no-mx'])) {

			// Use auto detection based on the server name
			Config::write("mtasts|mx", "");

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: mtasts");

		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		Log::debug("Enabling feature: mtasts");

		// Configuration
		Config::write("mtasts", "enabled");

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::debug("Disabling feature: mtasts");

		// Configuration
		Config::write("mtasts", "disabled");

		return true;

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		Log::warning("Start not implemented or not applicable for module: mtasts");

		return false;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		Log::warning("Stop not implemented or not applicable for module: mtasts");

		return false;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		Log::warning("Reload not implemented or not applicable for module: mtasts");

		return false;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		return "";

	}

}

