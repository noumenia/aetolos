<?php
/**
 * Aetolos - Apache HTTPd
 *
 * Apache support and implementation
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage apache
 */

/**
 * Apache class
 *
 * @package aetolos
 * @subpackage apache
 */
class Apache extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Virtualhost proxy template file
	 * @var string
	 */
	protected $vHostProxy = "";

	/**
	 * Per virtualhost instance template file
	 * @var string
	 */
	protected $vHostInstance = "";

	/**
	 * Constructor.
	 * @return void
	 */
	public function __construct()
	{

		// Call the module constructor
		parent::__construct();

		// Override virtualhost proxy template
		$this->vHostProxy = __DIR__ . "/virtualhostproxy.tpl";

		// Override virtualhost instance template
		$this->vHostInstance = __DIR__ . "/httpdconfinstance.tpl";

	}

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'apache'			=> "disabled",
			'apache|directoryConf'		=> "/etc/httpd/conf",
			'apache|directoryConfD'		=> "/etc/httpd/conf.d",
			'apache|directoryConfModulesD'	=> "/etc/httpd/conf.modules.d",
			'apache|maxClients'		=> 50,
			'apache|selinuxModule'		=> "",
			'apache|listenIp'		=> "",
			'apache|logrotateDHttpd'	=> "/etc/logrotate.d/httpd"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"httpd",
			"httpd-tools",
			"mod_ssl",
			"mod_security",
			"mod_http2"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array(
				"httpd",
				"httpd@"
			),
			'conflict'	=> array()
		);

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux httpd access to ~/public_html
		if(preg_match("/public_html.*httpd_sys_rw_content_t/", $contexts) === 0)
			exec("/usr/sbin/semanage fcontext -a -t httpd_sys_rw_content_t '/home/.*/public_html(/.*)?'");

		// Remove old context
		if(strpos($contexts, "/home/.*/tmp(/.*)?                                 all files          system_u:object_r:httpd_sys_rw_content_t:s0") !== false) {
			exec("/usr/sbin/semanage fcontext -d -t httpd_sys_rw_content_t '/home/.*/tmp(/.*)?' 2>/dev/null");
			$contexts = str_replace("/home/.*/tmp(/.*)?                                 all files          system_u:object_r:httpd_sys_rw_content_t:s0", "", $contexts);
		}

		// SELinux httpd access to ~/tmp
		if(preg_match("/tmp.*httpd_sys_rw_content_t/", $contexts) === 0)
			exec("/usr/sbin/semanage fcontext -a -t httpd_sys_rw_content_t '/home/[^/]+/tmp(/.*)?'");

		// SELinux - allow network access via scripts executed by Apache, if this is disabled then scripts like Roundcube can't run
		$rc = exec("/usr/sbin/getsebool httpd_can_network_connect");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_can_network_connect");

			// Enable network access
			exec("/usr/sbin/setsebool -P httpd_can_network_connect on");

		}

		// SELinux - allow Apache scripts to send emails
		$rc = exec("/usr/sbin/getsebool httpd_can_sendmail");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_can_sendmail");

			// Enable emails
			exec("/usr/sbin/setsebool -P httpd_can_sendmail on");

		}

		// SELinux - allow Apache to access home files
		$rc = exec("/usr/sbin/getsebool httpd_read_user_content");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_read_user_content");

			// Enable home access
			exec("/usr/sbin/setsebool -P httpd_read_user_content on");

		}

		// SELinux - allow Apache to access home directories
		$rc = exec("/usr/sbin/getsebool httpd_enable_homedirs");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_enable_homedirs");

			// Enable home access
			exec("/usr/sbin/setsebool -P httpd_enable_homedirs on");

		}

		// SELinux - allow Apache full access to tmp files
		if(Config::read("apache|selinuxModule") !== "installed14") {

			Log::debug("SELinux: enable httpd_home_tmp_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/httpd_home_tmp_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("apache|selinuxModule", "installed14");

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Generate a dynamic config
		$vhostLimits = ApacheHelper::generateDynamicConfig();

		// Assign variables
		$this->smarty->assign("CERTS", Config::read("pkitls|directoryCerts"));
		$this->smarty->assign("KEYS", Config::read("pkitls|directoryPrivate"));
		$this->smarty->assignByRef("VHOSTS", $vhosts);
		$this->smarty->assignByRef("VHOSTLIMITS", $vhostLimits);

		// Port counter
		$port = 8999;

		// Usable ports
		$httpPorts = array();

		// Get current SELinux status
		$status = shell_exec("/usr/sbin/sestatus | grep 'SELinux status:'");

		// If SELinux is enabled
		if(
			$status !== false &&
			$status !== null &&
			stripos($status, "enabled") !== false
		) {

			// List usable ports
			exec("/usr/sbin/semanage port --list | grep '^http_port_t '", $httpPorts, $rc);
			if(
				$rc !== 0 ||
				!is_array($httpPorts) ||
				!isset($httpPorts[0])
			)
				return false;

			// Remove characters
			$httpPorts = preg_replace('/[a-z_]/i', "", $httpPorts);
			if(!isset($httpPorts[0]))
				return false;

			// Explode into an array of ports
			$httpPorts = explode(", ", $httpPorts[0]);

			// Convert to integers
			$httpPorts = array_map("intval", $httpPorts);

			// Sort in ascending order
			asort($httpPorts);

		}

		// Loop 2 (generate virtual hosts)
		foreach($vhosts as &$v) {

			// Skip parked domains
			if(!empty($v->parkedUnder))
				continue;

			// Increment port
			$port++;

			// If our current port is not one of the usable ports
			if(
				sizeof($httpPorts) > 0 &&
				!in_array($port, $httpPorts)
			) {

				// Set empty defaults
				$output = null;
				$rc = 1;

				do {

					// Add port (if possible)
					exec("/usr/sbin/semanage port -a -t http_port_t -p tcp " . $port . " 2>/dev/null", $output, $rc);

					// If we failed to set our port
					if($rc !== 0) {

						// Move to the next one
						$port++;

						// Failsafe!
						if($port > 20000) {

							Log::error("Failed to find a viable SELinux http_port_t port");
							return false;

						}

					} else {

						// Add to usable ports
						$httpPorts[] = $port;

					}

				} while(!in_array($port, $httpPorts));

			}

			// Assign variables
			$this->smarty->assignByRef("SERVERNAME", $v->domainName);
			$this->smarty->assignByRef("UNIXNAME", $v->unixName);
			$this->smarty->assignByRef("IPADDRESS", $v->ipAddress);
			$this->smarty->assignByRef("PORT", $port);
			$this->smarty->assignByRef("HOME", $v->home);
			$this->smarty->assignByRef("PREFIXALIAS", $v->prefixAlias);
			if(is_file(Config::read("pkitls|directoryCerts") . "/" . $v->domainName . ".chain"))
				$this->smarty->assign("CHAIN", true);
			else
				$this->smarty->assign("CHAIN", false);
			if(is_file(Config::read("pkitls|directoryCerts") . "/" . $v->domainName . ".cabundle"))
				$this->smarty->assign("CABUNDLE", true);
			else
				$this->smarty->assign("CABUNDLE", false);
			$this->smarty->assignByRef("PARKEDDOMAINS", $v->parkedDomains);
			$this->smarty->assign("MTASTS", Config::read("mtasts"));

			// Save proxy configuration file
			$rc = $this->saveConfigFile($this->smarty, $this->vHostProxy, Config::read("apache|directoryConfD") . "/" . $v->domainName . ".proxy.conf");
			if($rc === false)
				return false;

			// Save instance configuration file
			$rc = $this->saveConfigFile($this->smarty, $this->vHostInstance, Config::read("apache|directoryConf") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

		}

		// Save logrotate configuration file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/logrotatehttpd.tpl", Config::read("apache|logrotateDHttpd"));
		if($rc === false)
			return false;

		// If SELinux is enabled
		if(
			$status !== false &&
			$status !== null &&
			stripos($status, "enabled") !== false
		) {

			// Loop usable ports
			foreach($httpPorts as $key => $p) {

				// Remove unused ports
				if($p > $port)
					exec("/usr/sbin/semanage port --delete -p tcp " . $p . " 2>/dev/null");

			}

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: apache");

		// Disable autoindex
		$file = Config::read("apache|directoryConfD") . "/autoindex.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Disable welcome
		$file = Config::read("apache|directoryConfD") . "/welcome.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Disable userdir
		$file = Config::read("apache|directoryConfD") . "/userdir.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Disable php
		$file = Config::read("apache|directoryConfD") . "/php.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Server name
		$serverName = php_uname("n");

		// Get all virtual hosts
		$vhosts = VirtualHostFactory::populate();

		// Flag for when the server name is also a virtual host
		$isVHost = false;

		// Loop 2 (server name)
		foreach($vhosts as &$v) {

			if($v->domainName === $serverName) {

				$isVHost = true;
				break;

			}

		}

		// Assign variables
		$this->smarty->assign("SERVERNAME", $serverName);
		$this->smarty->assign("ISVHOST", $isVHost);
		$this->smarty->assign("CERTS", Config::read("pkitls|directoryCerts"));
		$this->smarty->assign("KEYS", Config::read("pkitls|directoryPrivate"));
		if(!empty(Config::read("apache|listenIp")))
			$this->smarty->assign("SERVERIP", Config::read("apache|listenIp"));
		else
			$this->smarty->assign("SERVERIP", gethostbyname(php_uname("n")));
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.chain"))
			$this->smarty->assign("CHAIN", true);
		else
			$this->smarty->assign("CHAIN", false);
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.cabundle"))
			$this->smarty->assign("CABUNDLE", true);
		else
			$this->smarty->assign("CABUNDLE", false);

		// Generate main configuration (httpd.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/httpdconf.tpl", Config::read("apache|directoryConf") . "/httpd.conf");
		if($rc === false)
			return false;

		// Generate ssl configuration (ssl.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/sslconf.tpl", Config::read("apache|directoryConfD") . "/ssl.conf");
		if($rc === false)
			return false;

		// Generate htcacheclean configuration
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/htcacheclean.tpl", "/etc/sysconfig/htcacheclean");
		if($rc === false)
			return false;

		// Ensure the httpd service directory exists
		if(!is_dir("/etc/systemd/system/httpd.service.requires")) {

			$rc = mkdir("/etc/systemd/system/httpd.service.requires", 0755);
			if($rc === false)
				return false;

		}

		// Start the htcacheclean service when the httpd service starts
		if(!is_link("/etc/systemd/system/httpd.service.requires/htcacheclean.service")) {

			$rc = symlink("/usr/lib/systemd/system/htcacheclean.service", "/etc/systemd/system/httpd.service.requires/htcacheclean.service");
			if($rc === false)
				return false;

			// Request systemd reload
			$this->daemonReload = true;

		}

		// Required well-known directory
		if(!is_dir("/var/www/html/.well-known")) {

			// Create well-known directory
			$rc = mkdir("/var/www/html/.well-known", 0755);
			if($rc === false)
				return false;

		}

		// MTA-STS module
		if(Config::read("mtasts") === "enabled") {

			// Use the server name as an MX or use a custom MX
			if(empty(Config::read("mtasts|mx")))
				$this->smarty->assign("MX", php_uname("n"));
			else
				$this->smarty->assign("MX", Config::read("mtasts|mx"));

			// Save MTA-STS well-known file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mta-sts.tpl", Config::read("mtasts|wellknown"));
			if($rc === false)
				return false;

		}

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		return true;

	}

}

