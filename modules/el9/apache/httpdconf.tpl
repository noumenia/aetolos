# Aetolos - Automatically generated Apache configuration

ServerRoot "/etc/httpd"
Listen 80

# base
LoadModule alias_module modules/mod_alias.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule authz_core_module modules/mod_authz_core.so
LoadModule cache_module modules/mod_cache.so
LoadModule cache_disk_module modules/mod_cache_disk.so
LoadModule headers_module modules/mod_headers.so
LoadModule reqtimeout_module modules/mod_reqtimeout.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule socache_shmcb_module modules/mod_socache_shmcb.so
LoadModule unixd_module modules/mod_unixd.so

# mpm
LoadModule mpm_event_module modules/mod_mpm_event.so

# proxy
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so

# ssl
LoadModule ssl_module modules/mod_ssl.so

# systemd
LoadModule systemd_module modules/mod_systemd.so

# h2
LoadModule http2_module modules/mod_http2.so
LoadModule proxy_http2_module modules/mod_proxy_http2.so

User apache
Group apache
ServerName {$SERVERNAME}
DocumentRoot /var/www/html
ServerAdmin webmaster@{$SERVERNAME}
Options -ExecCGI -Includes
TraceEnable Off
ServerSignature Off
ServerTokens ProductOnly
FileETag MTime
KeepAlive On
MaxKeepAliveRequests 100
MaxConnectionsPerChild 100000
UseCanonicalName Off
Protocols h2 http/1.1
LimitInternalRecursion 5
LimitRequestBody 51200000
LimitRequestFields 500
AcceptPathInfo Off
AllowEncodedSlashes NoDecode
HttpProtocolOptions Strict LenientMethods
MaxRanges 100
Timeout 8
KeepAliveTimeout 4
ProxyTimeout 121
RequestReadTimeout header=5 body=20,MinRate=500

# Default limits
#ServerLimit 16
#ThreadsPerChild 25
#ThreadLimit 64
#MaxRequestWorkers 400

# Medium server
#ServerLimit 32
#ThreadsPerChild 64
#ThreadLimit 128
#MaxRequestWorkers 2048

# Large server
#ServerLimit 64
#ThreadsPerChild 128
#ThreadLimit 256
#MaxRequestWorkers 8192

CacheEnable disk /
CacheRoot "/var/cache/httpd/proxy/"
CacheDefaultExpire 86400
CacheIgnoreHeaders Set-Cookie
CacheMaxExpire 1209600
CacheIgnoreNoLastMod On
CacheDirLevels 2
CacheDirLength 1
CacheMaxFileSize 100000000

<Directory />
	AllowOverride none
	Require all denied
</Directory>

<Directory "/var/www/html">
	<LimitExcept GET POST>
		Require all denied
	</LimitExcept>
</Directory>

ErrorLog "logs/error_log"
LogLevel warn

EnableMMAP on
EnableSendfile on
{if isset($ISVHOST) && $ISVHOST===false}
<VirtualHost *:80>

	ServerName {$SERVERNAME}
	DocumentRoot /var/www/html

	<Directory "/var/www/html/.well-known">
		Require all granted
		<LimitExcept GET HEAD>
			Require all denied
		</LimitExcept>
	</Directory>

</VirtualHost>

<VirtualHost *:443>

	ServerName {$SERVERNAME}
	DocumentRoot /var/www/html

	SSLEngine on
	SSLCertificateFile {$CERTS}/localhost.crt
	SSLCertificateKeyFile {$KEYS}/localhost.key
	{if isset($CHAIN) && $CHAIN===true}SSLCertificateChainFile {$CERTS}/localhost.chain{/if} 
	{if isset($CABUNDLE) && $CABUNDLE===true}SSLCACertificateFile {$CERTS}/localhost.cabundle{/if} 

</VirtualHost>
{/if}
IncludeOptional conf.d/*.conf

