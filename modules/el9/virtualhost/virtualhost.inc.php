<?php
/**
 * Aetolos - Virtual host
 *
 * Virtual host support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage virtualhost
 */

/**
 * VirtualHost class
 *
 * @package aetolos
 * @subpackage virtualhost
 */
class VirtualHost extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"cmdParameters",
			"help",
			"controller",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array('virtualhost' => "enabled");

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array();

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"list-virtualhosts",
			"add-virtualhost:",
			"import:",
			"ipaddress:",
			"home:",
			"admin-email:",
			"ns:",
			"mx:",
			"remove-virtualhost:",
			"keep-home",
			"virtualhost:",
			"add-pdomain:",
			"remove-pdomain:",
			"set-prefix:",
			"no-prefix"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=virtualhost",
			array('short' => "",	'long' => "list-virtualhosts",			'desc' => "List virtual hosts"),
			"",
			array('short' => "",	'long' => "add-virtualhost=[DOMAIN]",		'desc' => "Add a new virtual host"),
			array('short' => "",	'long2' => "ipaddress=[IP]",			'desc' => "Use specific IP address	(default: main server IP)"),
			array('short' => "",	'long2' => "home=[DIR]",			'desc' => "Use specific home directory	(default: /home)"),
			array('short' => "",	'long2' => "admin-email=[DIR]",			'desc' => "Administrator/owner email	(default: none)"),
			array('short' => "",	'long2' => "set-prefix=[PREFIX]",		'desc' => "Set a domain prefix alias	(default: www)"),
			array('short' => "",	'long2' => "no-prefix",				'desc' => "Empty domain prefix alias"),
			array('short' => "",	'long2' => "ns=[HOST]",				'desc' => "Specify NS name servers	(default: defined by registrar)"),
			array('short' => "",	'long2' => "mx=[HOST]",				'desc' => "Specify MX mail servers	(default: virtual host name)"),
			"  Set the --ns and --mx parameters multiple times to add multiple hosts",
			"",
			array('short' => "",	'long' => "remove-virtualhost=[DOMAIN]",	'desc' => "Remove a virtual host"),
			array('short' => "",	'long2' => "keep-home",				'desc' => "Keep user home directory 	(default: is to delete)"),
			"",
			array('short' => "",	'long' => "virtualhost=[DOMAIN]",		'desc' => "Select the virtual host to modify with the following commands"),
			array('short' => "",	'long2' => "add-pdomain=[DOMAIN]",		'desc' => "Add new parked domain name to the selected virtual host"),
			array('short' => "",	'long2' => "remove-pdomain=[DOMAIN]",		'desc' => "Remove a parked domain and delete all email data"),
			array('short' => "",	'long2' => "set-prefix=[PREFIX]",		'desc' => "Set a domain prefix alias	(default: www)"),
			array('short' => "",	'long2' => "no-prefix",				'desc' => "Empty domain prefix alias"),
			"  Use set-prefix to add an alias like www to DOMAIN or no-prefix to remove the alias",
			""
		);

	}

	/**
	 * Module controller
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		// Create virtual host object
		$vhost = new VirtualHostManager();

		if(isset($cmdParameters['list-virtualhosts'])) {

			$data = $vhost->listVirtualHosts();

			// JSON output
			if(Config::read("json") === true) {

				// Loop the data
				foreach($data as &$d) {

					// Rewrite parked domain ID
					if(!empty($d[15]))
						$d[15] = true;
					elseif(empty($d[15]))
						$d[15] = false;

					// Remove unused items from the array
					unset($d[5], $d[6], $d[7], $d[8], $d[9], $d[10], $d[11], $d[12], $d[13], $d[14]);

					$d = array_values($d);

				}

				if(Config::read("daemon") === true)
					// Return data to the daemon
					return $data;
				else
					// Generate a JSON encoded object
					echo json_encode(array('VirtualHosts' => $data));

			} else {

				// Columns
				$columns = array(
					array(
						'header'	=> "ID",
						'arrayKey'	=> 0,
						'maxSize'	=> 3
					),
					array(
						'header'	=> "Timestamp",
						'arrayKey'	=> 1,
						'maxSize'	=> 10
					),
					array(
						'header'	=> "Virtual host",
						'arrayKey'	=> 2,
						'maxSize'	=> 13
					),
					array(
						'header'	=> "System user",
						'arrayKey'	=> 3,
						'maxSize'	=> 12
					),
					array(
						'header'	=> "MariaDB prefix",
						'arrayKey'	=> 4,
						'maxSize'	=> 15
					),
					array(
						'header'	=> "Parked",
						'arrayKey'	=> 15,
						'maxSize'	=> 7
					),
					array(
						'header'	=> "Prefix alias",
						'arrayKey'	=> 16,
						'maxSize'	=> 13
					)
				);

				// Loop over the data
				foreach($data as &$v) {

					// Loop over columns
					foreach($columns as &$c) {

						// Rewrite parked domain ID
						if(
							$c['arrayKey'] === 15 &&
							!empty($v[$c['arrayKey']])
						)
							$v[$c['arrayKey']] = true;
						elseif($c['arrayKey'] === 15)
							$v[$c['arrayKey']] = false;

					}

				}

				Ascii::drawTable($columns, $data);

			}

		} elseif(isset($cmdParameters['add-virtualhost'])) {

			// Set parameters - domain name
			$vhost->domainName = $cmdParameters['add-virtualhost'];

			// Set parameters - ip address
			if(isset($cmdParameters['ipaddress']))
				$vhost->ipAddress = $cmdParameters['ipaddress'];

			// Set parameters - home directory
			if(isset($cmdParameters['home']))
				$vhost->home = $cmdParameters['home'];

			// Set parameters - administrator/owner email address
			if(isset($cmdParameters['admin-email']))
				$vhost->adminEmail = $cmdParameters['admin-email'];

			// Set parameters - account quota
			if(isset($cmdParameters['quota']))
				$vhost->quota = intval($cmdParameters['quota']);

			// Set parameters - domain prefix alias
			if(isset($cmdParameters['set-prefix']))
				$vhost->prefixAlias = $cmdParameters['set-prefix'];
			elseif(isset($cmdParameters['no-prefix']))
				$vhost->prefixAlias = "";

			// Set parameters - name server(s)
			if(isset($cmdParameters['ns']) && is_array($cmdParameters['ns']))
				$vhost->ns = $cmdParameters['ns'];
			elseif(isset($cmdParameters['ns']))
				$vhost->ns = array($cmdParameters['ns']);

			// Set parameters - mail exchange server(s)
			if(isset($cmdParameters['mx']) && is_array($cmdParameters['mx']))
				$vhost->mx = $cmdParameters['mx'];
			elseif(isset($cmdParameters['mx']))
				$vhost->mx = array($cmdParameters['mx']);

			// Create virtual host and insert to database
			$rc = $vhost->insert();
			if($rc === false) {

				Log::error("Error while adding virtual host: " . $vhost->domainName);
				return false;

			}

		} elseif(isset($cmdParameters['remove-virtualhost'])) {

			// Set parameters
			if(isset($cmdParameters['keep-home']))
				$keepHome = true;
			else
				$keepHome = false;

			// Delete from database
			$rc = $vhost->delete($cmdParameters['remove-virtualhost'], $keepHome);
			if($rc === false) {

				Log::error("Error while deleting virtual host: " . $cmdParameters['remove-virtualhost']);
				return false;

			}

		} elseif(isset($cmdParameters['virtualhost'])) {

			// Set parameters
			$vhost->domainName = $cmdParameters['virtualhost'];

			// Get selected virtual host
			$rc = $vhost->get();
			if($rc === false) {

				Log::error("Unknown virtual host");
				return false;

			}

			if(isset($cmdParameters['add-pdomain'])) {

				// Set parameters - domain prefix alias
				if(isset($cmdParameters['set-prefix']))
					$vhost->prefixAlias = $cmdParameters['set-prefix'];
				elseif(isset($cmdParameters['no-prefix']))
					$vhost->prefixAlias = "";

				// Set parameters - name server(s)
				if(isset($cmdParameters['ns']) && is_array($cmdParameters['ns']))
					$ns = $cmdParameters['ns'];
				elseif(isset($cmdParameters['ns']))
					$ns = array($cmdParameters['ns']);
				else
					$ns = array();

				// Set parameters - mail exchange server(s)
				if(isset($cmdParameters['mx']) && is_array($cmdParameters['mx']))
					$mx = $cmdParameters['mx'];
				elseif(isset($cmdParameters['mx']))
					$mx = array($cmdParameters['mx']);
				else
					$mx = array();

				$rc = $vhost->addParkedDomain($cmdParameters['add-pdomain'], $ns, $mx);
				if($rc === false) {

					Log::error("Error while adding new parked domain");
					return false;

				}

			} elseif(isset($cmdParameters['remove-pdomain'])) {

				$rc = $vhost->removeParkedDomain($cmdParameters['remove-pdomain']);
				if($rc === false) {

					Log::error("Error while removing parked domain");
					return false;

				}

			} elseif(isset($cmdParameters['set-prefix'])) {

				// Set parameters - domain prefix alias
				$vhost->prefixAlias = $cmdParameters['set-prefix'];

				// Update virtual host with new prefix alias
				$rc = $vhost->update();
				if($rc === false) {

					Log::error("Error while updating virtual host: " . $vhost->domainName);
					return false;

				}

			} elseif(isset($cmdParameters['no-prefix'])) {

				// Set parameters - remove domain prefix
				$vhost->prefixAlias = "";

				// Update virtual host with an empty prefix alias
				$rc = $vhost->update();
				if($rc === false) {

					Log::error("Error while updating virtual host: " . $vhost->domainName);
					return false;

				}

			} else {

				Log::error("Parameter error");
				return false;

			}

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: virtualhost");

		// Get all virtual hosts
		$vhosts = VirtualHostFactory::populate();

		// Assign variables
		$this->smarty->assignByRef("VHOSTS", $vhosts);

		// Save systemd-tmpfiles config
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/hometmp.tpl", "/etc/tmpfiles.d/hometmp.conf");
		if($rc === false)
			return false;

		return true;

	}

	/**
	 * Export virtual host
	 * @param object $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array<string, string> $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array())
	{

		// By default, modules do not export anything
		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		Log::warning("Enable not implemented or not applicable for module: virtualhost");

		return false;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::warning("Disable not implemented or not applicable for module: virtualhost");

		return false;

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		Log::warning("Start not implemented or not applicable for module: virtualhost");

		return false;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		Log::warning("Stop not implemented or not applicable for module: virtualhost");

		return false;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		Log::warning("Reload not implemented or not applicable for module: virtualhost");

		return false;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		return "";

	}

}

