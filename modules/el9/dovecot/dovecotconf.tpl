# Aetolos - Automatically generated Dovecot configuration

protocols = imap pop3 lmtp
listen = *, [::]
login_greeting = Server ready.
!include conf.d/*.conf
!include_try local.conf

