# Aetolos - Automatically generated Dovecot configuration

mail_home = /etc/dovecot/vhost/%d
mail_location = maildir:~/
namespace inbox {
	type = private
	inbox = yes
}
first_valid_uid = 1000
first_valid_gid = 1000
mail_plugin_dir = /usr/lib64/dovecot
mail_plugins = $mail_plugins zlib quota
maildir_copy_with_hardlinks = yes
maildir_very_dirty_syncs = yes
mbox_write_locks = fcntl

