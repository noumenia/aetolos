# Aetolos - Automatically generated Dovecot configuration

imap_logout_format = in=%i, out=%o, bytes=%i/%o
imap_capability = +NAMESPACE
imap_idle_notify_interval = 20 mins
protocol imap {
	mail_plugins = $mail_plugins acl quota imap_quota imap_zlib
	mail_max_userip_connections = 50
}

