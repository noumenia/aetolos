# Aetolos - Automatically generated Dovecot configuration

namespace inbox {
	mailbox Drafts {
		special_use = \Drafts
		auto = subscribe
	}
	mailbox Junk {
		special_use = \Junk
		auto = create
	}
	mailbox Trash {
		special_use = \Trash
		auto = subscribe
	}
	mailbox Sent {
		special_use = \Sent
		auto = subscribe
	}
	mailbox "Sent Messages" {
		special_use = \Sent
		auto = no
	}
	mailbox Archive {
		special_use = \Archive
		auto = create
	}
	mailbox "Archives" {
		special_use = \Archive
		auto = no
	}
}

