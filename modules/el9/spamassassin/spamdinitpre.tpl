# Aetolos - Automatically generated SpamAssassin configuration

# RelayCountry - add metadata for Bayes learning, marking the countries
# a message was relayed through
#
# Note: This requires the Geo::IP Perl module
#
# loadplugin Mail::SpamAssassin::Plugin::RelayCountry

# URIDNSBL - look up URLs found in the message against several DNS
# blocklists.
#
loadplugin Mail::SpamAssassin::Plugin::URIDNSBL

# SPF - perform SPF verification.
#
{if isset($SPFSA) && $SPFSA==true}
loadplugin Mail::SpamAssassin::Plugin::SPF
{else}
# DISABLED - use postfix SPF checks for improved performance
#
#loadplugin Mail::SpamAssassin::Plugin::SPF
{/if}

