# Aetolos - Automatically generated Postgrey configuration

POSTGREY_TYPE="--unix=/var/spool/postfix/postgrey/socket"
POSTGREY_PID="--pidfile=/run/postgrey.pid"
POSTGREY_GROUP="--group=postgrey"
POSTGREY_USER="--user=postgrey"
POSTGREY_DELAY="--delay=60"
POSTGREY_OPTS="--auto-whitelist-clients=3 --max-age=60"

