<?php
/**
 * Aetolos - Postfix
 *
 * Postfix support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage postfix
 */

/**
 * Postfix class
 *
 * @package aetolos
 * @subpackage postfix
 */
class Postfix extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Virtual hosts
	 * @var array<string>
	 */
	private $vhosts = array();

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'postfix'			=> "disabled",
			'postfix|postfixDirectory'	=> "/etc/postfix",
			'postfix|virtualDomainsFile'	=> "/etc/postfix/virtual_mailbox_domains",
			'postfix|selinuxModule'		=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array("postfix");

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array("crb"),
			'service'	=> array("postfix"),
			'conflict'	=> array()
		);

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux postfix access to forward/forward.db
		if(preg_match('/forwarders.*postfix_etc_t/', $contexts) === 0)
			exec("/usr/sbin/semanage fcontext -a -t postfix_etc_t -f f '/home/.*/etc/.*/forwarders(\.db)?'");

		// SELinux - allow Postfix to access clamav-milter socket
		if(Config::read("postfix|selinuxModule") !== "installed12") {

			Log::debug("SELinux: enable postfix_socket_clamav_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/postfix_socket_clamav_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("postfix|selinuxModule", "installed12");

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Get all virtual hosts and generate virtual configuration
		$this->vhosts = array();
		DbFactory::$db->query("SELECT * FROM `virtualHost`");
		while(DbFactory::$db->nextRow()) {

			// Store in array
			$this->vhosts[] = DbFactory::$db->row;

			// Make sure the forwarders file exists
			$forwarderFile = DbFactory::$db->row[6] . "/" . DbFactory::$db->row[3] . "/etc/" . DbFactory::$db->row[2] . "/forwarders";
			if(!is_file($forwarderFile)) {

				// Create empty file
				$rc = touch($forwarderFile);
				if($rc === false) {
					Log::error("Error while creating forwarder file: " . $forwarderFile);
					return false;
				}

				// Set proper user ownership
				$rc = chown($forwarderFile, DbFactory::$db->row[3]);
				if($rc === false) {
					Log::error("Error while setting user ownerhsip of forwarder file: " . $forwarderFile);
					return false;
				}

				// Set proper group ownership
				$rc = chgrp($forwarderFile, "dovecot");
				if($rc === false) {
					Log::error("Error while setting group ownerhsip of forwarder file: " . $forwarderFile);
					return false;
				}

				// Compile with postmap
				exec("/usr/sbin/postmap " . escapeshellarg($forwarderFile));

			}

		}

		// Assign variables
		$this->smarty->assignByRef("VHOSTS", $this->vhosts);

		// Generate virtual_mailbox_domains file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/virtualdomainsfile.tpl", Config::read("postfix|virtualDomainsFile"));
		if($rc === false)
			return false;

		// Compile with postmap
		exec("/usr/sbin/postmap " . escapeshellarg(Config::read("postfix|virtualDomainsFile")));

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: postfix");

		// Save virtual hosts
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		// Set permissions
		chmod(Config::read("postfix|virtualDomainsFile"), 0640);

		// Assign variables
		$this->smarty->assignByRef("VHOSTS", $this->vhosts);
		$this->smarty->assign("CLAMAV", Config::read("clamav"));
		$this->smarty->assign("SPAMASSASSIN", Config::read("spamassassin"));
		$this->smarty->assign("OPENDKIM", Config::read("opendkim", true));
		$this->smarty->assign("OPENDMARC", Config::read("opendmarc", true));
		$this->smarty->assign("POSTGREY", Config::read("postgrey", true));
		$this->smarty->assign("DEHYDRATED", Config::read("dehydrated"));

		/*
		 * The following part checks for the existence of:
		 *
		 * Perl implementation of policyd-spf which can be downloaded from:
		 * https://launchpad.net/postfix-policyd-spf-perl/
		 * (requires manual copy of the file under /usr/local/lib/policyd-spf-perl/)
		 *
		 * Python implementation of policyd-spf which can be installed
		 * via the rpm package 'pypolicyd-spf'
		 * (yum install pypolicyd-spf)
		 *
		 * Warning: the Perl implementation seems abandoned
		 */

		if(is_file("/usr/local/lib/policyd-spf-perl/postfix-policyd-spf-perl")) {

			// Detected policyd-spf (perl)

			Log::debug("Detected: policyd-spf (perl)");

			// Assign variables
			$this->smarty->assign("SPFPERL", true);
			$this->smarty->assign("SPFPYTH", false);

		} elseif(is_file("/usr/libexec/postfix/policyd-spf")) {

			// Detected policyd-spf (python)

			Log::debug("Detected: policyd-spf (python)");

			// Assign variables
			$this->smarty->assign("SPFPERL", false);
			$this->smarty->assign("SPFPYTH", true);

		} else {

			// Assign variables
			$this->smarty->assign("SPFPERL", false);
			$this->smarty->assign("SPFPYTH", false);

		}

		// Postscreen access cidr file
		$postscreenAccess = Config::read("postfix|postfixDirectory") . "/postscreen_access.cidr";
		if(!is_file($postscreenAccess)) {

			// Create empty file
			$rc = touch($postscreenAccess);
			if($rc === false) {
				Log::error("Error while creating postscreen access cidr file: " . $postscreenAccess);
				return false;
			}

		}

		// postmap
		exec("/usr/sbin/postmap " . escapeshellarg($postscreenAccess));

		// HELO access hash file
		$heloAccess = Config::read("postfix|postfixDirectory") . "/helo_access";
		if(!is_file($heloAccess)) {

			// Create empty file
			$rc = touch($heloAccess);
			if($rc === false) {
				Log::error("Error while creating HELO access hash file: " . $heloAccess);
				return false;
			}

		}

		// postmap
		exec("/usr/sbin/postmap " . escapeshellarg($heloAccess));

		// Sender domain checks file
		$senderdomainChecks = Config::read("postfix|postfixDirectory") . "/sender_domain_checks";
		if(!is_file($senderdomainChecks)) {

			// Create empty file
			$rc = touch($senderdomainChecks);
			if($rc === false) {
				Log::error("Error while creating sender domain checks file: " . $senderdomainChecks);
				return false;
			}

		}

		// Generate transport
		$transport = Config::read("postfix|postfixDirectory") . "/transport";
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/transport.tpl", $transport);
		if($rc === false)
			return false;

		// postmap
		exec("/usr/sbin/postmap " . escapeshellarg($transport));

		// Generate smtp_header_checks
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/smtpheaderchecks.tpl", Config::read("postfix|postfixDirectory") . "/smtp_header_checks");
		if($rc === false)
			return false;

		// Generate mime_header_checks
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mimeheaderchecks.tpl", Config::read("postfix|postfixDirectory") . "/mime_header_checks");
		if($rc === false)
			return false;

		// Generate main.cf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/maincf.tpl", Config::read("postfix|postfixDirectory") . "/main.cf");
		if($rc === false)
			return false;

		// Generate master.cf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mastercf.tpl", Config::read("postfix|postfixDirectory") . "/master.cf");
		if($rc === false)
			return false;
		else
			return true;

	}

}

