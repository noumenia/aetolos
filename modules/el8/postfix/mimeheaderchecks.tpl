# Aetolos - Automatically generated Postfix configuration

# Reject file attachment double extensions like doc.htm (ascii)
{literal}/^Content-(Disposition|Type).*name\s*=\"(.*)\.[0-z]{3,4}\.html?(\?=)?"?\s*(;|$)/ REJECT Illegal file attachment format{/literal}

# Reject emails based on their file attachment extension (ascii)
{literal}/^Content-(Disposition|Type).*name\s*=\s*"?([^;]*(\.|=2E)(7z|386|r\d\d))(\?=)?"?\s*(;|$)/ REJECT Illegal file attachment format{/literal}

