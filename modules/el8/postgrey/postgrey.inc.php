<?php
/**
 * Aetolos - Postgrey
 *
 * Postgrey support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage postgrey
 */

/**
 * Postgrey class
 *
 * @package aetolos
 * @subpackage postgrey
 */
class PostGrey extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'postgrey'			=> "disabled",
			'postgrey|postgreyConfFile'	=> "/etc/sysconfig/postgrey"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array("postgrey");

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("postfix"),
			'repository'	=> array("epel-release"),
			'service'	=> array("postgrey"),
			'conflict'	=> array()
		);

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: postgrey");

		// Postgrey config file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/postgreyconffile.tpl", Config::read("postgrey|postgreyConfFile"));
		if($rc === false)
			return false;
		else
			return true;

	}

}

