# Aetolos - Automatically generated Apache virtual host configuration

ServerRoot "/etc/httpd"
DefaultRuntimeDir /run/httpd/instance-{literal}${HTTPD_INSTANCE}{/literal}
PidFile /run/httpd/instance-{literal}${HTTPD_INSTANCE}{/literal}.pid
Listen 127.0.0.1:{$PORT} http

# base
LoadModule alias_module modules/mod_alias.so
LoadModule allowmethods_module modules/mod_allowmethods.so
LoadModule auth_basic_module modules/mod_auth_basic.so
LoadModule authn_core_module modules/mod_authn_core.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule authz_core_module modules/mod_authz_core.so
LoadModule authz_host_module modules/mod_authz_host.so
LoadModule brotli_module modules/mod_brotli.so
LoadModule deflate_module modules/mod_deflate.so
LoadModule dir_module modules/mod_dir.so
LoadModule expires_module modules/mod_expires.so
LoadModule headers_module modules/mod_headers.so
LoadModule filter_module modules/mod_filter.so
LoadModule log_config_module modules/mod_log_config.so
LoadModule mime_magic_module modules/mod_mime_magic.so
LoadModule mime_module modules/mod_mime.so
LoadModule negotiation_module modules/mod_negotiation.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule setenvif_module modules/mod_setenvif.so
LoadModule socache_shmcb_module modules/mod_socache_shmcb.so
LoadModule unixd_module modules/mod_unixd.so

# event
LoadModule mpm_event_module modules/mod_mpm_event.so

# systemd
LoadModule systemd_module modules/mod_systemd.so

# proxy
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so
LoadModule proxy_http_module modules/mod_proxy_http.so

# h2
LoadModule http2_module modules/mod_http2.so

# env
LoadModule env_module modules/mod_env.so

# remote ip
LoadModule remoteip_module modules/mod_remoteip.so

User {$UNIXNAME}
Group {$UNIXNAME}
ServerName {$SERVERNAME}
DocumentRoot {$HOME}/{$UNIXNAME}/public_html
ServerAdmin webmaster@{$SERVERNAME}
Options -ExecCGI -Includes -FollowSymLinks +SymLinksIfOwnerMatch
TraceEnable Off
ServerSignature Off
ServerTokens ProductOnly
FileETag MTime
KeepAlive On
MaxKeepAliveRequests 100
MaxConnectionsPerChild 100000
UseCanonicalName Off
Protocols h2c http/1.1
H2Direct on
LimitInternalRecursion 5
LimitRequestBody 51200000
LimitRequestFields 500
AcceptPathInfo Off
AllowEncodedSlashes NoDecode
HttpProtocolOptions Strict LenientMethods
MaxRanges 100
Timeout 8
KeepAliveTimeout 4

RemoteIPHeader X-Forwarded-For
RemoteIPInternalProxy 127.0.0.1
ProxyTimeout 120

# Per back-end limits
{foreach $VHOSTLIMITS as $key => $P}{$key} {$P}
{/foreach}

{literal}
LogFormat "%a %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
CustomLog "logs/${HTTPD_INSTANCE}.log" combined
ErrorLog "logs/${HTTPD_INSTANCE}_error.log"
LogLevel warn
{/literal}

TypesConfig /etc/mime.types
AddType application/x-compress .Z
AddType application/x-gzip .gz .tgz
AddType text/html .shtml
AddOutputFilter INCLUDES .shtml
AddDefaultCharset UTF-8
MIMEMagicFile conf/magic

<Files ".user.ini">
    Require all denied
</Files>
AddType text/html .php
DirectoryIndex index.html index.php

EnableMMAP on
EnableSendfile on

{if isset($MAXCLIENTS) && $MAXCLIENTS!=''}MaxClientsVHost {$MAXCLIENTS}{/if}
{literal}
<If "%{HTTP:X-Forwarded-Proto} == 'https'">
	SetEnv HTTPS on
</If>
{/literal}

Header always set Referrer-Policy "strict-origin-when-cross-origin"
Header always set X-Xss-Protection "1; mode=block"
Header always set X-Content-Type-Options "nosniff"
Header always set X-Frame-Options "SAMEORIGIN"
Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
Header always set Permissions-Policy "interest-cohort=()"

<Directory />
	AllowOverride none
	Require all denied
</Directory>

<Files ".ht*">
	Require all denied
</Files>

AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css application/javascript application/xml application/xhtml+xml application/rss+xml font/woff font/woff2 image/svg+xml

<Files ~ "\.(css\.gz|js\.gz|css|js|jpg|jpeg|gif|png|bmp|tif|mov|mp4|mpg|mpeg|avi|mkv|pdf|ico|swf|webm|webp|woff|woff2|eot|svg|ttf)$">
	ExpiresActive on
	ExpiresDefault "access plus 1 month"
	Header unset Set-Cookie env=unset-cookie
</Files>

Alias /.well-known /var/www/html/.well-known
<Directory "/var/www/html/.well-known">
	Require all granted
	<LimitExcept GET HEAD>
		Require all denied
	</LimitExcept>
</Directory>

<Directory "{$HOME}/{$UNIXNAME}/public_html">
	Options FollowSymLinks
	AllowOverride All
	Require expr {literal}%{HTTP_HOST}{/literal} != '127.0.0.1:{$PORT}'
	<LimitExcept GET POST HEAD>
		Require all denied
	</LimitExcept>
	<FilesMatch "\.(php|phar)$">
		<If "-f %{REQUEST_FILENAME}">
			SetHandler "proxy:unix:/run/php-fpm/{$UNIXNAME}.sock|fcgi://localhost"
		</If>
	</FilesMatch>
</Directory>

