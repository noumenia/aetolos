<?php
/**
 * Aetolos - PHP
 *
 * PHP support and implementation
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage php
 */

/**
 * Php class
 *
 * @package aetolos
 * @subpackage php
 */
class Php extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'php'			=> "enabled",
			'php|iniFile'		=> "/etc/php.ini"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		$rpm = array(
			"php-cli",
			"php-common",
			"php-intl",
			"php-mbstring",
			"php-mysqlnd",
			"php-pdo",
			"php-process",
			"php-soap",
			"php-xml"
		);

		if(PHP_MAJOR_VERSION <= 7)
			$rpm[] = "php-json";

		return $rpm;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: php");

		// Assign variables
		$this->smarty->assign("TIMEZONE", Config::read("timezone"));

		$rc = $this->updateIniFile(__DIR__ . "/php.tpl", Config::read("php|iniFile"), $this->smarty);
		if($rc === false)
			return false;

		// Migrate from the php module to the phpfpm module: opcache.tpl
		if(is_file(__DIR__ . "/templates.d/opcache.tpl")) {

			Log::warning("Found old php/templates.d/opcache.tpl file. This template has moved under the phpfpm module.");

			if(is_file(dirname(__DIR__) . "/phpfpm/templates.d/opcache.tpl"))
				Log::warning("The phpfpm/templates.d/opcache.tpl already exists! Please merge and remove the old opcache.tpl.");
			else
				rename(__DIR__ . "/templates.d/opcache.tpl", dirname(__DIR__) . "/phpfpm/templates.d/opcache.tpl");

		}

		// Migrate from the php module to the phpfpm module: override.tpl
		if(is_file(__DIR__ . "/templates.d/override.tpl")) {

			Log::warning("Found old php/templates.d/override.tpl file. This template has moved under the phpfpm module.");

			if(is_file(dirname(__DIR__) . "/phpfpm/templates.d/override.tpl"))
				Log::warning("The phpfpm/templates.d/override.tpl already exists! Please merge and remove the old override.tpl.");
			else
				rename(__DIR__ . "/templates.d/override.tpl", dirname(__DIR__) . "/phpfpm/templates.d/override.tpl");

		}

		// Migrate from the php module to the phpfpm module: virtualhostfpm.tpl
		if(is_file(__DIR__ . "/templates.d/virtualhostfpm.tpl")) {

			Log::warning("Found old php/templates.d/virtualhostfpm.tpl file. This template has moved under the phpfpm module.");

			if(is_file(dirname(__DIR__) . "/phpfpm/templates.d/virtualhostfpm.tpl"))
				Log::warning("The phpfpm/templates.d/virtualhostfpm.tpl already exists! Please merge and remove the old virtualhostfpm.tpl.");
			else
				rename(__DIR__ . "/templates.d/virtualhostfpm.tpl", dirname(__DIR__) . "/phpfpm/templates.d/virtualhostfpm.tpl");

		}

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::warning("Disable not implemented or not applicable for module: php");

		return false;

	}

}

