#!/usr/bin/env bash

# Aetolos - Automatically generated Dehydrated cron script

/root/dehydrated/dehydrated --cron --config {$CONFIG} --keep-going >/dev/null
/root/dehydrated/dehydrated --cleanup --config {$CONFIG} >/dev/null

