#!/usr/bin/env bash

# Aetolos - Automatically generated Dehydrated hook.sh script

deploy_challenge() {
    local DOMAIN="${1}" TOKEN_FILENAME="${2}" TOKEN_VALUE="${3}"
}

clean_challenge() {
    local DOMAIN="${1}" TOKEN_FILENAME="${2}" TOKEN_VALUE="${3}"
}

sync_cert() {
    local KEYFILE="${1}" CERTFILE="${2}" FULLCHAINFILE="${3}" CHAINFILE="${4}" REQUESTFILE="${5}"
}

deploy_cert() {
    local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}" TIMESTAMP="${6}"

    if [ -f "/etc/pki/tls/certs/${DOMAIN}.crt" ] ; then
        rm -rf "/etc/pki/tls/certs/${DOMAIN}.crt"
    fi
    if [ ! -L "/etc/pki/tls/certs/${DOMAIN}.crt" ] ; then
        ln -s "${CERTFILE}" "/etc/pki/tls/certs/${DOMAIN}.crt"
    fi

    if [ -f "/etc/pki/tls/certs/${DOMAIN}.chain" ] ; then
        rm -rf "/etc/pki/tls/certs/${DOMAIN}.chain"
    fi
    if [ ! -L "/etc/pki/tls/certs/${DOMAIN}.chain" ] ; then
        ln -s "${CHAINFILE}" "/etc/pki/tls/certs/${DOMAIN}.chain"
    fi

    if [ -f "/etc/pki/tls/certs/${DOMAIN}.fullchain" ] ; then
        rm -rf "/etc/pki/tls/certs/${DOMAIN}.fullchain"
    fi
    if [ ! -L "/etc/pki/tls/certs/${DOMAIN}.fullchain" ] ; then
        ln -s "${FULLCHAINFILE}" "/etc/pki/tls/certs/${DOMAIN}.fullchain"
    fi

    if [ -f "/etc/pki/tls/private/${DOMAIN}.key" ] ; then
        rm -rf "/etc/pki/tls/private/${DOMAIN}.key"
    fi
    if [ ! -L "/etc/pki/tls/private/${DOMAIN}.key" ] ; then
        ln -s "${KEYFILE}" "/etc/pki/tls/private/${DOMAIN}.key"
    fi

{if $HAPROXY==="enabled"}
    cat "${FULLCHAINFILE}" "${KEYFILE}" > "/etc/haproxy/cert/${DOMAIN}.pem"
    chmod go-wrx "/etc/haproxy/cert/${DOMAIN}.pem"
{/if}

    touch "${BASEDIR}/.reloadservices"

}

deploy_ocsp() {
    local DOMAIN="${1}" OCSPFILE="${2}" TIMESTAMP="${3}"
}

unchanged_cert() {
    local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}"
}

invalid_challenge() {
    local DOMAIN="${1}" RESPONSE="${2}"
}

request_failure() {
    local STATUSCODE="${1}" REASON="${2}" REQTYPE="${3}" HEADERS="${4}"
}

generate_csr() {
    local DOMAIN="${1}" CERTDIR="${2}" ALTNAMES="${3}"
}

startup_hook() {
  :
}

exit_hook() {
    local ERROR="${1:-}"
    local SERVERNAME="{$SERVERNAME}" BASEDIR="{$BASEDIR}"

    if [ -f "${BASEDIR}/certs/${SERVERNAME}/cert.pem" ] ; then

        if [ -f "/etc/pki/tls/certs/localhost.crt" ] ; then
            rm -rf "/etc/pki/tls/certs/localhost.crt"
        fi
        if [ ! -L "/etc/pki/tls/certs/localhost.crt" ] ; then
            ln -s "${BASEDIR}/certs/${SERVERNAME}/cert.pem" "/etc/pki/tls/certs/localhost.crt"
        fi

    fi

    if [ -f "${BASEDIR}/certs/${SERVERNAME}/chain.pem" ] ; then

        if [ -f "/etc/pki/tls/certs/localhost.chain" ] ; then
            rm -rf "/etc/pki/tls/certs/localhost.chain"
        fi
        if [ ! -L "/etc/pki/tls/certs/localhost.chain" ] ; then
            ln -s "${BASEDIR}/certs/${SERVERNAME}/chain.pem" "/etc/pki/tls/certs/localhost.chain"
        fi

    fi

    if [ -f "${BASEDIR}/certs/${SERVERNAME}/fullchain.pem" ] ; then

        if [ -f "/etc/pki/tls/certs/localhost.fullchain" ] ; then
            rm -rf "/etc/pki/tls/certs/localhost.fullchain"
        fi
        if [ ! -L "/etc/pki/tls/certs/localhost.fullchain" ] ; then
            ln -s "${BASEDIR}/certs/${SERVERNAME}/fullchain.pem" "/etc/pki/tls/certs/localhost.fullchain"
        fi

    fi

    if [ -f "${BASEDIR}/certs/${SERVERNAME}/privkey.pem" ] ; then

        if [ -f "/etc/pki/tls/private/localhost.key" ] ; then
            rm -rf "/etc/pki/tls/private/localhost.key"
        fi
        if [ ! -L "/etc/pki/tls/private/localhost.key" ] ; then
            ln -s "${BASEDIR}/certs/${SERVERNAME}/privkey.pem" "/etc/pki/tls/private/localhost.key"
        fi

    fi

    if [ -e "${BASEDIR}/.reloadservices" ] ; then

{if $POSTFIX==="enabled"}        /usr/bin/systemctl reload postfix{/if}
{if $DOVECOT==="enabled"}        /usr/bin/systemctl reload dovecot{/if}
{if $HAPROXY==="enabled"}        /usr/bin/systemctl reload haproxy{/if}

        rm -rf "${BASEDIR}/.reloadservices"

    fi

}

HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(deploy_challenge|clean_challenge|sync_cert|deploy_cert|deploy_ocsp|unchanged_cert|invalid_challenge|request_failure|generate_csr|startup_hook|exit_hook)$ ]]; then
  "$HANDLER" "$@"
fi

