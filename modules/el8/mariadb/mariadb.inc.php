<?php
/**
 * Aetolos - MariaDB
 *
 * MariaDB support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage mariadb
 */

/**
 * MariaDb class
 *
 * @package aetolos
 * @subpackage mariadb
 */
class MariaDb extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"cmdParameters",
			"help",
			"controller",
			"saveConfiguration",
			"export"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'mariadb'		=> "disabled",
			'mariadb|serverFile'	=> "/etc/my.cnf.d/mariadb-server.cnf",
			'mariadb|limitnoFile'	=> "/etc/systemd/system/mariadb.service.d/limitnofile.conf",
			'mariadb|memoryUsage'	=> "50"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"mariadb",
			"mariadb-server"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("mariadb"),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"memory-usage:",
			"virtualhost:",
			"add-dbuser:",
			"remove-dbuser:",
			"modify-dbuser:",
			"add-db:",
			"remove-db:",
			"list-users",
			"grant:",
			"password:",
			"password-file:"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=mariadb",
			array('short' => "",	'long' => "memory-usage=[PERCENT]",			'desc' => "Set the percentage of memory used by MariaDB (default is 50%)"),
			array('short' => "",	'long' => "virtualhost=[DOMAIN]",			'desc' => "Select the virtual host to modify with the following commands"),
			array('short' => "",	'long2' => "add-db=[DATABASE]",				'desc' => "Add new database, prefixed by the virtualhost"),
			array('short' => "",	'long2' => "remove-db=[DATABASE]",			'desc' => "Remove database"),
			"",
			array('short' => "",	'long2' => "add-dbuser=[USER][@localhost]",		'desc' => "Add new database user (default access from localhost only)"),
			array('short' => "",	'long3' => "grant=\"[PRIV...]@DB[@TABLE]\"",		'desc' => "Set privileges separated by comma and database/table separated by the @ symbol"),
			array('short' => "",	'long3' => "password=[PASSWORD]",			'desc' => "Set user password (plain text or HASH encoded)"),
			array('short' => "",	'long3' => "password-file=[FILE]",			'desc' => "Set user password from file (plain text or HASH encoded)"),
			"",
			array('short' => "",	'long2' => "remove-dbuser=[USER][@localhost]",		'desc' => "Remove a database user and delete related privilege data"),
			"",
			array('short' => "",	'long2' => "modify-dbuser=[USER][@localhost]",		'desc' => "Select the user to modify with the following commands"),
			array('short' => "",	'long3' => "grant=\"[PRIV...]@DB[@TABLE]\"",		'desc' => "Set privileges separated by comma and database/table separated by the @ symbol"),
			array('short' => "",	'long3' => "password=[PASSWORD]",			'desc' => "Set user password (plain text or HASH encoded)"),
			array('short' => "",	'long3' => "password-file=[FILE]",			'desc' => "Set user password from file (plain text or HASH encoded)"),
			"",
			array('short' => "",	'long2' => "list-users",				'desc' => "List all users for the selected virtual host"),
			""
		);

	}

	/**
	 * Module controller
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		if(isset($cmdParameters['memory-usage'])) {

			// Remove the % sign
			$cmdParameters['memory-usage'] = intval(str_replace("%", "", $cmdParameters['memory-usage']));

			// Input validation
			if(
				$cmdParameters['memory-usage'] < 10 ||
				$cmdParameters['memory-usage'] > 90
			) {

				Log::error("Invalid input, memory usage must be between 10% and 90%");
				return false;

			}

			// Set memory usage as a percentage value
			Config::write("mariadb|memoryUsage", $cmdParameters['memory-usage']);
			return true;

		}

		// Check for parameter
		if(!isset($cmdParameters['virtualhost'])) {

			Log::error("Missing parameter");
			return false;

		}

		$vhost = new VirtualHostManager();
		$vhost->domainName = $cmdParameters['virtualhost'];
		$rc = $vhost->get();
		if($rc === false) {

			Log::error("Unknown virtual host");
			return false;

		}

		$databaseManager = new DatabaseManager($vhost);

		if(isset($cmdParameters['add-db'])) {

			$rc = $databaseManager->addDatabase($cmdParameters['add-db']);
			if($rc === false) {

				Log::error("Error while adding database");
				return false;

			}

		} elseif(isset($cmdParameters['remove-db'])) {

			$rc = $databaseManager->removeDatabase($cmdParameters['remove-db']);
			if($rc === false) {

				Log::error("Error while removing database");
				return false;

			}

		} elseif(isset($cmdParameters['add-dbuser'])) {

			if(isset($cmdParameters['grant'])) {

				if(mb_strpos($cmdParameters['grant'], "@") === false) {

					Log::error("Invalid grant parameter, missing '@' symbol");
					return false;

				}

			}

			if(!isset($cmdParameters['password'])) {

				if(isset($cmdParameters['password-file'])) {

					if(!is_file($cmdParameters['password-file'])) {

						Log::error("Error while opening the password file");
						return false;

					}

					$cmdParameters['password'] = file_get_contents($cmdParameters['password-file']);
					if($cmdParameters['password'] === false) {

						Log::error("Error while reading the password file");
						return false;

					}

					// Remove newlines
					$cmdParameters['password'] = str_replace("\n", "", $cmdParameters['password']);

				} else {

					Log::error("Password not defined");
					return false;

				}

			}

			$rc = $databaseManager->addUser($cmdParameters['add-dbuser'], $cmdParameters['password']);
			if($rc === false) {

				Log::error("Error while creating new database user");
				return false;

			}

			if(
				isset($cmdParameters['grant']) &&
				is_string($cmdParameters['grant'])
			) {

				if(mb_strpos($cmdParameters['grant'], "@") === false) {

					Log::error("Invalid grant parameter, missing '@' symbol");
					return false;

				}

				$rc = explode("@", $cmdParameters['grant']);

				if(isset($rc[2]))
					$table = $rc[2];
				else
					$table = "*";

				$rc = $databaseManager->grant($cmdParameters['add-dbuser'], $rc[1], $rc[0], $table);
				if($rc === false) {

					Log::error("Error while granting privileges");
					return false;

				}

			}

		} elseif(isset($cmdParameters['remove-dbuser'])) {

			$rc = $databaseManager->removeUser($cmdParameters['remove-dbuser']);
			if($rc === false) {

				Log::error("Error while removing database user");
				return false;

			}

		} elseif(isset($cmdParameters['modify-dbuser'])) {

			if(
				isset($cmdParameters['grant']) &&
				is_string($cmdParameters['grant'])
			) {

				if(mb_strpos($cmdParameters['grant'], "@") === false) {

					Log::error("Invalid grant parameter, missing '@' symbol");
					return false;

				}

				$rc = explode("@", $cmdParameters['grant']);

				if(isset($rc[2]))
					$table = $rc[2];
				else
					$table = "*";

				$rc = $databaseManager->grant($cmdParameters['modify-dbuser'], $rc[1], $rc[0], $table);
				if($rc === false) {

					Log::error("Error while granting privileges");
					return false;

				}

			}

		} elseif(isset($cmdParameters['list-users'])) {

			$data = $databaseManager->listUsers();

			// JSON output
			if(Config::read("json") === true) {

				// Combine user+host
				foreach($data as &$u)
					$u = $u[0] . "@" . $u[1];

				if(Config::read("daemon") === true)
					// Return data to the daemon
					return $data;
				else
					// Generate a JSON encoded object
					echo json_encode(array('User@Host' => $data));

			} else {

				// Columns
				$columns = array(
					array(
						'header'	=> "User",
						'arrayKey'	=> 0,
						'maxSize'	=> 4
					),
					array(
						'header'	=> "Host",
						'arrayKey'	=> 1,
						'maxSize'	=> 4
					)
				);

				Ascii::drawTable($columns, $data);

			}

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: mariadb");

		// Required directory Mariadb service
		if(!is_dir("/etc/systemd/system/mariadb.service.d")) {

			// Create mariadb.service.d directory for systemd variables
			$rc = mkdir("/etc/systemd/system/mariadb.service.d", 0755);
			if($rc === false)
				return false;

			// Request systemd reload
			$this->daemonReload = true;

		}

		// Generate limitnofile.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/limitnofileconf.tpl", Config::read("mariadb|limitnoFile"), "", "", true);
		if($rc === false)
			return false;

		// Generate a dynamic config
		list($perThreadMemory, $serverBuffers) = MariadbHelper::generateDynamicConfig();

		// Assign variables
		$this->smarty->assignByRef("PERTHREADMEMORY", $perThreadMemory);
		$this->smarty->assignByRef("SERVERBUFFERS", $serverBuffers);

		// Generate main configuration (mariadb-server.cnf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mariadb.tpl", Config::read("mariadb|serverFile"));
		if($rc === false)
			return false;
		else
			return true;

	}

	/**
	 * Export virtual host
	 * @param VirtualHostManager $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array<string, string> $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array())
	{

		// Input validation
		if(empty($vhost->dbPrefix))
			return false;

		// Create database object
		$mdb = new DatabaseMariadb();

		// Open database connection
		$rc = $mdb->open();
		if($rc === false)
			return false;

		// Show databases based on the database prefix
		$rc = $mdb->query("SHOW DATABASES LIKE '" . $vhost->dbPrefix . "%';");
		if($rc === false)
			return false;

		// Array of databases
		$dbs = $mdb->getAssocArray();

		// Pop the array of arrays
		if(sizeof($dbs) > 0) {

			$dbs = array_pop($dbs);
			if($dbs === null)
				$dbs = array();
			elseif(is_string($dbs))
				$dbs = array($dbs);

		}

		// Check for parameter
		if(isset($cmdParameters['extradb']))
			$dbs = array_merge($dbs, explode(",", $cmdParameters['extradb']));

		if(sizeof($dbs) < 1)
			return true;

		Log::debug("Detected SQL databases: " . implode(", ", $dbs));

		// Backup GRANTS
		Log::debug("Generating SQL grants file");
		exec("/usr/bin/mysql --skip-column-names -A -e\"SELECT CONCAT('SHOW GRANTS FOR ''',user,'''@''',host,''';') " .
			"FROM mysql.user WHERE user<>'' AND user like " . escapeshellarg($vhost->dbPrefix . "%") .
			"\"  | mysql --skip-column-names -A | sed 's/$/;/g' > " . escapeshellarg($tdir . "mysql.sql"), $output, $rc);
		if($rc !== 0) {

			Log::error("Error while running mysql SHOW GRANTS for dbprefix");
			return false;

		}

		// Backup database schema & data
		Log::debug("Generating SQL database schema & data files");
		foreach($dbs as $d) {

			// Backup GRANTS
			exec("/usr/bin/mysql --skip-column-names -A -e\"SELECT CONCAT('SHOW GRANTS FOR ''',user,'''@''',host,''';') " .
				"FROM mysql.user WHERE user<>'' AND user like " . escapeshellarg($d) .
				"\"  | mysql --skip-column-names -A | sed 's/$/;/g' >> " . escapeshellarg($tdir . "mysql.sql"), $output, $rc);
			if($rc !== 0) {

				Log::error("Error while running mysql SHOW GRANTS");
				return false;

			}

			// Backup database schema
			exec("/usr/bin/mysqldump --no-data --no-create-info --databases " . escapeshellarg($d) . " > " . escapeshellarg($tdir . "mysql/" . $d . ".create"), $output, $rc);
			if($rc !== 0) {

				Log::error("Error while running mysqldump database schema");
				return false;

			}

			// Backup table data
			exec("/usr/bin/mysqldump --no-create-db " . escapeshellarg($d) . " > " . escapeshellarg($tdir . "mysql/" . $d . ".sql"), $output, $rc);
			if($rc !== 0) {

				Log::error("Error while running mysqldump table data");
				return false;

			}

		}

		return true;

	}

}

