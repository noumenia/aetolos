<?php
/**
 * Aetolos - SpamAssassin
 *
 * SpamAssassin support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage spamassassin
 */

/**
 * SpamAssassin class
 *
 * @package aetolos
 * @subpackage spamassassin
 */
class SpamAssassin extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"selinux",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'spamassassin'			=> "disabled",
			'spamassassin|initPreFile'	=> "/etc/mail/spamassassin/init.pre",
			'spamassassin|milterFile'	=> "/etc/sysconfig/spamass-milter",
			'spamassassin|localCfFile'	=> "/etc/mail/spamassassin/local.cf",
			'spamassassin|selinuxModule'	=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"spamassassin",
			"spamass-milter",
			"spamass-milter-postfix"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("postfix"),
			'repository'	=> array(),
			'service'	=> array(
				"spamassassin",
				"spamass-milter",
				"sa-update.timer"
			),
			'conflict'	=> array()
		);

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux - allow SpamAssassin to read /etc/resolv.conf for DNS queries
		if(Config::read("spamassassin|selinuxModule") !== "installed") {

			Log::debug("SELinux: enable spamd_resolv_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/spamd_resolv_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("spamassassin|selinuxModule", "installed");

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: spamassassin");

		// Assign variables
		$this->smarty->assign("SERVERNAMEREGEX", str_replace(".", "\.", php_uname("n")));
		$this->smarty->assign("OPENDMARC", Config::read("opendmarc"));
		$this->smarty->assign("OPENDKIM", Config::read("opendkim"));

		// Enable SA SPF module if there is no SPF in postfix
		if(
			is_file("/usr/local/lib/policyd-spf-perl/postfix-policyd-spf-perl") ||
			is_file("/usr/libexec/postfix/policyd-spf")
		)
			$this->smarty->assign("SPFSA", false);
		else
			$this->smarty->assign("SPFSA", true);

		// SpamAssassin daemon (spamd)

		// Init file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/spamdinitpre.tpl", Config::read("spamassassin|initPreFile"));
		if($rc === false)
			return false;

		// Milter sysconfig file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/spamassmilter.tpl", Config::read("spamassassin|milterFile"));
		if($rc === false)
			return false;

		// Local file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/spamdlocalcf.tpl", Config::read("spamassassin|localCfFile"));
		if($rc === false)
			return false;
		else
			return true;

	}

}

