<?php
/**
 * Aetolos - HAproxy balancer
 *
 * HAproxy support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage haproxy
 */

/**
 * HAproxy class
 *
 * @package aetolos
 * @subpackage haproxy
 */
class HAproxy extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"cmdParameters",
			"help",
			"controller",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'haproxy'			=> "disabled",
			'haproxy|conf'			=> "/etc/haproxy/haproxy.cfg",
			'haproxy|sysctlConf'		=> "/etc/sysctl.d/90-haproxy.conf",
			'haproxy|type'			=> "web",
			'haproxy|maxConn'		=> "600",
			'haproxy|acme'			=> "",
			'haproxy|stats'			=> "",
			'haproxy|statsPassword'		=> hash("sha256", openssl_random_pseudo_bytes(16))
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"haproxy"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("haproxy"),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"haproxy-type:",
			"add-server:",
			"remove-server:",
			"acme-server:",
			"stats-listen:"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=haproxy",
			array('short' => "",	'long' => "haproxy-type=[web|db]",		'desc' => "Set to \"web\" or \"db\" to change the haproxy service type"),
			array('short' => "",	'long' => "add-server=[SERVER]",		'desc' => "Add a new server"),
			array('short' => "",	'long' => "remove-server=[SERVER]",		'desc' => "Remove an existing server"),
			array('short' => "",	'long' => "acme-server=[SERVER]",		'desc' => "Set a single server to redirect ACME requests"),
			"  SERVER = <ipaddress>[:<port>] [<haproxy server parameters>], for example: 192.168.10.10:80 check maxconn 200",
			"",
			array('short' => "",	'long' => "stats-listen=[IP:PORT]",		'desc' => "Set an IP address and port to enable the haproxy web stats"),
			""
		);

	}

	/**
	 * Module controller
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		// Create HAproxy manager object
		$haproxy = new HaproxyManager();

		if(isset($cmdParameters['haproxy-type']) && ($cmdParameters['haproxy-type'] === "web" || $cmdParameters['haproxy-type'] === "db")) {

			// Set configuration for HAproxy type
			Config::write("haproxy|type", $cmdParameters['haproxy-type']);

		} elseif(isset($cmdParameters['add-server'])) {

			// Add HAproxy server to database
			$rc = $haproxy->add($cmdParameters['add-server']);
			if($rc === false) {

				Log::error("Error while adding HAproxy server: " . $cmdParameters['add-server']);
				return false;

			}

		} elseif(isset($cmdParameters['remove-server'])) {

			// Delete HAproxy server from database
			$rc = $haproxy->remove($cmdParameters['remove-server']);
			if($rc === false) {

				Log::error("Error while removing HAproxy server: " . $cmdParameters['remove-server']);
				return false;

			}

		} elseif(isset($cmdParameters['acme-server'])) {

			// Set configuration for ACME server
			Config::write("haproxy|acme", $cmdParameters['acme-server']);

		} elseif(isset($cmdParameters['stats-listen'])) {

			// Set configuration for stats IP:PORT
			Config::write("haproxy|stats", $cmdParameters['stats-listen']);

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: haproxy");

		// Required cert directory
		if(!is_dir("/etc/haproxy/cert/")) {

			// Create cert directory for certificate storage
			$rc = mkdir("/etc/haproxy/cert/", 0700);
			if($rc === false)
				return false;

		}

		// Assign variables
		$this->smarty->assign("MAXCONN", Config::read("haproxy|maxConn"));
		$this->smarty->assign("ACME", Config::read("haproxy|acme"));
		$this->smarty->assign("STATS", Config::read("haproxy|stats"));
		$this->smarty->assign("STATSPASSWORD", Config::read("haproxy|statsPassword"));

		// Get all HAproxy backend servers
		$servers = array();
		DbFactory::$db->query("SELECT Server, Parameters FROM `haproxyServer`");
		while(DbFactory::$db->nextRow())
			$servers[] = DbFactory::$db->row[0] . " " . DbFactory::$db->row[1];
		$this->smarty->assignByRef("SERVERS", $servers);

		// Use haproxy as a web or database balancer
		if(Config::read("haproxy|type") === "web") {

			// Generate web configuration (haproxy.cfg)
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/haproxywebconf.tpl", Config::read("haproxy|conf"));
			if($rc === false)
				return false;

		} else {

			// Generate db configuration (haproxy.cfg)
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/haproxydbconf.tpl", Config::read("haproxy|conf"));
			if($rc === false)
				return false;

		}

		// Get the parent directory (by default: /etc/sysctl.d)
		$parentDir = dirname(Config::read("haproxy|sysctlConf"));
		if(!is_dir($parentDir)) {

			// Create it if it does not exist
			$rc = mkdir($parentDir, 0755);
			if($rc === false)
				return false;

		}

		// Generate sysctl configuration (90-haproxy.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/haproxysysctlconf.tpl", Config::read("haproxy|sysctlConf"));
		if($rc === false)
			return false;
		else
			return true;

	}

}

