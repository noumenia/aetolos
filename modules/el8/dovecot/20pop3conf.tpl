# Aetolos - Automatically generated Dovecot configuration

pop3_uidl_format = UID%u-%v
pop3_logout_format = top=%t/%p, retr=%r/%b, del=%d/%m, size=%s, bytes=%i/%o
protocol pop3 {
	mail_plugins = $mail_plugins quota
	mail_max_userip_connections = 50
}

