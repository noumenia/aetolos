<?php
/**
 * Aetolos - PHP-FPM
 *
 * PHP-FPM support and implementation
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage phpfpm
 */

/**
 * Phpfpm class
 *
 * @package aetolos
 * @subpackage php
 */
class Phpfpm extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'phpfpm'		=> "disabled",
			'phpfpm|directoryFpmD'	=> "/etc/php-fpm.d",
			'phpfpm|opcacheFile'	=> "/etc/php.d/10-opcache.ini",
			'phpfpm|override'	=> "/etc/systemd/system/php-fpm.service.d/override.conf"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"php-fpm",
			"php-opcache"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("apache"),
			'repository'	=> array(),
			'service'	=> array("php-fpm"),
			'conflict'	=> array()
		);

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Generate virtual hosts
		foreach($vhosts as &$v) {

			// Skip parked domains
			if(!empty($v->parkedUnder))
				continue;

			// Assign variables
			$this->smarty->assignByRef("SERVERNAME", $v->domainName);
			$this->smarty->assignByRef("UNIXNAME", $v->unixName);
			$this->smarty->assignByRef("HOME", $v->home);

			// Save configuration file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/virtualhostfpm.tpl", Config::read("phpfpm|directoryFpmD") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: php-fpm");

		// Required directory for php-fpm systemd directory
		if(!is_dir("/etc/systemd/system/php-fpm.service.d")) {

			// Create php-fpm.service.d directory for systemd variables
			$rc = mkdir("/etc/systemd/system/php-fpm.service.d", 0755);
			if($rc === false)
				return false;

			// Request systemd reload
			$this->daemonReload = true;

		}

		// Generate override.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/override.tpl", Config::read("phpfpm|override"), "", "", true);
		if($rc === false)
			return false;

		// Disable default www pool
		$file = Config::read("phpfpm|directoryFpmD") . "/www.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Assign variables
		$this->smarty->assign("TIMEZONE", Config::read("timezone"));

		$rc = $this->updateIniFile(__DIR__ . "/opcache.tpl", Config::read("phpfpm|opcacheFile"), $this->smarty);
		if($rc === false)
			return false;

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		return true;

	}

}

