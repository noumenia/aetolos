# Aetolos - Automatically generated Postfix configuration

queue_directory = /var/spool/postfix
command_directory = /usr/sbin
compatibility_level = 2
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
inet_interfaces = all
inet_protocols = all
smtp_address_preference = ipv4
mydestination = $myhostname, localhost.$mydomain, localhost
unknown_local_recipient_reject_code = 550
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
transport_maps = hash:/etc/postfix/transport
smtpd_banner = $myhostname ESMTP
debug_peer_level = 2
debugger_command =
	 PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
	 ddd $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
message_size_limit = 41943040
manpage_directory = /usr/share/man
meta_directory = /etc/postfix
shlib_directory = /usr/lib64/postfix
sample_directory = /usr/share/doc/postfix/samples
readme_directory = /usr/share/doc/postfix/README_FILES
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_auth_enable = no
smtpd_tls_auth_only = yes
smtpd_tls_security_level = may
smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3
smtpd_tls_protocols = !SSLv2, !SSLv3
smtpd_tls_ciphers = medium
smtpd_tls_mandatory_ciphers = medium
smtpd_tls_session_cache_database = btree:{literal}${data_directory}{/literal}/smtpd_session_cache
smtpd_tls_CApath = /etc/pki/tls/certs
smtp_tls_security_level = may
smtp_tls_mandatory_protocols = !SSLv2, !SSLv3
smtp_tls_protocols = !SSLv2, !SSLv3
smtp_tls_ciphers = medium
smtp_tls_mandatory_ciphers = medium
smtp_tls_session_cache_database = btree:{literal}${data_directory}{/literal}/smtp_session_cache
smtp_tls_CApath = /etc/pki/tls/certs
smtp_tls_CAfile = /etc/pki/tls/certs/ca-bundle.crt
{if $DEHYDRATED==="enabled"}smtpd_tls_cert_file = /etc/pki/tls/certs/localhost.fullchain
{else}smtpd_tls_cert_file = /etc/pki/tls/certs/localhost.crt
{/if}
smtpd_tls_key_file = /etc/pki/tls/private/localhost.key
smtpd_tls_CApath = /etc/pki/tls/certs
smtpd_tls_CAfile = /etc/pki/tls/certs/ca-bundle.crt
smtpd_tls_received_header = yes
default_rbl_reply = $rbl_code Service unavailable
unverified_sender_reject_reason = Service unavailable
unverified_recipient_reject_reason = Service unavailable
unverified_recipient_reject_code = 550
unverified_sender_reject_code = 550
unknown_local_recipient_reject_code = 550
mynetworks_style = host
smtpd_helo_required = yes
smtpd_soft_error_limit = 2
smtpd_client_connection_count_limit = 10
smtpd_client_connection_rate_limit = 60
smtpd_client_new_tls_session_rate_limit = 60
smtpd_client_recipient_rate_limit = 300
smtpd_client_auth_rate_limit = 15
smtpd_recipient_limit = 150
smtpd_recipient_overshoot_limit = 150
smtpd_reject_unlisted_sender = yes
maximal_backoff_time = 30m
minimal_backoff_time = 5m
queue_run_delay = 5m
maximal_queue_lifetime = 1h
bounce_queue_lifetime = 1h
address_verify_sender_ttl = 1h
strict_rfc821_envelopes = yes
disable_vrfy_command = yes
biff = no
always_add_missing_headers = yes
smtp_header_checks = regexp:/etc/postfix/smtp_header_checks
mime_header_checks = regexp:/etc/postfix/mime_header_checks
milter_mail_macros = {literal}i {auth_author} {auth_type} {auth_authen}{/literal}
milter_connect_macros = {literal}b j _ {daemon_name} {if_name} {if_addr}{/literal}
smtpd_milters = {if isset($OPENDKIM) && $OPENDKIM=='enabled'}inet:127.0.0.1:8891 {/if}{if isset($OPENDMARC) && $OPENDMARC=='enabled'}inet:127.0.0.1:8893 {/if}{if isset($CLAMAV) && $CLAMAV=='enabled'}inet:127.0.0.1:8890 {/if}{if isset($SPAMASSASSIN) && $SPAMASSASSIN=='enabled'}unix:/run/spamass-milter/postfix/sock{/if} 
{if isset($OPENDKIM) && $OPENDKIM=='enabled'}non_smtpd_milters = inet:127.0.0.1:8891
internal_mail_filter_classes = bounce
disable_mime_output_conversion = yes
{/if}

{if isset($SPFPERL) && $SPFPERL==true}
policy_time_limit = 3600
{/if}

# postscreen checks
postscreen_blacklist_action = drop
postscreen_cache_cleanup_interval = 24h
postscreen_cache_retention_time = 30d
postscreen_command_time_limit = 10s
postscreen_greet_action = enforce
postscreen_greet_wait = 15s

# postscreen combined white/black list in cidr format
postscreen_access_list = permit_mynetworks, cidr:/etc/postfix/postscreen_access.cidr

# postscreen deep tests
#postscreen_bare_newline_action = enforce
#postscreen_bare_newline_enable = yes
#postscreen_bare_newline_ttl = 60d
#postscreen_pipelining_action = enforce
#postscreen_pipelining_enable = yes
#postscreen_pipelining_ttl = 60d
#postscreen_non_smtp_command_action = drop
#postscreen_non_smtp_command_enable = yes
#postscreen_non_smtp_command_ttl = 60d

postscreen_dnsbl_action = enforce
postscreen_dnsbl_threshold = 3
postscreen_dnsbl_sites =
	zen.spamhaus.org*3
	bl.spamcop.net*3
	dnsbl-1.uceprotect.net*3
	hostkarma.junkemailfilter.com=127.0.0.2*3
	dnsbl-2.uceprotect.net*2
	bl.mailspike.net*1
	hostkarma.junkemailfilter.com=127.0.0.3*1

# step 1 - IP and hostname checks
smtpd_client_restrictions =
	reject_unknown_reverse_client_hostname
	permit_mynetworks
	reject_rhsbl_reverse_client dbl.spamhaus.org

# step 2 - helo/ehlo checks
smtpd_helo_restrictions =
	permit_mynetworks
	check_helo_access hash:/etc/postfix/helo_access
	reject_invalid_helo_hostname
	reject_non_fqdn_helo_hostname
	#reject_unknown_helo_hostname
	reject_rhsbl_helo dbl.spamhaus.org

# step 3 - mail from checks
smtpd_sender_restrictions =
	reject_non_fqdn_sender
	reject_unknown_sender_domain
	permit_mynetworks
	check_sender_access regexp:/etc/postfix/sender_domain_checks
	reject_rhsbl_sender dbl.spamhaus.org

# step 4 - rcpt checks
smtpd_recipient_restrictions =
	reject_non_fqdn_recipient
	permit_mynetworks
	reject_unauth_destination
	reject_unknown_recipient_domain
	reject_unverified_recipient
{if isset($POSTGREY) && $POSTGREY=='enabled'}	check_policy_service unix:/var/spool/postfix/postgrey/socket
{/if}
{if (isset($SPFPERL) && $SPFPERL==true) || (isset($SPFPYTH) && $SPFPYTH==true)}	check_policy_service unix:private/policyspf
{/if}

# step 5 - rcpt to extenal/foreign addresses
smtpd_relay_restrictions =
	defer_unauth_destination

# step 6
smtpd_data_restrictions =
	sleep 1
	reject_unauth_pipelining

# step 7 - end
smtpd_end_of_data_restrictions = 

# virtual hosts
virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_mailbox_domains = /etc/postfix/virtual_mailbox_domains
virtual_alias_maps = {if isset($VHOSTS)}{foreach $VHOSTS as $V}hash:{$V[6]}/{$V[3]}/etc/{$V[2]}/forwarders {/foreach}{/if}

