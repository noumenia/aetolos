# Aetolos - Automatically generated Dovecot configuration

passdb {
	driver = passwd-file
	args = scheme=CRYPT username_format=%n /etc/dovecot/vhost/%d/shadow
}
userdb {
	driver = passwd-file
	args = username_format=%n /etc/dovecot/vhost/%d/passwd
}

