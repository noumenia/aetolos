# Aetolos - Automatically generated Dovecot configuration

ssl = required
{if $DEHYDRATED==="enabled"}ssl_cert = </etc/pki/tls/certs/localhost.fullchain
{else}ssl_cert = </etc/pki/tls/certs/localhost.crt
{/if}
ssl_key = </etc/pki/tls/private/localhost.key
{if isset($DHPEM) && $DHPEM==true}ssl_dh = </etc/dovecot/dh.pem
{/if}
ssl_min_protocol = TLSv1.2
ssl_cipher_list = ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
ssl_prefer_server_ciphers = yes
{if isset($VHOSTS)}{foreach $VHOSTS as $V}
local_name {$V->domainName} {
{if $V->parentDomain!=''}{assign var=dom value=$V->parentDomain}{else}{assign var=dom value=$V->domainName}{/if}
	{if $DEHYDRATED==="enabled"}ssl_cert = </etc/pki/tls/certs/{$dom}.fullchain
{else}ssl_cert = </etc/pki/tls/certs/{$dom}.crt
{/if}
	ssl_key = </etc/pki/tls/private/{$dom}.key
	{if isset($V->caBundle) && $V->caBundle==true}ssl_ca = </etc/pki/tls/certs/{$dom}.cabundle{/if}
}
{/foreach}{/if}

