<?php
/**
 * Aetolos - NSD
 *
 * NSD support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage nsd
 */

/**
 * NSD class
 *
 * @package aetolos
 * @subpackage nsd
 */
class Nsd extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'nsd'			=> "disabled",
			'nsd|nsdConfFile'	=> "/etc/nsd/nsd.conf",
			'nsd|directoryConfD'	=> "/etc/nsd/conf.d",
			'nsd|selinuxModule'	=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array("nsd");

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("nsd"),
			'conflict'	=> array()
		);

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux - allow NSD to write to log and temporary directories/files
		if(Config::read("nsd|selinuxModule") !== "installed") {

			Log::debug("SELinux: enable nsd_etc_nsd_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/nsd_etc_nsd_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("nsd|selinuxModule", "installed");

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// OpenDKIM support
		if(Config::read("opendkim", true) === "enabled") {

			// Default public key file
			$signatureFile = "/etc/opendkim/keys/default.txt";
			if(!is_file($signatureFile)) {
				Log::error("OpenDKIM public key file not found: " . $signatureFile);
				return false;
			}

			// Get public key signature
			$signature = file_get_contents($signatureFile);
			if($signature === false) {
				Log::error("Error while reading the OpenDKIM public key file: " . $signatureFile);
				return false;
			}

			// Filter input
			$signature = str_replace(array("\n", "\t", "\""), "", $signature);

			// Extract the TXT part of the signature
			$rc = preg_match('/\((.*)\)/U', $signature, $matches);
			if($rc !== 1 || !isset($matches[1])) {
				Log::error("Error while extracting the signature from the OpenDKIM public key file: " . $signatureFile);
				return false;
			}

			// Remove extra white space within the string and trim
			$signature = preg_replace('/\s+/', " ", $matches[1]);
			if($signature !== null)
				$signature = trim($signature);

			// Assign variable
			$this->smarty->assignByRef("OPENDKIM", $signature);

		} else {

			$this->smarty->assign("OPENDKIM", "");

		}

		// Get all virtual hosts and generate zones
		$vhosts = VirtualHostFactory::populate();

		// Loop
		foreach($vhosts as &$v) {

			// Assign variables
			$this->smarty->assignByRef("DOMAINNAME", $v->domainName);
			$this->smarty->assignByRef("IPADDRESS", $v->ipAddress);
			$this->smarty->assign("SERIAL", date("Ymd") . $v->domainZoneVersion);

			$this->smarty->assignByRef("NAMESERVERS", $v->ns);
			$this->smarty->assignByRef("MAILEXCHANGE", $v->mx);
			$this->smarty->assign("WEIGHT", 10);
			$this->smarty->assign("MTASTS", Config::read("mtasts"));

			// Generate zone configuration file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/nsdzoneconf.tpl", Config::read("nsd|directoryConfD") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

			// Generate zone file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/nsdzone.tpl", Config::read("nsd|directoryConfD") . "/" . $v->domainName . ".zone");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: nsd");

		// Detect CPU count
		$cpucount = file_get_contents("/proc/cpuinfo");
		if($cpucount === false) {
			Log::error("Error while detecting CPU count via /proc/cpuinfo");
			return false;
		}
		$cpucount = preg_match_all('/^processor/m', $cpucount);
		if($cpucount === false || !is_numeric($cpucount) || $cpucount < 1)
			$cpucount = 1;

		// Assign variables
		$this->smarty->assignByRef("CPUCOUNT", $cpucount);

		// NSD daemon (nsd) configuration
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/nsdconf.tpl", Config::read("nsd|nsdConfFile"));
		if($rc === false)
			return false;

		// Generate conf+zone combo files for virtual hosts
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;
		else
			return true;

	}

}

