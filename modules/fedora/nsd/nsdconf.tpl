# Aetolos - Automatically generated NSD configuration

server:
	server-count: {$CPUCOUNT}
	ip-address: 0.0.0.0
	ip-transparent: yes
	do-ip4: yes
	do-ip6: no
	port: 53
	hide-version: yes
	identity: "server"
	tcp-count: 50
	tcp-query-count: 25
	tcp-timeout: 5
	round-robin: yes
	zonefiles-write: 3600
	include: "/etc/nsd/server.d/*.conf"

include: "/etc/nsd/conf.d/*.conf"

remote-control:
	control-enable: no
	control-interface: 127.0.0.1

