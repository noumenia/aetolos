#!/usr/bin/env bash

# Aetolos - Automatically generated Public Suffix List cron script

curl								\
	--silent						\
	--show-error						\
	-z "/usr/share/publicsuffix/public_suffix_list.dat"	\
	"https://publicsuffix.org/list/public_suffix_list.dat"	\
	-o "/usr/share/publicsuffix/public_suffix_list.dat"

