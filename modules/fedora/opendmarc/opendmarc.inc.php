<?php
/**
 * Aetolos - OpenDMARC
 *
 * OpenDMARC support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage opendmarc
 */

/**
 * OpenDMARC class
 *
 * @package aetolos
 * @subpackage opendmarc
 */
class OpenDMARC extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'opendmarc'			=> "disabled",
			'opendmarc|opendmarcConfFile'	=> "/etc/opendmarc.conf"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array("opendmarc");

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("postfix"),
			'repository'	=> array(),
			'service'	=> array("opendmarc"),
			'conflict'	=> array()
		);

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: opendmarc");

		// Required publicsuffix directory
		if(!is_dir("/usr/share/publicsuffix")) {

			// Create publicsuffix directory
			$rc = mkdir("/usr/share/publicsuffix", 0755);
			if($rc === false)
				return false;

		}

		if(!is_file("/usr/share/publicsuffix/public_suffix_list.dat")) {

			Log::debug("Downloading latest Public Suffix List");

			// Set CURL options
			$options = array(
				CURLOPT_URL		=> "https://publicsuffix.org/list/public_suffix_list.dat",
				CURLOPT_CONNECTTIMEOUT	=> 10,
				CURLOPT_TIMEOUT		=> 10,
				CURLOPT_RETURNTRANSFER	=> true
			);

			// Use the package manager proxy, if one is set
			if(!empty(Config::read("proxy")))
				$options[CURLOPT_PROXY] = Config::read("proxy");

			// Download latest Public Suffix List
			$curl = curl_init();
			curl_setopt_array($curl, $options);
			$rc = curl_exec($curl);
			curl_close($curl);
			if($rc === false) {

				Log::error("Error while downloading the Public Suffix List");
				return false;

			}

			Log::debug("Installing the Public Suffix List under: /usr/share/publicsuffix/");

			// Save to file
			$rc = file_put_contents("/usr/share/publicsuffix/public_suffix_list.dat", $rc);
			if($rc === false) {

				Log::error("Error while saving to file /usr/share/publicsuffix/public_suffix_list.dat");
				return false;

			}

			// Set permissions
			$rc = chmod("/usr/share/publicsuffix/public_suffix_list.dat", 0644);
			if($rc === false) {

				Log::error("Error while setting permissions to file /usr/share/publicsuffix/public_suffix_list.dat");
				return false;

			}

		}

		if(Config::read("opendmarc") === "enabled") {

			$rc = $this->generateCron();
			if($rc === false)
				return false;

		} else {

			$rc = $this->removeCron();
			if($rc === false)
				return false;

		}

		// OpenDMARC config file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/opendmarcconffile.tpl", Config::read("opendmarc|opendmarcConfFile"));
		if($rc === false)
			return false;
		else
			return true;

	}

	/**
	 * Secondary functionality to enable this module
	 * @return bool
	 */
	private function enableSecondary()
	{

		return $this->generateCron();

	}

	/**
	 * Secondary functionality to disable this module
	 * @return bool
	 */
	private function disableSecondary()
	{

		return $this->removeCron();

	}

	/**
	 * Generate cron job
	 * @return bool
	 */
	private function generateCron()
	{

		// In case cronie is not installed
		if(!is_dir("/etc/cron.weekly"))
			mkdir("/etc/cron.weekly");

		// Generate weekly cron script (publicsuffix.cron)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/publicsuffixcron.tpl", "/etc/cron.weekly/publicsuffix.cron");
		if($rc === false)
			return false;

		// Set permissions
		$rc = chmod("/etc/cron.weekly/publicsuffix.cron", 0700);
		if($rc === false) {

			Log::warning("Could not set 0700 permissions to: /etc/cron.weekly/publicsuffix.cron");
			return false;
		}

		return true;

	}

	/**
	 * Remove cron job
	 * @return bool
	 */
	private function removeCron()
	{

		// Delete cron script
		if(is_file("/etc/cron.weekly/publicsuffix.cron"))
			unlink("/etc/cron.weekly/publicsuffix.cron");

		return true;

	}

}

