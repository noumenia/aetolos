<?php
/**
 * Aetolos - OpenDKIM
 *
 * OpenDKIM support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage opendkim
 */

/**
 * OpenDKIM class
 *
 * @package aetolos
 * @subpackage opendkim
 */
class OpenDKIM extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'opendkim'			=> "disabled",
			'opendkim|opendkimConfFile'	=> "/etc/opendkim.conf",
			'opendkim|opendkimSysFile'	=> "/etc/sysconfig/opendkim"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"opendkim",
			"opendkim-tools"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("postfix"),
			'repository'	=> array(),
			'service'	=> array("opendkim"),
			'conflict'	=> array()
		);

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: opendkim");

		// Check for default key
		if(
			!is_file("/etc/opendkim/keys/default.private") ||
			!is_file("/etc/opendkim/keys/default.txt") ||
			filesize("/etc/opendkim/keys/default.private") === 0
		) {

			// Generate key
			exec("/usr/sbin/opendkim-default-keygen");

			// Re-check
			if(
				!is_file("/etc/opendkim/keys/default.private") ||
				!is_file("/etc/opendkim/keys/default.txt") ||
				filesize("/etc/opendkim/keys/default.private") === 0
			) {
				Log::error("Error while creating OpenDKIM keys via /usr/sbin/opendkim-default-keygen, ensure the /etc/hosts file contains the IP and FQDM of the server");
				return false;
			}

		}

		// OpenDKIM sysconfig file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/sysconfigopendkim.tpl", Config::read("opendkim|opendkimSysFile"));
		if($rc === false)
			return false;

		// OpenDKIM config file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/opendkimconffile.tpl", Config::read("opendkim|opendkimConfFile"));
		if($rc === false)
			return false;
		else
			return true;

	}

}

