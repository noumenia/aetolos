# Aetolos - Automatically generated Logrotate configuration

/var/log/httpd/access_log /var/log/httpd/error_log {
    missingok
    notifempty
    sharedscripts
    delaycompress
    postrotate
        /bin/systemctl reload httpd.service > /dev/null 2>/dev/null || true
    endscript
}
{if isset($VHOSTS) && is_array($VHOSTS) && sizeof($VHOSTS)>0}{foreach $VHOSTS as $V}{if $V->parkedUnder!=''}{continue}{/if}
/var/log/httpd/{$V->domainName}*log {
    missingok
    notifempty
    sharedscripts
    delaycompress
    postrotate
        /bin/systemctl reload httpd@{$V->domainName} > /dev/null 2>/dev/null || true
    endscript
}
{/foreach}{/if}

