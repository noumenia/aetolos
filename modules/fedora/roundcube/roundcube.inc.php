<?php
/**
 * Aetolos - Roundcube webmail
 *
 * Roundcube support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage roundcube
 */

/**
 * Roundcube class
 *
 * @package aetolos
 * @subpackage roundcube
 */
class Roundcube extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @TODO move hash() functions to controller
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'roundcube'			=> "disabled",
			'roundcube|dbPassword'		=> hash("sha256", openssl_random_pseudo_bytes(16)),
			'roundcube|desKey'		=> mb_substr(hash("sha256", openssl_random_pseudo_bytes(16)), rand(0, 40), 24),
			'roundcube|configFile'		=> "/etc/roundcubemail/config.inc.php",
			'roundcube|roundcubeDirectory'	=> "/etc/roundcubemail"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"roundcubemail",
			"php-pear-Auth-SASL",
			"php-pear-Mail-Mime",
			"php-pear-Net-IDNA2",
			"php-pear-Net-SMTP",
			"php-pear-Net-Sieve",
			"php-pear-Net-Socket"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(
				"phpfpm",
				"apache",
				"postfix",
				"dovecot",
				"mariadb"
			),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array()
		);

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Loop (generate virtual hosts)
		foreach($vhosts as &$v) {

			// Assign variables
			$this->smarty->assign("DOMAIN", $v->domainName);

			// Domain file
			$domainFile = Config::read("roundcube|roundcubeDirectory") . "/" . $v->domainName . ".inc.php";

			// Generate domain file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/domainincphp.tpl", $domainFile, "<" . "?php ");
			if($rc === false)
				return false;

			// Restrict permissions and ownership
			chmod($domainFile, 0640);
			chgrp($domainFile, "apache");

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: roundcube");

		// Save virtual hosts
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		// If MariaDB is enabled
		if(Config::read("mariadb") === "enabled") {

			// Make sure MariaDB is running
			if(Config::$modules['mariadb']->status() !== "running") {

				// Start MariaDB service
				Config::$modules['mariadb']->start();

				// Re-check MariaDB
				if(Config::$modules['mariadb']->status() !== "running") {

					Log::error("Could not start MariaDB, which is required to setup roundcube");
					return false;

				}

			}

			// Create database object
			$mdb = new DatabaseMariadb();

			// Open database connection
			$rc = $mdb->open();
			if($rc === false)
				return false;

			// Detect if the Roundcube table ('roundcubemail') has already been created and privileges already set
			$mdb->query("SHOW DATABASES LIKE 'roundcubemail'");
			$mdb->nextRow();
			if($mdb->row === false) {

				// Create database
				$mdb->query("CREATE DATABASE roundcubemail CHARACTER SET = 'utf8' COLLATE = 'utf8_unicode_ci'");

				// Set privileges
				$mdb->query("GRANT ALL PRIVILEGES ON roundcubemail.* TO roundcubemail@'localhost' IDENTIFIED BY '" . Config::read("roundcube|dbPassword") . "'");
				$mdb->query("FLUSH PRIVILEGES");

				// Select database
				$mdb->query("USE roundcubemail");

				// Load initial SQL database scheme
				$sql = file_get_contents("/usr/share/roundcubemail/SQL/mysql.initial.sql");
				if($sql === false) {

					Log::error("Could not load the SQL scheme /usr/share/roundcubemail/SQL/mysql.initial.sql");
					return false;

				}

				// Remove comments
				$sql = preg_replace(array('/-- .*$/m', '/^\/\*.*$/m', '/\/\*(.*)\*\//u'), "", $sql);
				if($sql === null) {

					Log::error("Could not parse SQL patterns");
					return false;

				}

				// Convert to a single line of multiple statements
				$sql = preg_replace(array('/,\n/', '/\n\)/m', '/\)\n/', '/\(\n/'), array(", ", ") ", ") ", "( "), $sql);
				if($sql === null) {

					Log::error("Could not convert SQL patterns to a single line");
					return false;

				}

				// Replace newlines
				$sql = str_replace("\n", " ", $sql);

				// Execute
				$mdb->conn->exec($sql);

			} else {

				// Select table
				$mdb->query("USE roundcubemail");

				// Reset password of roundcubemail user, in case this is a re-run of the setup, this avoids stale passwords
				$mdb->query("SET PASSWORD FOR roundcubemail@'localhost' = PASSWORD('" . Config::read("roundcube|dbPassword") . "')");

			}

		}

		// Assign variables
		$this->smarty->assign("USER", "roundcubemail");
		$this->smarty->assign("PASSWORD", Config::read("roundcube|dbPassword"));
		$this->smarty->assign("DESKEY", Config::read("roundcube|desKey"));

		// Get the major version number
		$rVersion = floatval(substr(strval(shell_exec("rpm -q roundcubemail")), 14, 3));

		// Use the old config values for roundcube versions 1.5 or older
		if($rVersion < 1.6)
			$this->smarty->assign("RVERSION", "OLD");
		else
			$this->smarty->assign("RVERSION", "NEW");

		// Generate config.inc.php
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/configincphp.tpl", Config::read("roundcube|configFile"), "<" . "?php ");
		if($rc === false)
			return false;

		// Restrict permissions and ownership
		chmod(Config::read("roundcube|configFile"), 0640);
		chgrp(Config::read("roundcube|configFile"), "apache");

		// Generate roundcubemail.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/roundcubemailconf.tpl", Config::read("apache|directoryConfD") . "/roundcubemail.conf");
		if($rc === false)
			return false;
		else
			return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		// Return if Phpfpm, Apache, Postfix, Dovecot or MariaDB are not enabled
		if(Config::read("phpfpm") !== "enabled") {

			Log::warning("Module is not enabled: phpfpm");
			return false;

		} elseif(Config::read("apache") !== "enabled") {

			Log::warning("Module is not enabled: apache");
			return false;

		} elseif(Config::read("postfix") !== "enabled") {

			Log::warning("Module is not enabled: postfix");
			return false;

		} elseif(Config::read("dovecot") !== "enabled") {

			Log::warning("Module is not enabled: dovecot");
			return false;

		} elseif(Config::read("mariadb") !== "enabled") {

			Log::warning("Module is not enabled: mariadb");
			return false;

		}

		Log::debug("Enabling module: roundcube");

		// Configuration
		Config::write("roundcube", "enabled");

		// Make sure there is a Roundcube configuration for Apache
		$cfgFile = Config::read("apache|directoryConfD") . "/roundcubemail.conf";
		if(!is_file($cfgFile)) {

			// Generate roundcubemail.conf
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/roundcubemailconf.tpl", Config::read("apache|directoryConfD") . "/roundcubemail.conf");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::debug("Disabling module: roundcube");

		// Configuration
		Config::write("roundcube", "disabled");

		// Find Roundcube configuration for Apache and delete it
		$cfgFile = Config::read("apache|directoryConfD") . "/roundcubemail.conf";
		if(is_file($cfgFile))
			unlink($cfgFile);

		return true;

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		Log::warning("Start not implemented or not applicable for module: roundcube");

		return false;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		Log::warning("Stop not implemented or not applicable for module: roundcube");

		return false;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		Log::warning("Reload not implemented or not applicable for module: roundcube");

		return false;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		return "";

	}

}

