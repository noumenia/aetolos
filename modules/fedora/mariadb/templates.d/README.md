# templates.d

Override directory with .tpl files. Copy here the .tpl files to override default module templates.

On modules that generate per-virtual host files, templates can be overriden by creating a subdirectory named as the virtual host domain under the templates.d direcotry. Any templates placed within will take precedence for that particular virtual host.

The following list shows the priority from top to bottom:

- php/templates.d/example.tld/virtualhostfpm.tpl	(override only for the virtual host example.tld)
- php/templates.d/virtualhostfpm.tpl			(override template)
- php/virtualhostfpm.tpl				(base template)

