<?php
/**
 * Aetolos - ClamAV
 *
 * ClamAV support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage clamav
 */

/**
 * ClamAV class
 *
 * @package aetolos
 * @subpackage clamav
 */
class ClamAV extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"selinux",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'clamav'			=> "disabled",
			'clamav|clamdConfFile'		=> "/etc/clamd.d/clamd.conf",
			'clamav|clamdSysconfigFile'	=> "/etc/sysconfig/clamd",
			'clamav|clamdSystemdFile'	=> "/usr/lib/systemd/system/clamd.service",
			'clamav|clamdTmpfilesFile'	=> "/usr/lib/tmpfiles.d/clamd.conf",
			'clamav|freshclamFile'		=> "/etc/freshclam.conf",
			'clamav|clamavMilterFile'	=> "/etc/mail/clamav-milter.conf",
			'clamav|clamavMilterTmp'	=> "/usr/lib/tmpfiles.d/clamav-milter.conf",
			'clamav|selinuxModule'		=> "",
			'clamav|milterSelinuxModule'	=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"clamd",
			"clamav",
			"clamav-milter",
			"clamav-freshclam"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("postfix"),
			'repository'	=> array(),
			'service'	=> array(
				"clamd",
				"clamav-milter"
			),
			'conflict'	=> array()
		);

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux - allow ClamAV JIT access
		$rc = exec("/usr/sbin/getsebool clamd_use_jit");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable clamd_use_jit");

			// Enable JIT access
			exec("/usr/sbin/setsebool -P clamd_use_jit on");

		}

		// SELinux - allow ClamAV to scan the system
		$rc = exec("/usr/sbin/getsebool antivirus_can_scan_system");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable antivirus_can_scan_system");

			// Enable ClamAV system scan
			exec("/usr/sbin/setsebool -P antivirus_can_scan_system on");

		}

		// SELinux - allow ClamAV to create its own tmp files
		if(Config::read("clamav|selinuxModule") !== "installed") {

			Log::debug("SELinux: enable clamav_tmp_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/clamav_tmp_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("clamav|selinuxModule", "installed");

		}

		// SELinux - allow ClamAV Milter to bind to milter port
		if(Config::read("clamav|milterSelinuxModule") !== "installed") {

			Log::debug("SELinux: enable clamav_port_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/clamav_port_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("clamav|milterSelinuxModule", "installed");

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: clamav");

		// ClamAV daemon

		// Find the default clamd.conf
		$clamdconfFile = exec("/usr/bin/find /usr/share/doc/ -iname clamd.conf");
		if(
			$clamdconfFile === false ||
			!is_file($clamdconfFile)
		) {

			Log::debug("Reinstall package: clamd");

			// Clamd.conf not found, try to install
			exec(Config::read("pmanager") . " -y --setopt='tsflags=' reinstall clamd");

			// Find the default clamd.conf
			$clamdconfFile = exec("/usr/bin/find /usr/share/doc/ -iname clamd.conf");
			if(
				$clamdconfFile === false ||
				!is_file($clamdconfFile)
			) {

				Log::error("Error while searching for: clamd.conf");
				return false;

			}

		}

		// Load custom changes from template (ignore prepend/append files)
		list($prependFile, $templateFile, $appendFile) = $this->selectTemplateFile(__DIR__ . "/clamdconf.tpl");
		$params = explode("\n", $this->smarty->fetch($templateFile));

		// Load the clamd.conf file into memory
		$conf = file_get_contents($clamdconfFile);
		if($conf === false) {
			Log::error("Error while loading file: " . $clamdconfFile);
			return false;
		}

		// Loop each parameter
		foreach($params as $p) {

			// Trim input
			$p = trim($p);

			// Skip empty and/or invalid lines
			if(empty($p))
				continue;

			$p = explode(" ", $p, 2);

			$rc = $this->setConfParameter($conf, $p[0], $p[1]);
			if($rc === false)
				return false;

		}

		// Remove line to enable execution of clamd
		$conf = str_replace("\nExample\n", "", $conf);

		// Save file
		Log::debug("Writing to file: " . Config::read("clamav|clamdConfFile"));
		$rc = file_put_contents(Config::read("clamav|clamdConfFile"), $conf, LOCK_EX);
		if($rc === false) {
			Log::error("Error while writing to file: " . Config::read("clamav|clamdConfFile"));
			return false;
		}

		// ClamAV daemon sysconfig file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/clamdsysconfig.tpl", Config::read("clamav|clamdSysconfigFile"));
		if($rc === false)
			return false;

		// Assign variables
		$this->smarty->assign("CLAMDCONFFILE", Config::read("clamav|clamdConfFile"));

		// Request systemd reload
		if(!is_file(Config::read("clamav|clamdSystemdFile")))
			$this->daemonReload = true;

		// ClamAV daemon systemd service
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/clamdsystemd.tpl", Config::read("clamav|clamdSystemdFile"));
		if($rc === false)
			return false;

		// ClamAV tmpfiles configuration
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/clamdtmpfiles.tpl", Config::read("clamav|clamdTmpfilesFile"));
		if($rc === false)
			return false;

		// Check for optional /etc/sysconfig/freshclam
		if(is_file("/etc/sysconfig/freshclam")) {

			// ClamAV updater (freshclam)
			$freshclam = file_get_contents("/etc/sysconfig/freshclam");
			if($freshclam === false) {
				Log::error("Error while loading file: /etc/sysconfig/freshclam");
				return false;
			}

			// Remove line to enable freshclam network access
			$freshclam = str_replace("FRESHCLAM_DELAY=disabled-warn", "", $freshclam);

			// Save file
			Log::debug("Writing to file: /etc/sysconfig/freshclam");
			$rc = file_put_contents("/etc/sysconfig/freshclam", $freshclam);
			if($rc === false) {
				Log::error("Error while writing to file: /etc/sysconfig/freshclam");
				return false;
			}

		}

		// ClamAV updater (freshclam.conf)
		$freshclam = file_get_contents("/etc/freshclam.conf");
		if($freshclam === false) {
			Log::error("Error while loading file: /etc/freshclam.conf");
			return false;
		}

		// Remove line to enable execution of freshclam
		$freshclam = str_replace("\nExample\n", "", $freshclam);

		// Disable this test procedure since it requires an excessive amount of memory and causes OOM-kill
		$freshclam = preg_replace('/^#?TestDatabases yes/mi', "TestDatabases no", $freshclam);
		if($freshclam === null) {

			Log::error("Error while parsing /etc/freshclam.conf");
			return false;

		}

		// Enable extra download of the bytecode database
		$freshclam = preg_replace('/^#?Bytecode (?:no|yes)/mi', "Bytecode yes", $freshclam);
		if($freshclam === null) {

			Log::error("Error while parsing /etc/freshclam.conf");
			return false;

		}

		// Save file
		Log::debug("Writing to file: /etc/freshclam.conf");
		$rc = file_put_contents("/etc/freshclam.conf", $freshclam);
		if($rc === false) {
			Log::error("Error while writing to file: /etc/freshclam.conf");
			return false;
		}

		// ClamAV milter
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/clamavmilter.tpl", Config::read("clamav|clamavMilterFile"));
		if($rc === false)
			return false;

		// ClamAV milter tmpfiles configuration
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/clamavmiltertmp.tpl", Config::read("clamav|clamavMilterTmp"));
		if($rc === false)
			return false;

		// Socket directory for clamav-milter
		if(!is_dir("/run/clamav-milter"))
			mkdir("/run/clamav-milter", 0750);

		// Socket directory ownership and permissions
		chown("/run/clamav-milter", "clamilt");
		chgrp("/run/clamav-milter", "virusgroup");
		chmod("/run/clamav-milter", 0750);

		/*
		 * @TODO As of 2015, there is a bug in clamav-milter which prevents it from setting the correct group
		 * ownership to 'postfix'. If we tried to use the the configuration parameter MilterSocketGroup, then
		 * the service would die and fail to start because it has already dropped root priviledges. In other
		 * distributions we could use the /etc/default/clamav-milter file to set the variable SOCKET_RWGROUP,
		 * but this does not seem to work in CentOS 7. As a result, we run a world-writable socket (for now).
		 */
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/defaultclamavmilter.tpl", "/etc/default/clamav-milter");
		if($rc === false)
			return false;
		else
			return true;

	}

}

