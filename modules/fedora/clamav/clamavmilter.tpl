# Aetolos - Automatically generated ClamAV configuration

MilterSocket inet:8890@127.0.0.1
User clamilt
TemporaryDirectory /var/tmp
ClamdSocket tcp:127.0.0.1:3310

OnClean Accept
OnInfected Reject
OnFail Defer
RejectMsg "Blocked by ClamAV: %v"
AddHeader no
LogSyslog yes
LogFacility LOG_MAIL

