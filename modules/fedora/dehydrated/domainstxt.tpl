{if $SERVERNAME!=""}{$SERVERNAME}
{/if}
{if isset($VHOSTS) && is_array($VHOSTS) && sizeof($VHOSTS)>0}{foreach $VHOSTS as $V}{if $V->parkedUnder!=''}{continue}{/if}{$V->domainName}{if isset($MTASTS, $V->prefixAlias) && $MTASTS==="enabled" && $V->prefixAlias!=""} mta-sts.{$V->domainName}{/if}{if isset($V->prefixAlias) && $V->prefixAlias!=""} {$V->prefixAlias}.{$V->domainName}{/if}{if isset($V->parkedDomains)}{foreach $V->parkedDomains as $P} {$P->domainName}{if isset($P->prefixAlias) && $P->prefixAlias!=""} {$P->prefixAlias}.{$P->domainName}{/if}{/foreach}{/if}
{/foreach}{/if}
