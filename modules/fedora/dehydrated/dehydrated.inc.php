<?php
/**
 * Aetolos - Dehydrated ACME & Let's Encrypt client
 *
 * ACME support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage dehydrated
 */

/**
 * Dehydrated class
 *
 * @package aetolos
 * @subpackage dehydrated
 */
class Dehydrated extends Module {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"dependencies",
			"cmdParameters",
			"help",
			"controller",
			"saveVirtualHosts",
			"saveConfiguration",
			"export"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'dehydrated'			=> "disabled",
			'dehydrated|config'		=> "/root/dehydrated/config",
			'dehydrated|hooksh'		=> "/root/dehydrated/hook.sh",
			'dehydrated|basedir'		=> "/etc/pki/letsencrypt",
			'dehydrated|wellknown'		=> "/var/www/html/.well-known/acme-challenge",
			'dehydrated|email'		=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array();

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"registration-email:"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=dehydrated",
			array('short' => "",	'long' => "registration-email=[EMAIL]",		'desc' => "Email to use for registering with the ACME server"),
			""
		);

	}

	/**
	 * Module controller
	 * @param array<string, string> $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		if(isset($cmdParameters['registration-email'])) {

			// Set email to use for registering with the ACME server
			Config::write("dehydrated|email", $cmdParameters['registration-email']);

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Server name, use as a flag for when the server name is also a virtual host
		$serverName = php_uname("n");

		// Loop 2 (server name)
		foreach($vhosts as &$v) {

			if($v->domainName === $serverName) {

				$serverName = "";
				break;

			}

		}

		// Assign variables
		$this->smarty->assign("SERVERNAME", $serverName);
		$this->smarty->assignByRef("VHOSTS", $vhosts);
		$this->smarty->assign("MTASTS", Config::read("mtasts"));

		// Save domains.txt file
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/domainstxt.tpl", Config::read("dehydrated|basedir") . "/domains.txt");
		if($rc === false)
			return false;
		else
			return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: dehydrated");

		// Assign variables
		$this->smarty->assign("SERVERNAME", php_uname("n"));
		$this->smarty->assign("CONFIG", Config::read("dehydrated|config"));
		$this->smarty->assign("BASEDIR", Config::read("dehydrated|basedir"));
		$this->smarty->assign("WELLKNOWN", Config::read("dehydrated|wellknown"));
		$this->smarty->assign("HOOKSH", Config::read("dehydrated|hooksh"));
		$this->smarty->assign("CONTACT_EMAIL", Config::read("dehydrated|email"));
		$this->smarty->assign("POSTFIX", Config::read("postfix"));
		$this->smarty->assign("DOVECOT", Config::read("dovecot"));
		$this->smarty->assign("HAPROXY", Config::read("haproxy"));

		// Generate main configuration (config)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/config.tpl", Config::read("dehydrated|config"));
		if($rc === false)
			return false;

		// Generate hook script (hook.sh)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/hooksh.tpl", Config::read("dehydrated|hooksh"));
		if($rc === false)
			return false;

		// Set permissions
		$rc = chmod(Config::read("dehydrated|hooksh"), 0755);
		if($rc === false) {
			Log::error("Error while setting permissions to file " . Config::read("dehydrated|hooksh"));
			return false;
		}

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		if(Config::read("dehydrated") === "enabled") {

			$rc = $this->generateCron();
			if($rc === false)
				return false;

		} else {

			$rc = $this->removeCron();
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Export virtual host
	 * @param VirtualHostManager $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array<string, string> $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array())
	{

		// Archive directory
		$adir = Config::read("dehydrated|basedir") . "/archive/" . $vhost->domainName;

		// Certificate directory
		$cdir = Config::read("dehydrated|basedir") . "/certs/" . $vhost->domainName;

		// Temporary backup directories
		mkdir($tdir . "dehydrated", 0700);
		mkdir($tdir . "dehydrated/archive", 0700);
		mkdir($tdir . "dehydrated/certs", 0700);

		// Check for archive directory
		if(is_dir($adir)) {

			Log::debug("Copying dehydrated archive");

			exec("cp -r " . escapeshellarg($adir) . " " . escapeshellarg($tdir . "dehydrated/archive"));

		}

		// Check for certs directory
		if(is_dir($cdir)) {

			Log::debug("Copying dehydrated certificates");

			exec("cp -r " . escapeshellarg($cdir) . " " . escapeshellarg($tdir . "dehydrated/certs"));

		}

		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		Log::debug("Enabling feature: dehydrated");

		// Configuration
		Config::write("dehydrated", "enabled");

		return $this->generateCron();

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::debug("Disabling feature: dehydrated");

		// Configuration
		Config::write("dehydrated", "disabled");

		return $this->removeCron();

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		Log::warning("Start not implemented or not applicable for module: dehydrated");

		return false;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		Log::warning("Stop not implemented or not applicable for module: dehydrated");

		return false;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		Log::warning("Reload not implemented or not applicable for module: dehydrated");

		return false;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		return "";

	}

	/**
	 * Generate cron job
	 * @return bool
	 */
	private function generateCron()
	{

		// Assign variables
		$this->smarty->assign("CONFIG", Config::read("dehydrated|config"));

		// In case cronie is not installed
		if(!is_dir("/etc/cron.daily"))
			mkdir("/etc/cron.daily");

		// Generate daily cron script (dehydrated.cron)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/dailycron.tpl", "/etc/cron.daily/dehydrated.cron");
		if($rc === false)
			return false;

		// Set permissions
		$rc = chmod("/etc/cron.daily/dehydrated.cron", 0700);
		if($rc === false) {

			Log::warning("Could not set 0700 permissions to: /etc/cron.daily/dehydrated.cron");
			return false;

		}

		return true;

	}

	/**
	 * Remove cron job
	 * @return bool
	 */
	private function removeCron()
	{

		// Delete cron script
		if(is_file("/etc/cron.daily/dehydrated.cron"))
			unlink("/etc/cron.daily/dehydrated.cron");

		return true;

	}

}

