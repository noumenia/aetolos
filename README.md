# Aetolos

```
                 _        _           
       /\       | |      | |          
      /  \   ___| |_ ___ | | ___  ___ 
     / /\ \ / _ \ __/ _ \| |/ _ \/ __|
    / ____ \  __/ || (_) | | (_) \__ \
   /_/    \_\___|\__\___/|_|\___/|___/
```

Aetolos is a secure open source control panel for virtual hosting.

What makes Aetolos unique, is its ability to create a complete virtual hosting server with basic distro RPM packages (base and epel), without external requirements to 3rd party tools.

Even more interesting, is the fact that Aetolos makes NO modification to the default system, it generates configuration files as needed by each daemon and nothing more. The system is left intact and future-proof, because the RPM packages will continue to update themselves without conflict.

We are not finished yet, Aetolos also has an import feature for cPanel backup archives, which are properly converted to run on vanilla system daemons. Thus, Aetolos makes it easy to migrate from a cPanel server to one of the supported vanilla distros: Alma, Rocky, Oracle and Fedora.

System upgrades have become a reality. It is now possible for Aetolos to backup virtualhosts from CentOS 7/CentOS 8 and restore them to an Alma/Rocky/Oracle 8/9 server.

Where no man (or control panel) has gone before: Aetolos can setup a system, import virtual hosts and then Aetolos can be removed entirely, which leaves you with a nice and clean distro server, for you to manage as you will.

Oh, did we mention that everything is done from the command line? or from a RESTful API daemon!


# Distributions

Since Redhat killed the CentOS project, the world had to move on to other projects. Two new projects have come to replace the sad end of CentOS, these are Alma and Rocky. Because these distros are all based on RHEL, we are going to divide them into EL9 and EL8 modules. These are found under the modules direcotry tree.

| Distro       | Module |
| :----------- | :----- |
| Alma   9     | el9    |
| Alma   8     | el8    |
| Rocky  9     | el9    |
| Rocky  8     | el8    |
| Oracle 9     | el9    |
| Oracle 8     | el8    |
| Fedora       | fedora |


# Required packages

## Packages for EL9
- Install the required packages:
  - ```dnf install php php-cli php-pdo php-mbstring epel-release unzip wget```
  - For Oracle, please replace `epel-release` with `oracle-epel-release-el9`  
      
## Packages for EL8
- Install the required packages:
  - ```dnf install php php-cli php-pdo php-mbstring epel-release unzip wget```
  - For Oracle, please replace `epel-release` with `oracle-epel-release-el8`  
      
## Packages for Fedora
- Install the required packages:
  - ```dnf install php php-cli php-pdo php-mbstring unzip wget```  
      
## MariaDB (optional)
- Install the optional package:
  - ```dnf install php-mysqlnd```  
      

# Install

## via git clone
- [Clone the repository](https://gitlab.com/noumenia/aetolos.git) under the directory: /root/aetolos/
  - ```cd /root```
  - ```git clone 'https://gitlab.com/noumenia/aetolos.git'```

## via zip file
- [Download the latest version](https://gitlab.com/noumenia/aetolos/-/archive/master/aetolos-master.zip) and decompress under the directory: /root/aetolos/
  - ```wget -O /root/master.zip 'https://gitlab.com/noumenia/aetolos/-/archive/master/aetolos-master.zip'```
  - ```unzip /root/master.zip -d /root/```
  - ```mv /root/aetolos-master /root/aetolos```
  - ```rm -rf /root/master.zip```  


# Setup modules

- Think about which modules you want Aetolos to manage, by default most modules are enabled. To see which modules are enabled/disabled use the status parameter:  
  ```/root/aetolos/aetolos --status```  
  You may disable unneeded modules by running:  
  ```/root/aetolos/aetolos --verbose --disable=moduleName```  
  You may enable modules by running:  
  ```/root/aetolos/aetolos --verbose --enable=moduleName```  
---

- You may review the default `.tpl` templates under each module directory. Templates can be overridden by placing modified templates under the templates.d subdirectory within each module. On modules that generate per-virtual host files, templates can be overridden by creating a subdirectory named as the virtual host domain under the templates.d direcotry. Any templates placed within will take precedence for that particular virtual host.

- In addition to copying an entire template file under the templates.d directory, it is also possible to create `_prepend.tpl` and `_append.tpl` files, these are then before and after the contents of the generated file. These template files can be used to add lines to the generated file, without making modifications to the entire template, thus Aetolos updates are more seamless.

- For example, the following template file `php/virtualhostfpm.tpl` can be used in the following ways:

    * replace the entire template:
      `php/templates.d/virtualhostfpm.tpl`

    * prepend to the final template:
      `php/templates.d/virtualhostfpm_prepend.tpl`

    * append to the final template:
      `php/templates.d/virtualhostfpm_append.tpl`

    * replace for a single virtual host:
      `php/templates.d/example.tld/virtualhostfpm.tpl`

    * prepend to the final virtual host template:
      `php/templates.d/example.tld/virtualhostfpm_prepend.tpl`

    * append to the final virtual host template:
      `php/templates.d/example.tld/virtualhostfpm_append.tpl`
---

- Execute the setup to perform a complete system initialization:  
  ```/root/aetolos/aetolos --verbose --setup```  

The setup procedure may ask for some additional requirements. By default Aetolos will install and configure everything required by a virtual hosting server.


# Quick start

Once the setup procedure is complete, here are some common examples to get you started:

- Add virtual host "example.tld" with the default alias "www.example.tld":  
  ```/root/aetolos/aetolos --verbose --module=virtualhost --add-virtualhost=example.tld```

- Add parked domain "foobar.tld" to the above virtual host, with no prefix alias:  
  ```/root/aetolos/aetolos --verbose --module=virtualhost --virtualhost=example.tld --add-pdomain=foobar.tld --no-prefix```

- Add email address "info@example.tld" with a password stored within the password.txt file:  
  ```/root/aetolos/aetolos --verbose --module=dovecot --virtualhost=example.tld --add-email=info --password-file=./password.txt```

- Setup everything and enable+start any relevant services:  
  ```/root/aetolos/aetolos --verbose --setup```  
  ```systemctl --now enable httpd httpd@example.tld postfix dovecot```


# RESTful API daemon

The daemon (aetolosd) can be used to send RESTful API commands to the server. The daemon process can either listen on a UNIX socket or an IP/port address, with optional SSL/TLS encryption. Connections to the daemon can be authenticated by username/password basic authentication (RFC7235). Connections are further limited by file permissions on UNIX sockets and firewalls for IP/port addresses.

The complete OpenAPI documentation is provided here:
https://gitlab.com/noumenia/aetolos-openapi/-/blob/master/openapi.json

A configuration file is required for the daemon work, create one and use the `--config` parameter to specify it to the daemon. Below is an example configuration:
```
# Configuration parameters with default values

# Effective daemon process user
effectiveUser = "root"

# Effective daemon process group
effectiveGroup = "root"

# PID lock file
filePid = "/run/aetolosDaemon.pid"

# Process limit
processLimit = 10

# Connection string
# A UNIX socket file or an IPv4/IPv6 address/port
#connection = "unix:/run/aetolosDaemon.sock"
connection = "inet:9999@127.0.0.1"

# Optional SSL/TLS parameters
# If removed, the daemon will run without encryption
# For details about the options please see:
# https://www.php.net/manual/en/context.ssl.php
ssl.verify_peer = false
ssl.verify_peer_name = false
ssl.allow_self_signed = true
ssl.cafile = "/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem"
ssl.capath = "/etc/pki/tls/certs"
ssl.local_cert = "/etc/pki/tls/certs/localhost.crt"
ssl.local_pk = "/etc/pki/tls/private/localhost.key"
ssl.passphrase = ""
ssl.disable_compression = true

# Basic Authentication RFC7235
username = "aladdin"
password = "opensesame"
```


# Features

The following services or features are currently supported by Aetolos:

##### Distro
- Alma
- Rocky
- Oracle
- Fedora

##### System
- php
- SELinux
- virtual hosts
- daemon with a RESTful API

##### Email
- postfix
- opendkim
- opendmarc
- postgrey
- clamav
- spamassassin
- dovecot

##### Database
- mariadb

##### DNS
- nsd

##### Web
- apache
- roundcube
- haproxy

##### Certificates
- ACME v1 & v2 (Let's Encrypt)

##### Import/Export
- Import/Export Aetolos backup file
- Import cPanel backup file

## How to use

```
Usage: aetolos [GENERAL OPTION] [--module=[MODULE] [OPTION]...]

GENERAL:
  -V,  --version                                 Display version information only
  -h,  --help                                    Display help about parameters
  -v,  --verbose                                 Enable verbose output to stdout
  -j,  --json                                    JSON format for all output
  -q,  --quiet                                   Disable all output
  -s,  --setup                                   Perform system setup for all modules
       --status                                  System and module status

IMPORT/EXPORT:
  -i,  --import=[FILE]                           Import Aetolos or cPanel backup file
  -e,  --export=[FILE]                           Export to an Aetolos backup file
           --virtualhost=[DOMAIN]                Select the virtual host to export/backup
           --extradb=[DB[,DB]]                   Provide additional databases, comma separated

MODULES:
       --enable=[MODULE]                         Enable a module
       --disable=[MODULE]                        Disable a module
       --start=[MODULE]                          Start a module via systemctl
       --stop=[MODULE]                           Stop a module via systemctl
  -r,  --reconfigure=[MODULE]                    Reconfigure/regenerate module config files
  MODULE = apache, apache24us, clamav, dehydrated, dovecot, haproxy, mariadb, mtasts, nsd, opendkim, opendmarc, php, php72us, postfix, postgrey, roundcube, spamassassin, virtualhost

  --module=dehydrated
       --registration-email=[EMAIL]              Email to use for registering with the ACME server

  --module=dovecot
       --virtualhost=[DOMAIN]                    Select the virtual host to modify with the following commands
           --add-email=[EMAIL]                   Add new email address (the @domain part is optional)
               --password=[PASSWORD]             Set email password (plain text or encrypted)
               --password-file=[FILE]            Set email password from file (plain text or encrypted)
               --quota=[bytes]                   Set email quota (in bytes)

           --remove-email=[EMAIL]                Remove an email address and delete all email data

           --modify-email=[EMAIL]                Select an email address to modify
               --password=[PASSWORD]             Set email password (plain text or encrypted)
               --password-file=[FILE]            Set email password from file (plain text or encrypted)
               --quota=[bytes]                   Set email quota (in bytes)
               --enable                          Enable email address
               --disable                         Disable email address

           --list-emails                         List all emails for the selected virtual host
  EMAIL = the @domain part is optional, if none given then the domain is selected from the --virtualhost parameter
  PASSWORD = encrypted passwords must have the {MD5-CRYPT} or {SHA512-CRYPT} prefix, otherwise they will be parsed as plain text.

  --module=haproxy
       --haproxy-type=[web|db]                   Set to "web" or "db" to change the haproxy service type
       --add-server=[SERVER]                     Add a new server
       --remove-server=[SERVER]                  Remove an existing server
       --acme-server=[SERVER]                    Set a single server to redirect ACME requests
  SERVER = <ipaddress>[:<port>] [<haproxy server parameters>], for example: 192.168.10.10:80 check maxconn 200

       --stats-listen=[IP:PORT]                  Set an IP address and port to enable the haproxy web stats

  --module=mariadb
       --memory-usage=[PERCENT]                  Set the percentage of memory used by MariaDB (default is 50%)
       --virtualhost=[DOMAIN]                    Select the virtual host to modify with the following commands
           --add-db=[DATABASE]                   Add new database, prefixed by the virtualhost
           --remove-db=[DATABASE]                Remove database

           --add-dbuser=[USER][@localhost]       Add new database user (default access from localhost only)
               --grant="[PRIV...]@DB[@TABLE]"    Set privileges separated by comma and database/table separated by the @ symbol
               --password=[PASSWORD]             Set user password (plain text or HASH encoded)
               --password-file=[FILE]            Set user password from file (plain text or HASH encoded)

           --remove-dbuser=[USER][@localhost]    Remove a database user and delete related privilege data

           --modify-dbuser=[USER][@localhost]    Select the user to modify with the following commands
               --grant="[PRIV...]@DB[@TABLE]"    Set privileges separated by comma and database/table separated by the @ symbol
               --password=[PASSWORD]             Set user password (plain text or HASH encoded)
               --password-file=[FILE]            Set user password from file (plain text or HASH encoded)

           --list-users                          List all users for the selected virtual host

  --module=mtasts
       --set-mx=[HOST]                           Custom MX to use in the mta-sts.txt file
       --no-mx                                   Empty custom MX
  Use set-mx to set a custom MX or no-mx to remove the custom MX and use autodetection of the server name

  --module=virtualhost
       --list-virtualhosts                       List virtual hosts

       --add-virtualhost=[DOMAIN]                Add a new virtual host
           --ipaddress=[IP]                      Use specific IP address	(default: main server IP)
           --home=[DIR]                          Use specific home directory	(default: /home)
           --admin-email=[DIR]                   Administrator/owner email	(default: none)
           --set-prefix=[PREFIX]                 Set a domain prefix alias	(default: www)
           --no-prefix                           Empty domain prefix alias
           --ns=[HOST]                           Specify NS name servers	(default: defined by registrar)
           --mx=[HOST]                           Specify MX mail servers	(default: virtual host name)
  Set the --ns and --mx parameters multiple times to add multiple hosts

       --remove-virtualhost=[DOMAIN]             Remove a virtual host
           --keep-home                           Keep user home directory 	(default: is to delete)

       --virtualhost=[DOMAIN]                    Select the virtual host to modify with the following commands
           --add-pdomain=[DOMAIN]                Add new parked domain name to the selected virtual host
           --remove-pdomain=[DOMAIN]             Remove a parked domain and delete all email data
           --set-prefix=[PREFIX]                 Set a domain prefix alias	(default: www)
           --no-prefix                           Empty domain prefix alias
  Use set-prefix to add an alias like www to DOMAIN or no-prefix to remove the alias
```


# Design diagram

The following diagram shows a simplified view of the Aetolos implementation.

```
                                                                                                                         +-----------------------------+
                                                                                                                         |                             |
                                                                                                                     +-> | LogDestinationArray.inc.php |
                                                                                                                     |   |                             |
                                                                                                                     |   +-----------------------------+
                                                                                                                     |
+-----------+         +----------------+              +----------------+                                             |   Log to array
|           |         |                |              |                |                                             |
| ./aetolos | +---+-> | common.inc.php | +--------+-> | Config.inc.php |                                             |   +----------------------------+
|           |     |   |                |          |   |                |                                             |   |                            |
+-----------+     |   +----------------+          |   +----------------+                                             +-> | LogDestinationNull.inc.php | <----+
                  |                               |                                                                  |   |                            |      |
Process command   |   Autoloader                  |   Set default configuration                                      |   +----------------------------+      |
line parameters   |   Default configuration       |   Detect running system                                          |                                       |
                  |   Initialize loadable modules |                                                                  |   Log to null                         |
                  |                               |                                                                  |                                       |
                  |   +--------------+            |   +-----------------+            +-------------+                 |   +-------------------------------+   |      +---------------------------------+
                  |   |              |            |   |                 |            |             |                 |   |                               |   |      |                                 |
--help            +-> | help.inc.php |            +-> | loginit.inc.php | +--------> | Log.inc.php | +---------------+-> | LogDestinationConsole.inc.php | <-+----+ | LogDestinationInterface.inc.php |
                  |   |              |            |   |                 |            |             |                 |   |                               |   |      |                                 |
                  |   +--------------+            |   +-----------------+            +-------------+                 |   +-------------------------------+   |      +---------------------------------+
                  |                               |                                                                  |                                       |
                  |   Show command parameters     |   Set log destination            Event logging                   |   Log to console                      |      Abstract log destination interface
                  |   Show per-module information |   Set message priority           Display handler                 |                                       |
                  |                               |                                                                  |                                       |
                  |   +----------------+          |   +--------------------+         +-------------------------+     |   +------------------------------+    |
                  |   |                |          |   |                    |         |                         |     |   |                              |    |
--status          +-> | status.inc.php |          +-> | moduleinit.inc.php | <-----+ | ModuleInterface.inc.php |     +-> | LogDestinationSyslog.inc.php | <--+
                  |   |                |          |   |                    |         |                         |         |                              |
                  |   +----------------+          |   +--------------------+         +-------------------------+         +------------------------------+
                  |                               |
                  |   Show module information     |   Instantiate all modules        Abstract module interface           Log to syslog
                  |   Show systemd service status |
                  |                               |
                  |   +----------------+          |   +----------------------+       +-------------------+           +----------------------------------+       +---------------------------+
                  |   |                |          |   |                      |       |                   |           |                                  |       |                           |
--import          +-> | import.inc.php |          +-> | databaseinit.inc.php | +---> | DbFactory.inc.php | +-------> | DatabaseSqlite3.inc.php          | <---+ | DatabaseInterface.inc.php |
                  |   |                |              |                      |       |                   |           |                                  |       |                           |
                  |   +----------------+              +----------------------+       +-------------------+           +----------------------------------+       +---------------------------+
                  |
                  |   Import Aetolos backup           Database factory               SQLite3 database                SQLite3 implementation                     Abstract database interface
                  |   Import cPanel backup            Setup database                 Aetolos configuration
                  |                                   Load database
                  |
                  |   +----------------+              +---------------------+        +------------------------+
                  |   |                |              |                     |        |                        |
--export          +-> | export.inc.php |          +-> | SetupFedora.inc.php | <--+-+ | SetupInterface.inc.php |
                  |   |                |          |   |                     |    |   |                        |
                  |   +----------------+          |   +---------------------+    |   +------------------------+
                  |                               |                              |
                  |   Export Aetolos backup       |   Fedora                     |   Abstract distro interface
                  |                               |                              |
                  |   +---------------+           |   +------------------+       |   +-------------------------+         +----------------+
                  |   |               |           |   |                  |       |   |                         |         |                |
--setup           +-> | setup.inc.php | +---------+-> | SetupEl8.inc.php | <-----+-+ | TemplateFactory.inc.php | <-----+ | Smarty.inc.php |
                  |   |               |           |   |                  |       |   |                         |         |                |
                  |   +---------------+           |   +------------------+       |   +-------------------------+         +----------------+
                  |                               |                              |
                  |   Choose Linux distro         |   Alma 8                     |   Template engine                     Fake Smarty
                  |   Perform system setup        |   Rocky 8                    |
                  |                               |   Oracle 8                   |
                  |                               |                              |
                  |   +----------------+          |   +------------------+       |
                  |   |                |          |   |                  |       |
--enable          +-> | enable.inc.php |          +-> | SetupEl9.inc.php | <-----+
                  |   |                |              |                  |
                  |   +----------------+              +------------------+
                  |
                  |   Enable module                   Alma 9
                  |                                   Rocky 9
                  |   +-----------------+             Oracle 9
                  |   |                 |
--disable         +-> | disable.inc.php |
                  |   |                 |
                  |   +-----------------+
                  |
                  |   Disable module
                  |
                  |   +---------------+
                  |   |               |
--start           +-> | start.inc.php |
                  |   |               |
                  |   +---------------+
                  |
                  |   Start a module via systemctl
                  |
                  |   +--------------+
                  |   |              |
--stop            +-> | stop.inc.php |
                  |   |              |
                  |   +--------------+
                  |
                  |   Stop a module via systemctl
                  |
                  |   +---------------------+
                  |   |                     |
--reconfigure     +-> | reconfigure.inc.php |
                  |   |                     |
                  |   +---------------------+
                  |
                  |   Reconfigure/regenerate module config files
                  |
                  |   +-------------------+
--module          |   |                   |
                  +-> | module class file |
                      |                   |
                      +-------------------+

                      Module specific command-line

```


# Related projects

- [libMilterPHP is a Postfix/Sendmail Milter library implementation in PHP.](https://gitlab.com/noumenia/libmilterphp)
- [mmDbDaemon is a memory-resident MaxMind Database reader implementation in PHP.](https://gitlab.com/noumenia/mmdbdaemon)
- [guardian-milter - Multi-purpose security milter.](https://gitlab.com/noumenia/guardian-milter)

